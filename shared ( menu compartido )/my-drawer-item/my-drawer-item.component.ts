import { Component, Input, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { getNumber } from "tns-core-modules/application-settings";
import * as app from "tns-core-modules/application";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
/************************************************************
 * Keep data that is displayed as drawer items in the MyDrawer component class.
 *************************************************************/
@Component({
    selector: "MyDrawerItem",
    moduleId: module.id,
    templateUrl: "./my-drawer-item.component.html"
})
export class MyDrawerItemComponent implements OnInit {
    @Input() title: string;
    @Input() route: string;
    @Input() icon: string;
    @Input() isSelected: boolean;

    constructor(private routerExtensions: RouterExtensions) {}

    ngOnInit(): void {}

    onNavItemTap(navItemRoute: string): void {
        let f_1 = new Date();
        let number = f_1.getFullYear() + f_1.getMonth() + f_1.getDate();
        let hoy = getNumber("diamesanio");

        console.log(navItemRoute);

        if (number == hoy) {
            this.routerExtensions.navigate([navItemRoute], {
                clearHistory: true
            });
            const sideDrawer = <RadSideDrawer>app.getRootView();
            sideDrawer.closeDrawer();
        } else {
            let options = {
                title: "ATENCIÓN",
                message:
                    "Posee una jornada previa o no ha dado inicio a la jornada de hoy.",
                okButtonText: "OK"
            };

            alert(options);
        }
    }
}