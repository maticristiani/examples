import {
  Component,
  Input,
  OnInit,
  OnDestroy,
  ViewChild,
  ElementRef,
  NgZone
} from "@angular/core";
import {
  DrawerTransitionBase,
  RadSideDrawer,
  SlideInOnTopTransition
} from "nativescript-ui-sidedrawer";
import {
  getString,
  setString,
  setNumber,
  getNumber,
  clear,
  setBoolean,
  getBoolean
} from "tns-core-modules/application-settings";
import { DatabaseService } from "../.././providers/database/database";
import { LoadingIndicator, Mode, OptionsCommon } from '@nstudio/nativescript-loading-indicator';

import { RouterExtensions } from "nativescript-angular/router";
import * as appversion from "nativescript-appversion";
import { InternetConnectionService } from "../.././providers/internet/internet";
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";
import { Progress } from "tns-core-modules/ui/progress";
import * as dialogs from "tns-core-modules/ui/dialogs";
import * as CryptoJS from "crypto-js";
import * as app from "tns-core-modules/application";
import * as fs from "tns-core-modules/file-system";
import { Color } from "tns-core-modules/color";

var OneSignal: any = require("nativescript-onesignal").TnsOneSignal;

var application = require("tns-core-modules/application");
var utilityModule = require("tns-core-modules/utils/utils");
var http = require("tns-core-modules/http");

declare let CGAffineTransformMakeScale: any;
declare var UIApplication;
declare var android;
declare var NSURL;

@Component({
  selector: "MyDrawer",
  moduleId: module.id,
  templateUrl: "./my-drawer.component.html"
})
export class MyDrawerComponent implements OnInit, OnDestroy {
  
  @ViewChild("drawer", {static: false}) drawer: ElementRef;
  @Input() selectedPage: string;
  @Input() email: string;
  @Input() empresa: any;

  public rendicion:boolean
  public progressValue: number;
  public version: string;

  loader: LoadingIndicator;
  options: OptionsCommon; 

  connectionStatus: boolean;
  connection$;

  constructor(
    private serviceDB: DatabaseService,
    public routerExtensions: RouterExtensions,
    private _ngZone: NgZone,
    private _internetConnection: InternetConnectionService
  ) {
    
    this.rendicion = false;
    this.loader = new LoadingIndicator();
 
    this.options = {
            message: 'ENVIANDO',
            details: 'Este proceso puede llevar unos segundos.',
            color: '#FFFFFF', // color of indicator and labels
            backgroundColor: '#393939',
            dimBackground: true, 
            progress: 0,
            margin: 10,
            userInteractionEnabled: false, // default true. Set false so that the touches will fall through it.
            hideBezel: true, // default false, can hide the surrounding bezel
            mode: Mode.DeterminateHorizontalBar, // see options below
            android: {
              cancelable: false,
              progressNumberFormat: "%1d/%2d",
              progressPercentFormat: 0,
              secondaryProgress: 1,
              progressStyle: 1,
              max: 1,
            },
            ios: {
              square: false
            }

      };

    this.version = "1.00";
    this.progressValue = 0;
    this.connectionStatus = true;

    this.connection$ = this._internetConnection.connectionStatus$.subscribe(
      data => {
        this.connectionStatus = data.valueOf();
      }
    );

  }

  ngOnInit(): void {

    appversion.getVersionName().then((version: string) => {
      appversion.getVersionCode().then((code: string) => {
        this.version = version + ' - '+code;
        setString("version", version);
      });
    });

    let rendicion: boolean = getBoolean('rendicion');
    
    if(rendicion || !rendicion){
      this.rendicion = rendicion;
      console.dir('rendicion:', this.rendicion);
    }else if (getString('email')) {
      try{
        this.serviceDB.query('SELECT clave, valor FROM preferencias WHERE clave = "beeorder-rendicion"').then((r: any)=>{
          this.rendicion = ( (r.valor && r.valor == 'true') || (r.valor && r.valor == 1) ) ? true : false;
          console.dir('rendicion:', this.rendicion);
        })  
      }catch(e){
        console.dir('Imposible setear rendicion en drawer.component')
      }
    }    

    if (this.selectedPage == '/login') {
      try{
        this.loader.hide();
      }catch(e){
        console.dir('not-loading');
      }
    }

  }

  ngOnDestroy(): void {
    if (this.connection$) this.connection$.unsubscribe();
  }

  public callNumber(number): void {
    let q = "tel://+" + number;
    utilityModule.openUrl(q);
  }

  public enviarMail(mail): void {
    let q = "mailto:" + mail;
    utilityModule.openUrl(q);
  }

  isPageSelected(pageTitle: string): boolean {
    return pageTitle === this.selectedPage;
  }

  ViewToUrl(url): void {
    utilityModule.openUrl(url);
  }

  cerrarSesion(): void {

    const sideDrawer = <RadSideDrawer>app.getRootView();
    // this.options.android.view = sideDrawer.android;  

    this.loader.show(Object.assign(this.options,{ message: "CERRANDO SESIÓN" }));

    this.loader.show(Object.assign(this.options,{ progress: 0.30 }));                    

    this.enviarDatosGPS().then(res => {

      this.loader.show(Object.assign(this.options,{ progress: 0.30 }));
      this.loader.show(Object.assign(this.options,{ message: "CORROBORANDO PENDIENTES" }));
    

        this.getCantPedidosAndRecibos().then(res => {
          if (res == 0) {
            this.removePedidosAndRecibos().then(() => {
                let obj = { empresa: "", email: "", asociado_erp: "" };
                let tags = JSON.stringify(obj);

                if (isIOS) {
                  try {
                    OneSignal.deleteTagsWithJsonString(tags);
                  } catch {
                    console.log("error delete tags");
                  }
                } else {
                  try {
                    OneSignal.deleteTag("empresa");
                    OneSignal.deleteTag("email");
                    OneSignal.deleteTag("asociado_erp");
                  } catch {
                    console.log("error delete tags");
                  }
                }

                let path_fotos = getString("path_fotos")
                  ? getString("path_fotos")
                  : "";

                clear();
                this.loader.hide();

                setBoolean("flag", false);
                setString("path_fotos", path_fotos);
                
                this.loader.show(Object.assign(this.options,{ progress: 0.90 }));
                this.loader.show(Object.assign(this.options,{ message: "SESIÓN FINALIZADA" }));

                this.routerExtensions.navigate(["/login"], {
                    clearHistory: true
                });

                const sideDrawer = <RadSideDrawer>app.getRootView();
                  sideDrawer.gesturesEnabled = false;
                  sideDrawer.closeDrawer();

                this.loader.hide();
                

               
              })
              .catch(e => {
                console.log("error");

                let path_fotos = getString("path_fotos")
                  ? getString("path_fotos")
                  : "";

                clear();
                this.loader.hide();

                setBoolean("flag", false);
                setString("path_fotos", path_fotos);

                this.loader.show(Object.assign(this.options,{ progress: 0.90 }));
                this.loader.show(Object.assign(this.options,{ message: "SESIÓN FINALIZADA" }));

                this.routerExtensions.navigate(["/login"], {
                    clearHistory: true
                });

                const sideDrawer = <RadSideDrawer>app.getRootView();
                  sideDrawer.gesturesEnabled = false;
                  sideDrawer.closeDrawer();

                this.loader.hide();

              });
          } else {
            this.loader.hide();
            let options = {
              title: "No puede cerrar sesión",
              message: "Posee pedidos pendientes de sincronizar, envielos o borrelos para poder cerrar sesión.",
              okButtonText: "OK"
            };
            dialogs.alert(options);
          }
        });
      })
      .catch(err => {
        this.loader.hide();
        console.log(err);
        let options = {
          title: "ATENCIÓN",
          message: String(err),
          okButtonText: "OK"
        };
        dialogs.alert(options);
      });
  }

  removePedidosAndRecibos() {
      return new Promise((resolve, reject) => {
        let query_ped_del = "DELETE FROM pedidos";
        let query_ped_d_del = 'DELETE FROM pedidos_detalle WHERE pedido_id > 0';
        let query_rec_del = "DELETE FROM recibos";
        let query_rec_r_del = "DELETE FROM recibos_items  WHERE recibo_id > 0";
        
        this.serviceDB.query(query_ped_del, []).then(res => {
              this.serviceDB.query(query_ped_d_del, []).then(res => {
                 this.serviceDB.query(query_rec_del, []).then(res => {
                     this.serviceDB.query(query_rec_r_del, []).then(res => {
                        resolve(res);
                     }).catch(err => { reject(err); });
                 }).catch(err => { reject(err); });
               }).catch(err => { reject(err); });
          }).catch(err => { reject(err); });
      });
   }

  /* sincronizar datos */

  forzarSync() {
    if (this.connectionStatus) {
     
      const sideDrawer = <RadSideDrawer>app.getRootView();
      
      // this.options.android.view = sideDrawer.android;  
      this.loader.show(Object.assign(this.options,{ message: "SINCRONIZANDO DATOS" }));

      this.enviarDatosGPS().then(res => {
          
          this.loader.show(Object.assign(this.options,{ progress: 0.15 }));
          this.loader.show(Object.assign(this.options,{ message: "OBTENIENDO NUEVOS DATOS" }));

          this.getCantPedidosAndRecibos().then(res => {
            
            this.loader.show(Object.assign(this.options,{ progress: 0.30 }));
            this.loader.show(Object.assign(this.options,{ message: "CORROBORANDO PENDIENTES" }));

            if (res == 0) {

              let url = "http://api.beemarket.com.ar/beeorder";
              let testearSQL = false;

              let usuario = {
                password: getString("password"),
                licencia: true,
                email: getString("email"),
                imei: getString("imei"),
                app_version: getString("version"),
                empresa: getString("empresa"),
              };
              
              this.postDatos(url, usuario).then((res: any) => {
                if (res.estado && res.estado == "OK") {
                  this.selectEmpresa(res).then((res: any) => {
                    
                    this.loader.show(Object.assign(this.options,{ progress: 0.50 }));
                    this.loader.show(Object.assign(this.options,{ message: "CORROBORANDO LICENCIA" }));


                    if (res == "CANCEL") {
                      this.progressValue = 0;
                      this.loader.hide();
                      return;
                    }
                    
                    //corroborar
                    if (!res.datos.licencia || res.datos.licencia.length == 0) {
                      
                      this.progressValue = 0;
                      this.loader.hide();
                      
                      let options = {
                        title: "ATENCIÓN",
                        message: "La empresa que ha seleccionado no posee licencia para utilizar beeorder",
                        okButtonText: "OK"
                      };
  
                      alert(options);

                      return this.cerrarSesion();
                    }
  
                    if (!res.datos.dispositivos || res.datos.dispositivos.length == 0) {
                      this.progressValue = 0;
                      this.loader.hide();
                      return this.cerrarSesion();
                    }
  
                    let vigencia = res.datos.licencia.filter((lic) => {
                      let res = lic ? lic : false;
                      return res;
                    });
  
  
                    let dispositivo = res.datos.dispositivos.filter((dis) => {
                      let imei_remoto = dis.dispositivo_id;
  
                      if (dis.dispositivo_id.length != usuario.imei.length) {
                        return null;
                      }
  
                      let res = (dis.dispositivo_id.indexOf(usuario.imei) > -1) ? dis : null;
  
                      return res
                    })
  
  
  
                    if (!dispositivo || dispositivo.length == 0) {
                      this.progressValue = 0;
                      this.loader.hide();
                      return this.cerrarSesion();
                    }
  
                    let date_vigencia = new Date(vigencia[0].vig_hasta);
                    let date_now = new Date();
  
  
                    //dispositivo-bloqueado
                    if (dispositivo[0] && dispositivo[0].bloqueado == 'S') {
                      
                      this.progressValue = 0;
                      this.loader.hide();
                      
                      let options = {
                        title: "ATENCIÓN",
                        message: "Su dispositivo ha sido bloqueado. Comuniquese con el proveedor del servicio.",
                        okButtonText: "OK"
                      };
  
                      alert(options);
  
                      return this.cerrarSesion();
                    }
  
                    //licencia-caducada
                    if (date_now >= date_vigencia) {
                      
                      this.progressValue = 0;
                      this.loader.hide();

                      let options = {
                        title: "ATENCIÓN",
                        message: "La empresa que ha seleccionado posee una licencia caducada. " + date_vigencia,
                        okButtonText: "OK"
                      };
  
                      alert(options);

                      return this.cerrarSesion();
                    }
                    
                   
                    this.loader.show(Object.assign(this.options,{ progress: 0.70 }));
                    this.loader.show(Object.assign(this.options,{ message: "ACTUALIZANDO DATOS" }));
                    //fin

                    let url_descarga = res.datos.url;
                    let name_bd = res.datos.nombre;

                    if (res.datos.host) {
                      setString('empresa_host', res.datos.host);
                    }
                    

                    //salvo-usuario-para-enviar
                    setString("imei", String(usuario.imei));
                    setString("empresa", String(res.datos.empresa));

                    if (testearSQL) {
                      url_descarga = "testing_bd";
                      name_bd = "testing_bd";
                    }

                    this.loader.show(Object.assign(this.options,{ progress: 0.10 }));
                    this.loader.show(Object.assign(this.options,{ message: "DESCARGANDO DATOS" }));
                    
                    let progress = 0.10;
                    
                    let id = setInterval(() => {
                        
                        if (progress>1.00) {
                          progress = 0.00;
                        }

                        progress = Number(progress) + 0.10;
                        
                        console.log('intervalo:',progress)
                        
                        this.loader.show(Object.assign(this.options,{ progress: progress  }));
                    }, 2000);

                    console.dir('DESCARGA DE BD: ');

                    this.descargaBase(usuario, url_descarga, name_bd).then(
                      res => {
                        
                        try{
                          clearInterval(id);
                        }catch(e){
                          clearInterval(0);
                        }
                        
                        if (res == "ERROR") {
                          this.loader.hide();

                          let options = {
                            title: "ATENCIÓN",
                            message:
                              "Ocurrio un error al intentar descargar la base de datos. Intente nuevamente en unos momentos.",
                            okButtonText: "OK"
                          };
                     
                          dialogs.alert(options);
                     
                        } else {
                          console.dir("voy-inicio-local");
                          this.inicioLocal(usuario);
                        }

                      }).catch((err)=>{

                          try{
                            clearInterval(id);
                          }catch(e){
                            clearInterval(0);
                          }

                          this.loader.hide();

                          let options = {
                            title: "ATENCIÓN",
                            message:"Ocurrio un error al intentar descargar la base de datos. Intente nuevamente en unos momentos.",
                            okButtonText: "OK"
                          };
                          
                          dialogs.alert(options);
                      });
                  });
                } else {
                  console.dir("respuesta-error-post-data");

                  this.loader.hide();

                  dialogs
                    .confirm({
                      title: res.estado,
                      message: res.mensaje,
                      okButtonText: "INICAR SESION NUEVAMENTE",
                      cancelButtonText: "CANCELAR"
                      // neutralButtonText: "Neutral text"
                    })
                    .then(result => {
                      console.dir(result);

                      if (result) {
                        this.getCantPedidosAndRecibos().then(res => {
                          if (res == 0) {
                            this.removePedidosAndRecibos()
                              .then(() => {
                                clear();
                                this.loader.hide();
                                this.routerExtensions.navigate(["/login"], {
                                  clearHistory: true
                                });
                              })
                              .catch(e => {
                                console.log("error");
                                clear();
                                this.loader.hide();
                                this.routerExtensions.navigate(["/login"], {
                                  clearHistory: true
                                });
                              });
                          } else {
                            this.loader.hide();
                            let options = {
                              title: "No puede cerrar sesión",
                              message:
                                "Posee pedidos pendientes de sincronizar, envielos o borrelos para poder actualizar sus datos.",
                              okButtonText: "OK"
                            };
                            dialogs.alert(options);
                          }
                        });
                      }
                    });
                }
              });
            } else {
              this.loader.hide();
              let options = {
                title: "Imposible actualizar la base",
                message:
                  "Posee pedidos pendientes de sincronizar, envielos o borrelos para poder actualizar sus datos.",
                okButtonText: "OK"
              };
              dialogs.alert(options);
            }
          });
        })
        .catch(err => {
          this.loader.hide();

          let options = {
            title: "ATENCIÓN",
            message: String(err),
            okButtonText: "OK"
          };

          dialogs.alert(options);
        });
    } else {
      let options = {
        title: "ATENCIÓN",
        message: "Necesita una conexion a internet para sincronizar",
        okButtonText: "VOLVER"
      };

      alert(options);
    }
  }

  private inicioLocal(usuario) {
    let query =
      'SELECT * FROM usuarios WHERE email = "' +
      usuario.email.toLowerCase() +
      '"';
    console.dir(query);
    this.serviceDB
      .get(query, [])
      .then((row: any) => {
        if (row) {
          console.dir(row);

          if (!row.asociado_erp) {
            row.asociado_erp = "sin-erp";
          }

          setString("asociado_erp", String(row.asociado_erp));
          setNumber("asociado_id", Number(row.asociado_id));
          setString("asociado_tabla", String(row.asociado_tabla));
          setString("nombre", String(row.nombre));
          setString("email", String(row.email));

          if (row.supervisor) {
            setNumber("supervisor", Number(row.supervisor));
          }
          if (row.id) {
            setNumber("id", Number(row.id));
          }

          if (row.asociado_tabla == "vendedores") {
            let query_vendedor =
              'SELECT * FROM vendedores WHERE id = "' + row.asociado_id + '"';

            this.serviceDB.get(query_vendedor).then((row: any) => {
              if (row.codigo_erp) {
                setString("vendedor_erp", String(row.codigo_erp));
              }
              setNumber("vendedor_id", Number(row.id));
            });
          }
        }
        this.loader.hide();
      })
      .catch(err => {
        console.dir(err);
        this.loader.hide();
        let options = {
          title: "ATENCIÓN",
          message: "No hemos encontrado el usuario que ha ingresado",
          okButtonText: "OK"
        };

        dialogs.alert(options);
      });
  }

  private getCantPedidosAndRecibos() {
    return new Promise((resolve, reject) => {
      let pendientes = 0;
      
      let query_p = " SELECT count(p.id) AS pendientes_p FROM pedidos AS p WHERE p.enviado = ?   ";
      let query_r = " SELECT count(r.id) AS pendientes_r FROM recibos AS r ";
          query_r += " WHERE r.tipo =? AND r.id IS NOT NULL AND r.sincronizado = ?";
          query_r += " ORDER BY r.id ";
      //rendicion se activa en el constructor
      
      let params_1 = [0];

      let tipo = "recibo";
      let params_2 = [tipo, 0];

      this.serviceDB
        .get(query_p, params_1)
        .then((row: any) => {
          pendientes = pendientes + row.pendientes_p;
          if (this.rendicion) {
          
              this.serviceDB.get(query_r, params_2)
              .then((row: any) => {
                pendientes = pendientes + row.pendientes_r;
                console.dir('pedidos+recibos', pendientes);
                resolve(pendientes);  
              })
              .catch(err => { reject(err) });
           
          }else{
            resolve(pendientes);  
          }
        })
        .catch(err => {
          reject(err);
        });
    });
  }


  private postDatos(url, data) {
    return new Promise((resolve, reject) => {

      if (!this.connectionStatus) {
        reject({
          estado: "ERROR",
          mensaje: 'Debe poseer una conexion a internet para proceder.'
        });
      }

      let password = CryptoJS.MD5(data.password).toString();
      
      if (data.imei && data.imei.length == 0) {
        data.imei = "IMEI";
      }

      data.empresa = data.empresa ? data.empresa : '';

      let datos = encodeURI(
        "email=" +
        data.email +
        "&imei=" +
        data.imei +
        "&password=" +
        password +
        "&app_version=" +
        this.version +
        "&licencia=" +
        data.licencia +
        "&empresa=" +
        data.empresa
      );

      if (data.tipo) {

        datos = encodeURI(
          "email=" +
          data.email +
          "&imei=" +
          data.imei +
          "&app_version=" +
          this.version +
          "&licencia=" +
          data.licencia +
          "&empresa=" +
          data.empresa +
          "&nombre=" +
          data.nombre +
          "&tipo=" +
          data.tipo +
          "&modelo=" +
          data.modelo +
          "&os=" +
          data.os +
          "&osversion=" +
          data.osversion
        );
      }

      http.request({
          url: url,
          method: "POST",
          headers: {
            "postman-token": "4742a34e-aaaa-fcc2-b6f5-28002649ffeb",
            "cache-control": "no-cache",
            "content-type": "application/x-www-form-urlencoded"
          },
          content: datos
        })
        .then(
          function (response) {
                let r = {
                    estado: "ERROR",
                    mensaje: "Respuesta no legible"
                  };

                  try {
                    r = response.content.toJSON();
                    resolve(r);
                  } catch (err) {
                    console.dir("no-parse-json");
                    resolve(r);
                  }
          },
          function (e) {
            console.log(e);

            let res = {
              estado: "ERROR"
            };

            reject(res);
          }
        );
    });
  }
 
  private descargaBase(usuario, url, name_bd) {
    return new Promise((resolve, reject) => {
      var filePath = "";

      if (isAndroid) {
        filePath = fs.path.join(fs.knownFolders.currentApp().path, name_bd);
      } else {
        filePath = fs.path.join(fs.knownFolders.documents().path, name_bd);
      }

      http.getFile(url, filePath).then(downloadedFile => {
          let bd_oldName = getString("bd_name");

          setString("bd_email", usuario.email);

          this.serviceDB.setDB(name_bd, filePath, bd_oldName).then(res => {
            resolve(res);
          });
        })
        .catch(err => {
          console.log("error en fx postData");
          console.log("${e}");

          let res = {
            estado: "ERROR"
          };

          reject(res);
        });
    });
  }

  private selectEmpresa(res) {
    return new Promise((resolve, reject) => {
      if (res.datos.length > 1) {
        let opciones = [];

        res.datos.forEach(d => {
          opciones.push(d.empresa);
        });

        dialogs
          .action(
            "Seleccione una empresa para continuar.",
            "Cancelar",
            opciones
          )
          .then(result => {
            if (result == "Cancelar") {
              resolve("CANCEL");
            }

            res.datos.forEach(d => {
              if (result == d.empresa) {
                res.datos = d;
                resolve(res);
              }
            });
          });
      } else {
        res.datos.forEach(d => {
          res.datos = d;
          resolve(res);
        });
      }
    });
  }

  // acciones sobre gps

   private enviarDatosGPS() {
    return new Promise((resolve, reject) => {
      this.serviceDB
        .get(
          'SELECT * FROM preferencias WHERE clave = "beeorder-seguimiento-gps"'
        )
        .then((row: any) => {
          if ( (row && row.valor == "true") || (row && row.valor == 1)) {
            this.gpsCabecera()
              .then((base: any) => {
                // console.dir(gps);
                console.log("armado de gps: OK");

                if (base && base.gps[0].eventos.length > 1) {
                  
                  console.dir(base);

                  this.postData(base, "gps")
                    .then((datos: any) => {
                      console.log("rta: OK");
                      if (datos.estado == "OK") {
                        this.removeAllGps()
                          .then(() => {
                            setTimeout(()=>{
                              resolve(datos);
                            },1000)
                          })
                          .catch(err => {
                            reject(
                              "NO PUDIERON ELIMINARSE LOS DATOS SINCRONIZADOS DE LA TABLA GPS."
                            );
                          });
                      } else {
                        reject(
                          "NO SE RECIBIERON CORRECTAMENTE LOS DATOS DE SEGUIMIENTO."
                        );
                      }
                    })
                    .catch(err => {
                      reject(
                        "NO SE PUDIERON ENVIAR LOS DATOS DE SEGUIMIENTO AL SERVIDOR."
                      );
                    });
                } else {
                  console.log("sin-datos-pendientes");
                  resolve({
                    estado: "OK",
                    mensaje: "SIN DATOS PARA SINCRONIZAR."
                  });
                }
              })
              .catch(err => {
                console.log("armado de gps: ERROR");
                reject("NO PUDIMOS OBTENER LOS DATOS DE SEGUIMIENTO.");
              });
          } else {
            resolve({ estado: "OK", mensaje: "SIN TRACKING GPS." });
          }
        });
    });
  }

  private gpsCabecera() {
    return new Promise((resolve, reject) => {
      let params = [];
      let query_gps = "SELECT * FROM gps";

      let fecha = new Date();
      let fecha_envio =
        fecha.getDay() +
        1 +
        "/" +
        (fecha.getMonth() + 1) +
        "/" +
        fecha.getFullYear();

      let base = {
        gps: []
      };

      base.gps[0] = {
        imei: getString("imei") ? getString("imei") : "",
        modelo: device.model,
        deviceType: device.deviceType,
        manufacturer: device.manufacturer,
        os: device.os + "[" + device.osVersion + "]",
        sdkVersion: device.sdkVersion,
        asociado_erp: getString("asociado_erp")
          ? getString("asociado_erp")
          : "",
        nombre: getString("nombre") ? getString("nombre") : "",
        email: getString("email") ? getString("email") : "",
        empresa: getString("empresa") ? getString("empresa") : "",
        fecha_envio: fecha_envio ? fecha_envio : "-",
        eventos: []
      };

      this.serviceDB
        .all(query_gps, params)
        .then((rows: any) => {
          base.gps[0].eventos = rows;
          resolve(base);
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  private removeAllGps() {
    return new Promise((resolve, reject) => {
      let query_gps = "DELETE  FROM gps";
      let params = [];

      this.serviceDB
        .query(query_gps, params)
        .then(res => {
          resolve("OK");
        })
        .catch(err => {
          reject("ERROR");
        });
    });
  }

  public postData(base, instancia) {
    return new Promise((resolve, reject) => {
      
      let empresa: string = getString("empresa").toLowerCase();
      
      let url: string = getString('empresa_host') ? getString('empresa_host') :  "http://" + empresa + ".beemarket.com.ar/";
          url +=  "beecart/" + instancia;

          console.dir('url');
          console.dir(url);

      http.request({
          url: url,
          method: "POST",
          headers: {
            "postman-token": "4742a34e-aaaa-fcc2-b6f5-28002649ffeb",
            "cache-control": "no-cache",
            "content-type": "application/json"
          },
          content: JSON.stringify(base)
        })
        .then(response => {
            console.dir(response) 
            let r = {
              estado: "ERROR",
              mensaje: "Respuesta no legible"
            };

            try {
              r = response.content.toJSON();
              resolve(r);
            } catch (err) {
              console.dir("no-parse-json");
              resolve(r);
            }

          },e => {
            console.dir(e);
            reject("ERROR");
          }
        );
    });
  }

  public openNavigation() {
    let direccion = this.empresa.direccion;

    // Creates an Intent that will load a navigation
    if (isAndroid) {
      let string = "google.navigation:q=" + direccion;

      let packageManager = application.android.context.getPackageManager();
      let gmmIntentUri = android.net.Uri.parse(string);

      let mapIntent = new android.content.Intent(
        android.content.Intent.ACTION_VIEW,
        gmmIntentUri
      );

      mapIntent.setPackage("com.google.android.apps.maps");
      application.android.foregroundActivity.startActivity(mapIntent);
    } else {
      //"comgooglemaps://?center=40.765819,-73.975866&zoom=14&views=traffic"
      //comgooglemaps://?center=-34.8207997,-58.4668565&zoom=18&views=traffic&q=Alem 644,Monte Grande,Buenos Aires.

      try {
        let replaced = direccion.split(" ").join("+");
        let replaced_2 = replaced.split(",").join("+");

        let string =
          "comgooglemaps://?daddr=" + replaced_2 + "&directionsmode=driving";

        console.log(string);

        //comgooglemapsurl://maps.google.com/?q=@37.3161,-122.1836 -> coordenadas
        let app = NSURL.URLWithString("comgooglemaps://");
        let url = NSURL.URLWithString(string);

        if (
          utilityModule.ios
            .getter(UIApplication, UIApplication.sharedApplication)
            .canOpenURL(app)
        ) {
          return utilityModule.ios
            .getter(UIApplication, UIApplication.sharedApplication)
            .openURL(url);
        } else {
          //id585027354?l=es&mt=8
          let urlStr =
            "itms-apps://itunes.apple.com/ar/app/apple-store/id585027354?l=es&mt=8";
          if (
            utilityModule.ios
              .getter(UIApplication, UIApplication.sharedApplication)
              .canOpenURL(urlStr)
          ) {
            return utilityModule.ios
              .getter(UIApplication, UIApplication.sharedApplication)
              .openURL(urlStr);
          }
        }

        // if (UIApplication.sharedApplication().canOpenURL(app)) {
        //     return UIApplication.sharedApplication().openURL(url);
        // }
      } catch (e) {
        // We Don't do anything with an error.  We just output it
        console.error("Error in OpenURL", e);
      }
    }
  }
}