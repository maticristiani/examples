# APP README

### Requerimients
Dependencias requeridas para trabajar con beeorder

* [Angular]
* [NativeScript]
* [node.js]
* [androidStudio]
* [Xcode]

### Instalación de entorno
Crear proyecto e instalar sus dependencias
```sh
$ cd ...
$ npm install -g
$ tns run android
```

Llevar a produccion...

```sh
$ tns build android --release --key-store-path <path-to-your-keystore> --key-store-password <your-key-store-password> --key-store-alias <your-alias-name> --key-store-alias-password <your-alias-password> ---copy-to app.apk
$ NODE_ENV=production node app
```

### Plugins de app
Listado de plugins requeridos.

| Plugin | URL |
| ------ | ------ |
| **crypto-js** | [url] |
| **nativescript-angular** | [url] |
| **nativescript-appversion** | [url] |
| **nativescript-camera** | [url] |
| **nativescript-geolocation** | [url] |
| **nativescript-mapbox** | [url] |
| **nativescript-sqlite** | [url] |
| **nativescript-theme-core** | [url] |
| **nativescript-ui-calendar** | [url] |
| **nativescript-ui-chart** | [url] |
| **nativescript-ui-dataform** | [url] |
| **nativescript-ui-gauge** | [url] |
| **nativescript-ui-listview** | [url] |
| **nativescript-ui-sidedrawer** | [url] |
| **nativescript-web-image-cache** | [url] |
| **pdfmake** | [url] |

n la app. |


### changelog

**app**


License
----
2019@**MEEMBA SOFTWARE SRL**


