/* imports */
import { ListViewLinearLayout, ListViewGridLayout, ListViewStaggeredLayout, LoadOnDemandListViewEventData, ListViewEventData, RadListView, ListViewLoadOnDemandMode  } from "nativescript-ui-listview";
import { Component, OnInit, OnDestroy, ViewChild, ViewContainerRef, AfterContentInit, NgZone } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";

/* models */
import { Producto } from "./models/producto/producto.model";
import { Vista } from "./models/vista/vista.model";
import { Pedido } from "./models/pedido/pedido.model";
import { Cliente } from "./models/cliente/cliente.model";
import { Filtrera } from "./models/filtrera/filtrera.model";
import { Titulo } from "./models/titulo/titulo.model";

/* modals */
import { modalPopAndroid } from "./modals/producto_pop_android/producto-pop-android";
import { modalFiltrador } from "./modals/producto_filtrador/producto-filtrador";
import { modalBuscador } from "./modals/producto_buscador/producto-buscador";
import { modalCabecera } from "./modals/producto_cabecera/producto-cabecera";
import { modalDetalle } from "./modals/producto_detalle/producto-detalle";
import { modalPopIos } from "./modals/producto_pop_ios/producto-pop-ios";
import { modalImage } from "./modals/producto_image/producto-image";
import { setString, getString, getNumber, getBoolean, remove } from "tns-core-modules/application-settings";

import { RadListViewComponent } from "nativescript-ui-listview/angular";

import { DatabaseService } from ".././providers/database/database";
import { RouterExtensions } from "nativescript-angular/router";
import { GPService } from ".././providers/geolocation/geolocation";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";

import { ObservableArray } from "tns-core-modules/data/observable-array";
import { alert, confirm, action } from "tns-core-modules/ui/dialogs";
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";
import * as app from "tns-core-modules/application";
import { ActivatedRoute } from "@angular/router";
import * as application from "tns-core-modules/application";
import { WorkerService } from "../worker.service";

declare let CGAffineTransformMakeScale: any;
declare var UIApplication;
declare var android;
declare var NSURL;

var imageCacheModule=require("nativescript-web-image-cache");
var sqlite = require('nativescript-sqlite');
var utilityModule = require("tns-core-modules/utils/utils");


/* declare */
declare var android;
declare var java;

@Component({
    selector: "productos2",
    moduleId: module.id,
    providers: [ModalDialogService],
    templateUrl: "./productos2.component.html"
})

export class productos2Component implements OnInit, AfterContentInit, OnDestroy {
    
    @ViewChild("radListProds", {static: false}) listViewComponent: RadListViewComponent;
    
    private _templateSelector: (item: any, index: number, items: any) => string;
    private _dataItems: ObservableArray<Producto>;
    private _dataLoad: Array<Producto>;

    private _numberOfAddedItems: number;
    public p_nocompra: Array<any>;
    
    public _viewActive: Vista;
    public filtrera: Filtrera;
    private cliente: Cliente;
    public titulo : Titulo;
    public pedido: Pedido;

    public flexDirections: string;
    public searchPhrase: string;
    public de_sistema: boolean;
    public mostrarFoto:boolean;
    public openModal: boolean;
    public activity: boolean;
    public widthScreen: any;
    private dbWorker: any;
    public isDeviceIos: boolean;

    renderView = false;
    renderViewTimeout: any;

    public isios: boolean;

    constructor(
       private ngZone: NgZone,
       private routerExtensions: RouterExtensions, 
       private modalService: ModalDialogService,
       private workerService: WorkerService,
       private serviceDB: DatabaseService, 
       private vcRef: ViewContainerRef,
       private route: ActivatedRoute,
       private _gpsservice: GPService,
       ) {

        this.dbWorker = this.workerService.initDatabaseWorker();
        this.widthScreen = screen.mainScreen.widthPixels-4;
        this._dataLoad = new Array();
        this._dataItems = new ObservableArray<Producto>();
        this._templateSelector = this.templateSelectorFunction;

        this.isDeviceIos = (isIOS) ? true: false;

        this._viewActive = { url: 'res://ic_list_white', name:'list', android: ( isAndroid ? true : false )  };
        this.filtrera = { activo: false, filtros: [], orden: '', };
        this.flexDirections = 'column';
        this.de_sistema = false;
        this.openModal = false;
        this.activity = true;
        this.p_nocompra = [];
        this.searchPhrase = '';
        this.isios = false;
        this.mostrarFoto = true;
        this.cliente = { id: 0, erp: '0', nombre: '-', precio_lista: 1, tipo_id: 0};
        this.titulo = { linea_1: '' ,  linea_2: '', linea_3: ''};
        this.pedido = { id: null, subtotal_con_descuento: 0, total: 0, iva: 0, descuento: 0, cantidad: 0, cantidad_items: 0, no_compra: '' };
        
    }
    
    public get dataItems(): ObservableArray<Producto> {
        return this._dataItems;
    }

    get templateSelector(): (item: any, index: number, items: any) => string {
        return this._templateSelector;
    }

    set templateSelector(value: (item: any, index: number, items: any) => string) {
        this._templateSelector = value;
    }

    public templateSelectorFunction = (item: any, index: number, items: any) => {
        if (item.de_sistema && item.de_sistema=='N') {
          return "producto";
        }else{
          return "sistema";  
        }
    }

    ngOnInit(){
      this.isios = isIOS ? true: false;
      this.route.params.subscribe((params) => {
                  console.log(params["cliente_erp"])
                  this.cliente.erp = params["cliente_erp"];
                  this.cliente.id = params["cliente_id"];
                  this.cliente.tipo_id = params["cliente_tipo_id"];
                  this.cliente.precio_lista = params["cliente_p_lista"];
                  this.pedido.id = params["pedido_id"];
                  this.cliente.nombre = params["cliente_nombre"];
      });
      
      this.mostrarFoto = (getString('mostrar_foto') && getString('mostrar_foto')=='ON') ? true : false;

    }

    ngAfterContentInit() {
         this.renderViewTimeout = setTimeout(() => {
             this.renderView = true;             

             this.titulo.linea_1 = this.cliente.nombre+" | [ "+this.cliente.erp+" ]" ;
             this.titulo.linea_2 = (this.pedido.id && this.pedido.id ==0 ) ?  ((this._dataLoad.length+this._dataItems.length)+' items / Nuevo pedido : -') : ((this._dataLoad.length+this._dataItems.length)+' items / Modif pedido : '+this.pedido.id);

             let filtrera_str = getString('filtrera') ? getString('filtrera')  : '';
             let query_filtro = getString('query_filtro') ? getString('query_filtro') : '   '; 
             
             if (filtrera_str && filtrera_str.length > 0 && this.pedido.id>0) {
                   console.dir('inicio-con-filtro')
                   this.filtrera = (filtrera_str && filtrera_str.length >0) ? JSON.parse(filtrera_str) : { activo: true, filtros: [], orden: '' };
                   this.inicializarNoCompra().then(()=>{
                     this.inicializarProductos(true, query_filtro)
                   })
                 }else{
                   console.dir('inicio-normal')
                   this.inicializarNoCompra().then(()=>{
                     this.inicializarProductos(false, ' GROUP BY p.codigo_erp order BY p.orden ASC ');
                   })
              }

         }, 600);
      }
      
    ngOnDestroy() {
           clearTimeout(this.renderViewTimeout); 
           this._dataItems = null;
           this._dataLoad = null;
           this.vcRef.clear()
    }

    onDrawerButtonTap(): void {
          if(this.activity){
              return;
          }

          const sideDrawer = <RadSideDrawer>app.getRootView();
          sideDrawer.showDrawer();
    }

    public deleteProduct(p): void{

        if(this.activity){
            return;
        }

        if (this.pedido.id != 0 && p.cantidad != 0) {
        
        let options = {
            title: "ELIMAR PRODUCTO",
            message: "Esta seguro que desea eliminar el producto "+p.codigo_erp+".",
            okButtonText: "Si",
            cancelButtonText: "No",
            neutralButtonText: "Cancelar"
        };

            confirm(options).then((res: boolean) => {
                if (res) {
              
                    let query_prod_del = 'DELETE FROM pedidos_detalle WHERE codigo_erp =? and pedido_id =?';
                    let params = [p.codigo_erp, this.pedido.id];
                  
                    let params_2 = [];
                        params_2.push(this.pedido.id)
                        

                    let query_prod =  " SELECT p.codigo_erp, p.nombre, ";
                        query_prod += " IFNULL(p.stock,9999) AS stock, ";
                        query_prod += " IFNULL(ped_d.cantidad,0) AS cantidad, IFNULL(ped_d.descuento,0) AS descuento, ";
                   
                        /* asociar Lista de precio */
                        let precio = '';
                        if (!this.cliente.precio_lista) {
                          precio = 'p.precio_'+1;
                        }else{
                          precio = isNaN(this.cliente.precio_lista) ? 'p.precio_1'  : 'p.precio_'+Number(this.cliente.precio_lista);
                        }
                   
                        query_prod +=" ROUND("+precio+", 2) AS precio, "
                        query_prod +=" ROUND(("+precio+"-("+precio+"*((IFNULL(ped_d.descuento,0)/100)))),2) AS precio_con_descuento,"    
                        query_prod +=" ROUND(IFNULL((("+precio+"-"+precio+"*(IFNULL(ped_d.descuento,0)/100))*ped_d.cantidad),0),2) AS subtotal_con_descuento, "
                        query_prod +=" p.stock - IFNULL(pd_reservado.cantidad,0) AS disponible, "
                        query_prod +=" IFNULL(p.de_sistema,'N') AS de_sistema "

                        query_prod += " FROM productos as p ";
                        query_prod += " LEFT JOIN pedidos_detalle as ped_d ON (p.codigo_erp = ped_d.codigo_erp AND ped_d.pedido_id = ? ) ";
                        query_prod += " LEFT JOIN pedidos_detalle AS pd_reservado ON p.codigo_erp = pd_reservado.codigo_erp ";
                        query_prod += " LEFT JOIN pedidos AS p_reservado ON (pd_reservado.pedido_id = p_reservado.id AND p_reservado.enviado = '0') ";
                        query_prod += " WHERE p.codigo_erp = '"+p.codigo_erp+"'";
                        
                        console.dir(query_prod);

                    this.serviceDB.query(query_prod_del, params).then((res: any)=>{
                      this.serviceDB.get(query_prod, params_2).then((item: any)=>{
                        //update-vista
                        p.cantidad = Number(item.cantidad).toFixed(0);
                        p.descuento = Number(item.descuento).toFixed(0);

                        p.precio = Number(item.precio).toFixed(2);
                        p.precio_con_descuento  = Number(item.precio_con_descuento).toFixed(2);
                        p.subtotal_con_descuento  = Number(item.subtotal_con_descuento).toFixed(2);
                        p.disponible = item.disponible;
                        
                        if(this.pedido.cantidad <= 1){
                          console.log('ultimo-prod');
                          let params_3 = [this.pedido.id];
                          let query_ped_del = 'DELETE FROM pedidos WHERE id =?';

                          this.serviceDB.query(query_ped_del, params_3).then((res: any)=>{
                             return this.confirmarPedido();
                          }).catch((err)=>{
                              console.dir(err);
                          });

                        }else{
                          this.consultaTotal();
                        }
                        
                        
                      })
                    })
                }
            });

        }
    }
    
    private inicializarNoCompra() {
      return new Promise((resolve, reject) => {
        this.p_nocompra = [];
        
        let query =  " SELECT p.* FROM Productos AS p  WHERE p.codigo_erp IS NOT NULL AND p.de_sistema = 'S' ;";    
        let params = [];
        
        this.dbWorker.postMessage({ type: 'all', query: query, params: params });
        
        const that = new WeakRef(this);
        
        this.dbWorker.onmessage = (res) => {
           that.get().ngZone.runOutsideAngular(()=>{
             let status = res.data.success;
             let rows = res.data.rows;
             that.get().p_nocompra = rows;
             // here is where i update my UI with the response from the worker, like set my Observable Array to the sectionedShifts returned from the worker.
             // that.get().dbWorker.terminate();
             resolve(true);

           })
        }            

      })
    }

    private inicializarProductos(filtro, query_filter): void{
        this.activity = true;
        this._dataLoad = new Array<Producto>();
        
        let fotos_online = (typeof(getBoolean('fotos_online')) === 'boolean' ) ? getBoolean('fotos_online') : false;
            
        if (this.pedido.id == undefined || this.pedido.id == null ) { this.pedido.id = 0 };

        let params = [this.pedido.id];
           
        let q_p =  " SELECT p.orden, p.id, p.codigo_erp, p.nombre, IFNULL(p.activo,1) AS activo, p.sin_foto, p.foto_thumbnail, p.foto_medium, p.foto_big, p.foto, ";
            q_p += " p.filtro_1, p.filtro_2, p.filtro_3, p.filtro_4, p.filtro_5, p.filtro_6, ";
            q_p += " IFNULL(p.stock,9999) AS stock, IFNULL(p.unidad_venta,1) AS unidad_venta, IFNULL(p.iva,21) AS iva, ";
            q_p += " IFNULL(ped_d.cantidad,0) AS cantidad, IFNULL(ped_d.descuento,0) AS descuento, ";
       
            /* asociar Lista de precio */
            let precio = '';
            if (!this.cliente.precio_lista) {
              precio = 'p.precio_'+1;
            }else{
              precio = isNaN(this.cliente.precio_lista) ? 'p.precio_1'  : 'p.precio_'+Number(this.cliente.precio_lista);
            }
       
            q_p +=" ROUND("+precio+", 2) AS precio, "
            q_p +=" ROUND(("+precio+"-("+precio+"*((IFNULL(ped_d.descuento,0)/100)))),2) AS precio_con_descuento,"    
            q_p +=" ROUND(IFNULL((("+precio+"-"+precio+"*(IFNULL(ped_d.descuento,0)/100))*ped_d.cantidad),0),2) AS subtotal_con_descuento, "
            q_p +=" p.stock - IFNULL(pd_reservado.cantidad,0) AS disponible, "
            q_p +=" IFNULL(p.de_sistema,'N') AS de_sistema "

            q_p += " FROM productos as p ";
            q_p += " LEFT JOIN pedidos_detalle as ped_d ON (p.codigo_erp = ped_d.codigo_erp AND ped_d.pedido_id = ? ) ";
            q_p += " LEFT JOIN pedidos_detalle AS pd_reservado ON p.codigo_erp = pd_reservado.codigo_erp ";
            q_p += " LEFT JOIN pedidos AS p_reservado ON (pd_reservado.pedido_id = p_reservado.id AND p_reservado.enviado = '0') ";
            
            //where
            q_p += " WHERE p.codigo_erp IS NOT NULL AND p.de_sistema = 'N' "    
            
            //precio-cero
            let p_cero: string= getString('precio_cero') ? getString('precio_cero') : 'NO';

              switch (p_cero) {
                case "SI":
                  q_p += " AND "+precio+" >= 0 " 
                  break;
                case "NO":
                  q_p += " AND "+precio+" > 0 " 
                  break;
                default:
                  q_p += " AND "+precio+" > 0 " 
                  break;
              }

             q_p += query_filter;

        console.dir(q_p);

        this.dbWorker.postMessage({ type: 'all', query: q_p, params: params });
        const that = new WeakRef(this);

        this.dbWorker.onmessage = (res) => {  
          that.get().ngZone.runOutsideAngular(()=>{
            let status = res.data.success;
            let rows = res.data.rows;

            if (!status) {
                that.get().activity = false;
                alert("Hubo un error al inicializar los productos.");
            }
             
            // here is where i update my UI with the response from the worker, like set my Observable Array to the sectionedShifts returned from the worker.
            // that.get().dbWorker.terminate();
            
            if(filtro){
              that.get().titulo.linea_2 = (rows && rows.length) ? (rows.length+' resultados. ') : ('Sin resultados');
              that.get().titulo.linea_3 =  (that.get().searchPhrase && that.get().searchPhrase.length==0) ? 'DESHACER FILTRO ' : 'DESHACER BUSQUEDA ';
            }

            if (rows && rows.length > 0) { 
                    that.get()._dataLoad = rows;
                    that.get()._numberOfAddedItems = 0;
                    let numbers = getNumber("itemShows") ? getNumber("itemShows") : isAndroid ? 100 : 10;
                    if (numbers > rows.length) { numbers = rows.length };
                    
                    that.get().ngZone.run(()=>{
                      
                      that.get()._dataItems = new ObservableArray<Producto>();
                      that.get().addMoreItemsFromSource(numbers);
                    })
                    
            }else{
              that.get().activity = false;
            }

            that.get().consultaTotal();
          })
        }

        this.dbWorker.onerror = function(err) {  
          that.get().ngZone.run(()=>{
            console.log(`An unhandled error occurred in worker: ${err.filename}, line: ${err.lineno} :`);
            console.log(err.message);
            that.get().activity = false;
          })
        }

    }


    public addMoreItemsFromSource(chunkSize: number) {
        try{
            
            let newItems = this._dataLoad.splice(0, chunkSize);
            console.dir("newItems");
            console.dir(newItems);
            
            if (newItems && newItems.length > 0) {

                newItems.forEach((n)=>{
                  n.precio =  Number(n.precio).toFixed(2);
                  n.precio_con_descuento = Number(n.precio_con_descuento).toFixed(2);
                  n.subtotal_con_descuento = Number(n.subtotal_con_descuento).toFixed(2);
                })
                
                this._dataItems.push(newItems);
                this.activity = false;
                
                this.titulo.linea_2 = (this.pedido.id && this.pedido.id ==0 ) ?  ((this._dataLoad.length+this._dataItems.length)+' items / Nuevo pedido : -') : ((this._dataLoad.length+this._dataItems.length)+' items / Modif pedido : '+this.pedido.id);
                this.listViewComponent.listView.loadOnDemandBufferSize = chunkSize;
                this.listViewComponent.listView.loadOnDemandMode = ListViewLoadOnDemandMode.None;
                this.listViewComponent.listView.loadOnDemandMode = ListViewLoadOnDemandMode.Auto;

            }
        
        }catch(e){
            console.dir('ERROR - addMoreItemsFromSource')
        }
    }



    public onLoadMoreItemsRequested(args: LoadOnDemandListViewEventData) {
        const that = new WeakRef(this);
        const listView: RadListView = args.object;

        console.dir('that.get()._dataLoad:', that.get()._dataLoad.length);

        if (that.get()._dataLoad && that.get()._dataLoad.length > 0) {
                let numbers = getNumber("itemShows") ? getNumber("itemShows") : isAndroid ? 40 : 10;
                
                setTimeout(function () {
                    that.get().addMoreItemsFromSource(numbers);
                    listView.notifyLoadOnDemandFinished();
                }, 600);

                args.returnValue = true; 
         
        } else {
            args.returnValue = false;
            listView.notifyLoadOnDemandFinished(true);
        }

        // args.returnValue = true;
        // listView.notifyLoadOnDemandFinished();
    }

    public limpiarBusqueda(){
      if (this.searchPhrase.length > 0) {
          this.titulo.linea_2 = (this.pedido.id && this.pedido.id ==0 ) ?  ((this._dataLoad.length+this._dataItems.length)+' items / Nuevo pedido : -') : ((this._dataLoad.length+this._dataItems.length)+' items / Modif pedido : '+this.pedido.id);
          this.titulo.linea_3 =  '';
          this.searchPhrase = '';
          
          //rstauro-productos
          this.inicializarProductos(false, ' GROUP BY p.codigo_erp order BY p.orden ASC ');
      };

      if (this.filtrera.activo) { 

            this.filtrera.activo = false;
            this.filtrera.filtros = [];
            this.filtrera.orden = '';
            
            remove('query_filtro')
            remove('filtrera')
            
            //restauro-title
            this.titulo.linea_2 = (this.pedido.id && this.pedido.id ==0 ) ?  ((this._dataLoad.length+this._dataItems.length)+' items / Nuevo pedido : -') : ((this._dataLoad.length+this._dataItems.length)+' items / Modif pedido : '+this.pedido.id);
            this.titulo.linea_3 =  '';

            //rstauro-productos
            this.inicializarProductos(false, ' GROUP BY p.codigo_erp order BY p.orden ASC ');
      }


       switch (this._viewActive.name) {
              case "grid":
                this._viewActive = { url: 'res://ic_grid_white', name:'grid',  android: ( isAndroid ? true : false )   };
                this.flexDirections = 'column';
                this.changeToGrid(null);
                break;
              case "stagged":
                this._viewActive = { url: 'res://ic_stagged_white', name:'stagged',  android: ( isAndroid ? true : false )   };
                this.flexDirections = 'column';
                this.changeToStaggered(null);
                break;
              case "list":
                this._viewActive = { url: 'res://ic_list_white', name:'list',  android: ( isAndroid ? true : false )   };
                this.flexDirections = 'row';
                this.changeToLinear(null);
                break;
              default:
                console.log('default');
                break;
      }

    }

    public openViewFilter() {
        
        if(this.activity){
          return;
        }

        this.createModel(this.filtrera, true, 'filtro').then(res => {
        this.openModal = false;
        this.titulo.linea_2 =  '';
          //where
          if (res && res != 'undefined') {
            
            let query_filtro = ' AND 1 = 1 ';
            let array_orden = [];
            let query_orden = '';

            res.filtros.forEach((f,index)=>{
              f.valores.forEach(v=>{

                if (v.activo && v.query && v.query.length > 4) {
                  
                  query_filtro +=  v.query;

                  if (v.orden && v.orden.length > 0) {
                      array_orden.push(v.orden);
                  }

                }else if(v.activo){
                  query_filtro +=  (v.id =='todos') ? (' AND 2 = 2 ') : (' AND p.filtro_'+f.id+' = '+v.id);
                }

              })
            })

          //group
          query_filtro += ' GROUP BY p.codigo_erp ';
          
          //orden_personalizado
          if (array_orden && array_orden.length > 0) {
            array_orden.forEach((orden,i)=>{
               if (i==0) {
                 query_orden += ' ORDER BY '+orden;
               }else{
                 query_orden += ' , '+orden;
               }
            })
          }

          //orden  
           if (res.orden && res.orden.length > 0) {
            
            /* asociar Lista de precio */
            let precio = '';
            if (!this.cliente.precio_lista) {
              precio = 'p.precio_'+1;
            }else{
              precio = isNaN(this.cliente.precio_lista) ? 'p.precio_1'  : 'p.precio_'+Number(this.cliente.precio_lista);
            }

             switch (res.orden) {
                case "comprados":
                  query_orden += (array_orden && array_orden.length==0) ? ' ORDER BY ped_d.cantidad DESC ' : ' , ped_d.cantidad DESC ';
                  break;
                case "orden":
                  query_orden += (array_orden && array_orden.length==0) ? ' ORDER BY p.orden ASC ': ' , p.orden ASC ';
                  break;
                case "min-price":
                  query_orden += (array_orden && array_orden.length==0) ? ' ORDER BY '+precio+' ASC ': ' , '+precio+' ASC ';
                  break;
                case "max-price":
                  query_orden += (array_orden && array_orden.length==0) ? ' ORDER BY '+precio+' DESC ': ' , '+precio+' DESC ';
                  break;
                default:
                  break;
              }

            }
            query_filtro = query_filtro + query_orden;
            
            console.dir('query-filtro')
            console.dir(query_filtro)
            
            //limpi-busqueda si estaba
            if (this.searchPhrase.length >0) {
              this.searchPhrase = '';
            }

            this.activity = true;
            
            this.filtrera.activo = true;
            this.filtrera.filtros = res.filtros;
            this.filtrera.orden = res.orden;

            let filtrera_str = JSON.stringify(this.filtrera);
            setString('filtrera', filtrera_str);
            setString('query_filtro', query_filtro);

            this.titulo.linea_2 =  'Calculando...';
            this.titulo.linea_3 =  'Eliminar filtro';

            this.inicializarProductos(true, query_filtro);

            //reset-view-active
            this.resetViewActive();

          }
        
        })
    }

    public resetViewActive(): void{ 
      switch (this._viewActive.name) {
              case "grid":
                this._viewActive = { url: 'res://ic_grid_white', name:'grid',  android: ( isAndroid ? true : false )   };
                this.flexDirections = 'column';
                this.changeToGrid(null);
                break;
              case "stagged":
                this._viewActive = { url: 'res://ic_stagged_white', name:'stagged',  android: ( isAndroid ? true : false )   };
                this.flexDirections = 'column';
                this.changeToStaggered(null);
                break;
              case "list":
                this._viewActive = { url: 'res://ic_list_white', name:'list',  android: ( isAndroid ? true : false )   };
                this.flexDirections = 'row';
                this.changeToLinear(null);
                break;
              default:
                console.log('default');
                break;
            }
    }
    
    public openViewSearch(){
      if(this.activity){
          return;
      }

      this.createModel(this.searchPhrase, true, 'buscar').then((res) => {
        this.openModal = false;
        let words = res ? res.str : false;
        
        if (words && words != 'undefined' && words.length > 0) {
          //guardo la frase que busco
          this.searchPhrase = (Array.isArray(words)) ? words[0] : words;
          console.dir(this.searchPhrase);

          let query_erp = '';
          
          //where
          if ((Array.isArray(words))) {
              query_erp = " AND  1 = 1 AND p.codigo_erp = '"+this.searchPhrase+"' ";  
          }else{
              query_erp = " AND  1 = 1 AND p.codigo_erp LIKE '%"+this.searchPhrase+"%' OR  p.nombre LIKE '%"+this.searchPhrase+"%'";  
          }
          
          //group
          query_erp += ' GROUP BY p.codigo_erp ';
          //orden
          query_erp += ' ORDER BY p.nombre ASC ';


          this.titulo.linea_2 = '"'+this.searchPhrase+ '"';
          this.titulo.linea_3 =  'Eliminar busqueda';
          
          if (this.filtrera.filtros && this.filtrera.filtros.length > 0 ) {

            this.filtrera.filtros = [];
            this.filtrera.orden = '';
            this.filtrera.activo = false;
            remove('filtrera');
            remove('query_filtro');

          }

          this.activity = true;
          this.inicializarProductos(true, query_erp);
          //reset-view-active
          this.resetViewActive();
          
          setTimeout(()=>{
          
          if (res.modal) {
            if (this._dataItems.length>0) {
              for (let i = 0; i < this._dataItems.length; i++) {
                       if (this._dataItems[i].codigo_erp.toUpperCase().indexOf(this.searchPhrase) !== -1) {
                           return (this.openPopCant(this._dataItems[i]));
                       }
              }
            }
          }

          },1200)

        }

      })

    }
  
    public openImage(imageUrl): void{
      
      if(this.activity){
          return;
      }

      if(!this.mostrarFoto){
        return;
      }        
      
      if (!this.openModal && imageUrl && imageUrl.length>0) {
        
        this.openModal = true;          
        
        try {
                this.openModal = false;  
                
                console.log(imageUrl);

                if (isAndroid) {
                    
                  if ((imageUrl.indexOf('https://') > -1) || !getBoolean('visualizador_nativo')) {
                    console.log('a');
                    this.createModel(imageUrl, true, 'image').then(result => {
                        this.openModal = false;
                    })

                  }else{                  
                    let intent = new android.content.Intent(android.content.Intent.ACTION_VIEW);
                    intent.setDataAndType(android.net.Uri.fromFile(new java.io.File(imageUrl)), "image/*");
                    application.android.foregroundActivity.startActivity(intent);
                  }
                
                }else{

                 if (getBoolean('fotos_online') || !getBoolean('visualizador_nativo')){
                   
                   this.createModel(imageUrl, true, 'image').then(result => {
                       this.openModal = false;
                    })
                 
                 }else{
                  
                    utilityModule.openFile(imageUrl);                 
                }

             }

        } catch(e) { 
          console.log('@error open native') 

          this.createModel(imageUrl, true, 'image').then(result => {
             this.openModal = false;
          })

        };

      }
    
    };

    public openPopCant(producto): void{
        
        if(this.activity){
          return;
        }

        this.createModel(producto, false, 'pop').then(result => {
                this.openModal = false;

                if (result) {
                    
                    console.dir(result);

                    this.validarProducto(producto.codigo_erp, result).then((valido)=>{

                        if (valido) {
                            
                            if (this.de_sistema) {
                                this.deleteAll();
                                this.de_sistema = false;
                            }

                            this.agregarEditarCarrito(producto,result).then((res)=>{
                                if (res) {
                                      console.dir(producto.disponible);
                                      console.dir(result.cantidad);
                                      let stock = producto.disponible - result.cantidad;
                                      producto.disponible = stock;
                                      producto.cantidad = Number(result.cantidad).toFixed(0);
                                      producto.descuento = Number(result.descuento).toFixed(0);                                      
                                       
                                      setTimeout(()=>{
                                          
                                          this.consultaTotal();
                                                    
                                          //reset-view-active
                                          this.resetViewActive();
                                      }, 300)

                                }
                            });  
                        }

                      })
                                        
                  }else{
                    console.dir('cancel pop')
                  }

        })

    };

    private validarProducto(codigo_erp,result){
        return new Promise((resolve, reject) => {
        if (result.cantidad > 0) {
             if (result.descuento >= 0 && result.descuento  <=100) {
                    resolve('OK')
                }   
        }
      })   
    };


    private getInfoCliente(erp,id){
        return new Promise((resolve, reject) => {
        let query =  " SELECT c.id, c.web_id, c.descuento, c.transporte, c.forma_de_pago, c.observaciones, p.nombre AS nombre_pago, p.id AS id_pago, t.id AS id_transporte, t.nombre AS nombre_transporte FROM CLIENTES AS c";
            query += " LEFT JOIN transportes as t ON (c.transporte = t.id)  ";
            query += " LEFT JOIN pagos as p ON (c.forma_de_pago = p.id)  ";
            query += " WHERE c.codigo_erp =? OR c.id=? ";
        this.serviceDB.get(query,[erp,id]).then((row: any)=>{
            resolve(row);
        }).catch(err=>{
            resolve('ERROR')
        })
       })
    };

    private agregarEditarCarrito(producto,result){
       return new Promise((resolve, reject) => {

        if (this.pedido.id && this.pedido.id == 0) {
        //agregar   
        //datos-de-usuario
        let usuario_email = getString('email');

        //datos-de-vendedor
        let vendedor_erp: any = getString("vendedor_erp");
          if (!vendedor_erp) {
            vendedor_erp = getNumber("vendedor_id");
          }
        //datos-del-cliente 
        let cliente_web_id = 0; 
        
        let estado = 'PENDIENTE'; 
        let enviado = 0; 
    
        let fecha = new Date();

        let dia =  fecha.getDate()+1;
        let mes =  fecha.getMonth()+1;
        let anio = fecha.getFullYear();

        let fecha_entrega = dia + '/' + mes + '/' + anio;

        let transporte_manual_erp = ''; 
        let forma_pago_erp = ''; 
        
        let transporte_manual = ''; 
        let forma_pago = ''; 
        
        let descuento = 0; 
        let observaciones = '';
        let cliente_id = this.cliente.id;

        this.getInfoCliente(this.cliente.erp, cliente_id).then((cliente: any)=>{
            
            forma_pago_erp = cliente.forma_de_pago ? cliente.forma_de_pago : cliente.id_pago ? cliente.id_pago : '999999' ;    
            transporte_manual_erp = cliente.transporte ? cliente.transporte : cliente.id_transporte ? cliente.id_transporte : '999999' ;    

            forma_pago = cliente.nombre_pago ? cliente.nombre_pago : 'ELEGIR'    
            transporte_manual = cliente.nombre_transporte ? cliente.nombre_transporte : 'ELEGIR';
          
            descuento = cliente.descuento ? cliente.descuento :0;
            observaciones = cliente.observaciones ? cliente.observaciones : '';

            cliente_web_id  = cliente.web_id ? cliente.web_id : 0;

            if (this.cliente.id == 0 ) { this.cliente.id  = cliente.id; };

            let query = "INSERT INTO pedidos (dispositivo, cliente_id, cliente_erp, cliente_web_id, usuario_email, vendedor_erp, vendedor_email, descuento, observaciones, web_id, estado, total, enviado, fecha, transporte_manual_erp, forma_pago_erp, transporte_manual, forma_pago,  de_sistema, de_sistema_tipo, fecha_entrega) VALUES (?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?,? ,?,? ,? )";            
            
            let params = [ 
                device.uuid,
                this.cliente.id ? this.cliente.id : cliente.id,
                this.cliente.erp ? this.cliente.erp : cliente.codigo_erp, 
                cliente_web_id,
                usuario_email,
                vendedor_erp,
                '-',
                descuento,
                observaciones,
                0,
                estado,
                0,
                enviado,
                fecha,
                transporte_manual_erp,
                forma_pago_erp,
                transporte_manual,
                forma_pago,
                'N',
                '-',
                fecha_entrega
                ];

            this.insertCabecera(query,params).then((res : any)=>{
                // this.pedido.id = res;
                if (res) {
                    
                    this.pedido.id = res;
                    this.titulo.linea_2 = (this.pedido.id && this.pedido.id ==0 ) ?  ((this._dataLoad.length+this._dataItems.length)+' items / Nuevo pedido : -') : ((this._dataLoad.length+this._dataItems.length)+' items / Modif pedido : '+this.pedido.id);
                    
                    this.eventGPS('NUEVO-PEDIDO-'+res);
                    this.insertDetalle(res,producto,result).then((res)=>{

                        resolve('OK')
                    })
                }
                
            })

        })

        }else{

            if (producto.cantidad > 0) {
                // editar producto
                this.modifDetalle(this.pedido.id,producto.codigo_erp,result).then((res)=>{
                  resolve('OK')
                })
            }else{
                // agregar producto
                this.insertDetalle(this.pedido.id,producto,result).then((res)=>{
                    resolve('OK')
                })
            }
        }
     })

    }

    private insertCabecera(query, params){
     return new Promise((resolve, reject) => {                           
         this.serviceDB.query(query,params).then((res)=>{
              resolve(res)
          }).catch((err)=>{
              resolve('ERROR')
          })
     })
    }    

     private modifDetalle(id, erp,datos){
     return new Promise((resolve, reject) => {                           
        let query = "UPDATE pedidos_detalle SET cantidad =?,  descuento =? WHERE pedido_id = ? and codigo_erp=?"
        
       
        let params = [ 
            datos.cantidad,
            datos.descuento,
            id,
            erp
            ];


         this.serviceDB.query(query, params).then((res)=>{
                resolve('OK')
          }).catch((err)=>{
              resolve('ERROR')
          })
     })
    }

    private insertDetalle(id,p,datos){
        return new Promise((resolve, reject) => {                              
            let query = "INSERT INTO pedidos_detalle (pedido_id, codigo_erp, nombre, precio_unitario, unidad_venta, cantidad, descuento, iva, subtotal, de_sistema) VALUES (?,?,?, ?,?,?, ?,?,? ,? )";
            
            let subtotal = (p.precio * datos.cantidad)-(p.precio * datos.cantidad*datos.descuento/100)
            
            let params = [ 
                id,
                p.codigo_erp,
                p.nombre,
                p.precio,
                p.unidad_venta,
                datos.cantidad,
                datos.descuento,
                p.iva,
                subtotal,
                p.de_sistema
                ];

             
             
             this.serviceDB.query(query, params).then((res)=>{
                  resolve('OK')
              }).catch((err)=>{
                  resolve('ERROR')
              })
        })
    }

    private eliminarCarrito(){
        
    }

    private consultaTotal() {
        // this.dbWorker = this.workerService.initDatabaseWorker();

           let query_1 = " SELECT p.id, ";
               // query_1 += " (SUM(pd.cantidad * pd.precio_unitario))  AS subtotal_sin_descuento, ";
    
               query_1 += " ( (SUM(pd.cantidad * pd.precio_unitario) )  ";
               query_1 += " - (SUM(pd.cantidad * pd.precio_unitario * IFNULL(pd.descuento,0)/100) ) )  ";
               query_1 += " - ";
               query_1 += " ( ( (SUM(pd.cantidad * pd.precio_unitario) )  ";
               query_1 += " - (SUM(pd.cantidad * pd.precio_unitario * IFNULL(pd.descuento,0)/100) ) ";
               query_1 += " ) *  IFNULL(p.descuento,0)/100) ";
               query_1 += " AS subtotal_con_descuento, ";
                    
               query_1 += " COUNT(pd.id) AS items, ";
               query_1 += " SUM(pd.cantidad) AS items_suma, ";

               // query_1 += " ROUND(SUM(pd.cantidad * pd.precio_unitario*pd.iva/100),2) AS iva_sin_descuento, " ;
               query_1 += " ROUND( SUM( pd.cantidad * pd.precio_unitario * pd.iva/ 100), 2) AS iva_sin_descuento, " ;

               query_1 += " p.descuento FROM pedidos_detalle AS pd INNER JOIN pedidos  AS p ON pd.pedido_id = p.id WHERE p.id = ? GROUP BY pd.pedido_id order by p.id DESC ";
             
               let query_2 = " SELECT COUNT(pd.id) AS cant_sistema FROM pedidos_detalle AS pd INNER JOIN pedidos AS p ON pd.pedido_id = p.id WHERE p.id = ? AND pd.de_sistema = 'S' GROUP BY pd.pedido_id order by p.id DESC ";

               let params = [this.pedido.id];
 
              if (this.pedido.id && this.pedido.id != 0) {  
                  
                    this.dbWorker.postMessage({ type: 'get', query: query_1, params: params });
                    const that = new WeakRef(this);

                    this.dbWorker.onmessage = (res) => {
                      that.get().ngZone.runOutsideAngular(()=>{

                      let status = res.data.success;
                      let row = res.data.row;

                                  if (!status) {
                                      this.activity = false;
                                  }
                                  
                                  if (!row) {
                                    that.get().pedido.total = 0;
                                    that.get().pedido.iva = 0;
                                    return;
                                  }
                                  
                                  that.get().pedido.descuento = Number(row.descuento.toFixed(2));

                                  that.get().pedido.cantidad = row.items;
                                  that.get().pedido.cantidad_items = row.items_suma;
                                  that.get().pedido.subtotal_con_descuento = Number(row.subtotal_con_descuento.toFixed(2));
                                   
                                  // that.get().pedido.iva =  Number((row.subtotal_con_descuento*21/100).toFixed(2))
                                  that.get().pedido.iva =  Number((row.iva_sin_descuento).toFixed(2));
                                  
                                  let total = Number(that.get().pedido.subtotal_con_descuento) + Number(that.get().pedido.iva);
                                  that.get().pedido.total =  Number(total.toFixed(2));
                                    
                                  that.get().serviceDB.get(query_2, params).then((row_2: any)=>{
                                     that.get().de_sistema = (row_2 && row_2.cant_sistema && row_2.cant_sistema > 0) ? true : false;
                                  });

                                  that.get().ngZone.run(()=>{
                                    
                                  })

                      })            
                     }

                      this.dbWorker.onerror = function(err) {
                                console.log(`An unhandled error occurred in worker: ${err.filename}, line: ${err.lineno} :`);
                                console.log(err.message);
                      }
               }
    }

    public emitNoCompra(){  
      if (this.activity) {
                return; 
      }   

      if (!this.de_sistema && this.pedido.cantidad == 0 && this.p_nocompra.length > 0) {
           this.popNoCompra();
      }else{
        let options = { title: "ATENCIÓN", message: "Al parecer no posee motivos de no compra cargados o bien posee items en su pedido.", okButtonText: "OK"};
        alert(options)
      }
    }

    public emitLoadAllProducts() {
            if (this.activity) {
                      return; 
            }   


            let disponible = 0;
            this._dataLoad.forEach((p)=>{
              disponible =  p.disponible + 1;
            })
            this._dataItems.forEach((p)=>{
              disponible =  p.disponible + 1;
            })

            if (disponible && disponible==0) {
              alert('No hay productos disponibles para descargar stock.')
              return;
            }
            
            /*Borro pedido detalle y pedido si se creo uno anteriormente*/
            if (this.pedido.id != 0) {
                alert("Posee items previamente cargados en su pedido, borrelos y realice la descarga de stock nuevamente.");
                return;
            }

            this.activity = true;
            let fecha = new Date();

            let dia =  fecha.getDate()+1;
            let mes =  fecha.getMonth()+1;
            let anio = fecha.getFullYear();

            let fecha_entrega = dia + '/' + mes + '/' + anio;
            let query_cabecera = "INSERT INTO pedidos (dispositivo, cliente_id, cliente_erp, cliente_web_id, usuario_email, vendedor_erp, vendedor_email, descuento, observaciones, web_id, estado, total, enviado, fecha, transporte_manual_erp, forma_pago_erp, transporte_manual, forma_pago,  de_sistema, de_sistema_tipo, fecha_entrega) VALUES (?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?,? ,?,? ,? )";            
            
            let params = [ 
                device.uuid,
                this.cliente.id ? this.cliente.id : 0,
                this.cliente.erp ? this.cliente.erp : '', 
                0,
                '',
                getString("vendedor_erp") ? getString("vendedor_erp") : '',
                '-',
                0,
                'DESCARGA DE STOCK',
                0,
                'PENDIENTE',
                0,
                0,
                fecha,
                '999999',
                '999999',
                'ELEGIR',
                'ELEGIR',
                'N',
                '-',
                fecha_entrega
                ];

                this.insertCabecera(query_cabecera, params).then((res)=>{

                if (!res) {
                  this.activity = false;
                  return;
                }

                /*armo query para copiar todo los productos*/
                let query = ' INSERT INTO pedidos_detalle (id, pedido_id, codigo_erp, nombre, precio_unitario, unidad_venta, cantidad, descuento, iva, subtotal, de_sistema) ';

                /* asociar pedido-id */
                query += ' SELECT p.id AS id, '+res+' AS pedido_id, p.codigo_erp, p.nombre, ';

                /* asociar Lista de precio */
                let precio = '';
                if (!this.cliente.precio_lista) {
                    precio = 'p.precio_'+1;
                }else{
                    precio = isNaN(this.cliente.precio_lista) ? 'p.precio_1'  : 'p.precio_'+Number(this.cliente.precio_lista);
                }
               
                query += precio+' AS precio_unitario, ';
                query += ' IFNULL(p.unidad_venta, 1) AS unidad_venta, ';
                query += ' p.stock - IFNULL(pd_reservado.cantidad,0) AS cantidad, '
                query += ' 0 AS descuento, '
                query += ' IFNULL(p.iva,21) AS iva, '
                query += ' ( ('+precio+' * p.stock) - ('+precio+' * IFNULL(pd_reservado.cantidad,0)))  AS cantidad, ';
                query += ' "N" as de_sistema ';
                query += ' FROM productos AS p LEFT JOIN pedidos_detalle AS pd_reservado ON p.codigo_erp = pd_reservado.codigo_erp ';
                query += ' WHERE p.codigo_erp IS NOT NULL AND p.de_sistema = "N" AND p.stock > pd_reservado.cantidad OR pd_reservado.cantidad IS NULL ; ';

                this.serviceDB.query(query).then((res: any)=>{
                      this.activity = false;
                      this.routerExtensions.navigate(['pedidos', this.cliente.id, this.cliente.erp, this.cliente.nombre, this.cliente.precio_lista, this.cliente.tipo_id], { clearHistory: false});  
                }).catch((e)=>{
                      console.dir(e)
                })

        
              })
    
    }

    public confirmarPedido(){

         if (this.activity) {
                return;
         }
         
         if (!this.de_sistema && this.pedido.cantidad == 0 && this.p_nocompra.length > 0) {
           this.popNoCompra();
         }else{
          this.routerExtensions.navigate(['pedidos', this.cliente.id, this.cliente.erp, this.cliente.nombre, this.cliente.precio_lista, this.cliente.tipo_id], { clearHistory: false});  
         }
    };
   
    public openPopDetalle(codigo_erp){
         if(this.activity){
              return;
         }

         this.getInfoProducto(codigo_erp).then( p =>{
           if (p && p != 'ERROR') {
             this.createModel(p, true, 'detalle').then(result => {
                this.openModal = false;
            })
           }
         }).catch((e)=>{
           console.dir(e);
           console.log('Error: GetInfoProducto')     
         })     
    }

    private getInfoProducto(erp){
       return new Promise((resolve, reject) => {             
            let params = [erp]
             let fotos_online = (typeof(getBoolean('fotos_online')) === 'boolean' ) ? getBoolean('fotos_online') : true;

            let query = 'SELECT p.id, p.codigo_erp,  p.destacado,  p.descripcion, p.nombre, p.orden,  p.sin_foto, p.foto_thumbnail, p.foto_medium, p.foto_big, p.foto, p.activo, p.stock, p.unidad_venta, p.iva, ';
            let query_2 = "SELECT f.id, f.nombre, '' AS value FROM Filtros as f "
            
            /* asociar Lista de precio */
            let precio = '';
            if (!this.cliente.precio_lista) {
              precio = 'p.precio_'+1;
            }else{
              precio = isNaN(this.cliente.precio_lista) ? 'p.precio_1'  : 'p.precio_'+Number(this.cliente.precio_lista);
            }
            
            query += ' p.stock - IFNULL(pd_reservado.cantidad,0) AS disponible, '
            query += ' IFNULL(fv_1.nombre, "") AS filtro_1, ';
            query += ' IFNULL(fv_2.nombre, "") AS filtro_2, ';
            query += ' IFNULL(fv_3.nombre, "") AS filtro_3, ';
            query += ' IFNULL(fv_4.nombre, "") AS filtro_4, ';
            query += ' IFNULL(fv_5.nombre, "") AS filtro_5, ';
            query += ' IFNULL(fv_6.nombre, "") AS filtro_6, ';

            query += precio+' AS precio '
            query += ' FROM productos AS p ';   
            query += ' LEFT JOIN filtros_valores AS fv_1 ON (fv_1.id = p.filtro_1 )   ';   
            query += ' LEFT JOIN filtros_valores AS fv_2 ON (fv_2.id = p.filtro_2 ) ';   
            query += ' LEFT JOIN filtros_valores AS fv_3 ON (fv_3.id = p.filtro_3 ) ';   
            query += ' LEFT JOIN filtros_valores AS fv_4 ON (fv_4.id = p.filtro_4 ) ';   
            query += ' LEFT JOIN filtros_valores AS fv_5 ON (fv_5.id = p.filtro_5 ) ';   
            query += ' LEFT JOIN filtros_valores AS fv_6 ON (fv_6.id = p.filtro_6 ) ';   
            query += ' LEFT JOIN pedidos_detalle AS pd_reservado ON p.codigo_erp = pd_reservado.codigo_erp ';

            query += ' WHERE p.codigo_erp = ? ';   
            query += ' GROUP BY p.codigo_erp ';  

           this.serviceDB.all(query_2, []).then((filtros: any)=>{  
             
             this.serviceDB.get(query, params).then((row: any)=>{    
                 row.precio = row.precio.toFixed(2);
                 
                if (!fotos_online) {
                        let foto = row.foto.split("/");
                        row.foto_thumbnail = getString('path_fotos') +'/'+ foto[1];
                        row.foto_medium = getString('path_fotos') +'/'+ foto[1];
                        row.foto_big = getString('path_fotos') +'/'+ foto[1];
                }
               
                  row.filtros = filtros;
                  resolve(row);
                
                }).catch((err)=>{
                   reject('ERROR')
                })

           }).catch((err)=>{
               reject('ERROR')
           })
       })
    }



    public popCabecera(){
    
    if(this.activity){
      return;
    }


    if (this.pedido.id != 0) {
    
    let cliente_id = this.cliente.id;
    this.getTransportes().then((transportes: any)=>{    
              console.log("get-pagos:")
              console.dir(transportes);
              this.getPagos().then((pagos: any)=>{
                console.log("get-info:")
                this.getInfoCliente(this.cliente.erp, cliente_id).then((datos: any)=>{
                  console.log("get-cabecera:")
                  if (pagos != 'ERROR') {
                      this.getPcabecera().then((pedido: any)=>{
                           if (pedido != 'ERROR') {
                             
                             pedido.cliente = datos;
                             pedido.pagos_array = pagos ? pagos : [];
                             pedido.transportes_array = transportes ? transportes : [];
                             
                             if (pedido) {
                               
                             this.createModel(pedido, true, 'cabecera').then((res: any)=>{
                                   this.openModal = false;
                                   if (res) {
                                     this.aplicarCabecera(res);
                                   }
                             })  

                             }
                             
                          }
                      })      
                  }
                 })  
              })
            })
              
      }else{  
        let options = { title: "ATENCIÓN", message: "Debe agregar aunque sea un producto a su pedido.", okButtonText: "OK"};
        alert(options)
      }

    }

    private getPcabecera(){
       return new Promise((resolve, reject) => {  
            let params = [this.pedido.id]
            let query = "SELECT p.* FROM pedidos AS p WHERE id=?";           
            this.serviceDB.get(query, params).then((row: any)=>{
               resolve(row);
            }).catch((res)=>{
               reject('ERROR')
           })
       })
    }

    private getPagos(){
       return new Promise((resolve, reject) => {             
           let query = "SELECT * FROM pagos";
           let params = [];
           let pagos = [];
           this.serviceDB.all(query, params).then((rows: any)=>{    
               rows.forEach(row => {pagos.push(row)})
               resolve(pagos);
           }).catch((res)=>{
               reject('ERROR')
           })
       })
    }

    private getTransportes(){
       return new Promise((resolve, reject) => {             
           let query = "SELECT * FROM transportes";
           let params = [];
           let pagos = [];
           this.serviceDB.all(query, params).then((rows: any)=>{    
               rows.forEach(row => {pagos.push(row)})
               resolve(pagos);
           }).catch((res)=>{
               reject('ERROR')
           })
       })
    }

    public popBorrarProd(){
                if (this.pedido.id != 0) {
                    
                    let options = {
                        title: "ELIMAR PRODUCTOS",
                        message: "Esta seguro que desea eliminar todos los productos agregados.",
                        okButtonText: "Si",
                        cancelButtonText: "No",
                        neutralButtonText: "Cancelar"
                    };

                        confirm(options).then((res: boolean) => {
                            if (res) {
                                this.deleteAll();
                            }
                        });

                    }
    }

    private aplicarCabecera(datos){
        console.dir('aplicarCabecera')
        console.dir(datos)
        let query_prod_del = 'UPDATE pedidos SET fecha_entrega=?, transporte_manual=?, forma_pago=?, transporte_manual_erp=?, forma_pago_erp=?, descuento=?, observaciones=?, desde_hs=?, hasta_hs=?  WHERE id =?';
        
        let f = new Date(datos.fecha_entrega);

        let dia =  f.getDate();
        let mes =  f.getMonth()+1;
        let anio = f.getFullYear();

        let string = dia + '/' + mes + '/' + anio;
        
        let params = [
            string,
            datos.transporte_manual,
            datos.forma_pago,
            datos.transporte_manual_erp,
            datos.forma_pago_erp,
            datos.descuento,
            datos.observaciones,
            datos.desde_hs,
            datos.hasta_hs,
            this.pedido.id
        ];
        
        this.serviceDB.query(query_prod_del, params).then((res: any)=>{
               this.inicializarProductos(false, ' GROUP BY p.codigo_erp order BY p.orden ASC ');
               this.resetViewActive()
        }) 
    }

    private deleteAll(){
        let query_prod_del = 'DELETE FROM pedidos_detalle WHERE pedido_id =?';
        let params = [this.pedido.id];
        
        this.serviceDB.query(query_prod_del, params).then((res: any)=>{
             this.inicializarProductos(false, ' GROUP BY p.codigo_erp order BY p.orden ASC ');
             this.resetViewActive()
        })                              
    }
    
    public popNoCompra(){
        let opciones :any= [];
        
        this.p_nocompra.forEach(p => { opciones.push(p.nombre) })
        
        opciones.push('CANCELAR PEDIDO');

        action("Seleccionar motivo", "CANCELAR", opciones).then(res => {  
                if (res) {

                    if (res.toUpperCase() == 'CANCELAR') {
                       return;
                    }

                    if (res.toUpperCase() == 'CANCELAR PEDIDO') {
                      this.routerExtensions.navigate(['pedidos', this.cliente.id, this.cliente.erp, this.cliente.nombre, this.cliente.precio_lista, this.cliente.tipo_id], { clearHistory: false});  
                    }

                    this.p_nocompra.forEach(p =>{
                      
                      if (p.nombre.toUpperCase().trim() == res.toUpperCase().trim()) {
                        let result = { cantidad: 1 , descuento: 0 };
                        p.precio = 0;
                        
                        if (this.pedido.total>0) {
                             this.deleteAll();
                        }

                        this.agregarEditarCarrito(p, result).then((res)=>{
                          if (res == 'OK') {
                              this.de_sistema = true;
                              this.routerExtensions.navigate(['pedidos', this.cliente.id, this.cliente.erp, this.cliente.nombre, this.cliente.precio_lista, this.cliente.tipo_id], { clearHistory: false});  
                          }
                        })
                      }
                    })
                  }
            });
    }


    private createModel(item: any, fullscreen: boolean, type: string): Promise<any> {
        if(!this.openModal){
            this.openModal = true;
            
            let options: ModalDialogOptions = {
                viewContainerRef: this.vcRef,
                context: item,
                fullscreen: fullscreen
            };
           
            if (type == 'pop') {
                if (isAndroid) {
                  return this.modalService.showModal(modalPopAndroid, options);
                }else{
                  return this.modalService.showModal(modalPopIos, options);
                }
              }

            if(type == 'cabecera'){
                  return this.modalService.showModal(modalCabecera, options);
              }

            if(type == 'detalle'){
                  return this.modalService.showModal(modalDetalle, options);
              }

            if(type == 'filtro'){
                  return this.modalService.showModal(modalFiltrador, options);
            }

            if(type == 'buscar'){
                  return this.modalService.showModal(modalBuscador, options);
            }

            if(type == 'image'){
                return this.modalService.showModal(modalImage, options);
            }
            
        }
    }

    eventGPS(tipo){
        // this.dbWorker = this.workerService.initDatabaseWorker();
        
        let asociado_erp = getString("asociado_erp") ? getString("asociado_erp") : ("sin-erp-"+getString("asociado_id")); 

        let email =  getString("email") ? getString("email") : '';
        let lat = this._gpsservice.latitude;
        let long =  this._gpsservice.longitude;
        let ts =  this._gpsservice.timestamp;
        let params = [ asociado_erp, email, tipo, lat, long, ts ];
        let query = ' INSERT INTO gps (usuario_erp, usuario_email, tipo, lat, long, timestamp) VALUES (?,?,?,?,?,?) ';                 
        
        const that = new WeakRef(this);
        this.dbWorker.postMessage({ type: 'query', query: query, params: params });
        this.dbWorker.onmessage = (res) => {};
    }

   
    /* MULTIPLE LAYOUTS */
    public changeView(args){

      switch (this._viewActive.name) {
        case "list":
          this._viewActive = { url: 'res://ic_grid_white', name:'grid',  android: ( isAndroid ? true : false )   };
          this.flexDirections = 'column';
          this.changeToGrid(args);
          break;
        case "grid":
          this._viewActive = { url: 'res://ic_stagged_white', name:'stagged',  android: ( isAndroid ? true : false )   };
          this.flexDirections = 'column';
          this.changeToStaggered(args);
          break;
        case "stagged":
          this._viewActive = { url: 'res://ic_list_white', name:'list',  android: ( isAndroid ? true : false )   };
          this.flexDirections = 'row';
          this.changeToLinear(args);
          break;
        default:
          console.log('default');
          break;
      }

    }

    public changeToLinear(args) {
        //list-one
        try{
          const layout = new ListViewLinearLayout();

                layout.itemHeight = 120;

                layout.itemWidth = this.widthScreen;

          this.ngZone.runOutsideAngular(()=>{
            this.listViewComponent.listView.listViewLayout = layout;

            this.ngZone.run(()=>{
              console.log('change-view')
            })
          })
          
        }catch{
          console.log('error: changeToLinear');
        }
    }

    public changeToGrid(args) {
        //grid-one
        try{
        
        const layout = new ListViewGridLayout();

              layout.itemHeight = 530;

              layout.itemWidth = this.widthScreen;
              layout.rowSpan = 1;
              layout.spanCount = 1;

        this.ngZone.runOutsideAngular(()=>{
          this.listViewComponent.listView.listViewLayout = layout;

          this.ngZone.run(()=>{
              console.log('change-view')
          })
        })
        


        }catch{
          console.log('error: changeToGrid');
        }
    }

    public changeToStaggered(args) {
        //stagged-two
        try{
        
        const layout = new ListViewStaggeredLayout();
              layout.spanCount = 2;
        
        this.ngZone.runOutsideAngular(()=>{
          this.listViewComponent.listView.listViewLayout = layout;

          this.ngZone.run(()=>{
              console.log('change-view')
          })
        })

        

        }catch{
          console.log('error: changeToStaggered');
        }
    }

}
