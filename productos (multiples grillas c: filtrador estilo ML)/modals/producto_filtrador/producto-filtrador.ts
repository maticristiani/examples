import { Component, OnInit, AfterContentInit } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { DatabaseService } from "../../../providers/database/database";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { Slider } from "tns-core-modules/ui/slider";


// >> passing-parameters
@Component({
    moduleId: module.id,
    templateUrl: "./producto-filtrador.html"
})

export class modalFiltrador implements AfterContentInit {
    
   
   
   public filtros: any;
   public enabledButton: boolean;
   public ordenarPor: string;
   public activo: boolean;

   renderView = false;
   renderViewTimeout: any;


    constructor(
       private _params: ModalDialogParams,
       private serviceDB: DatabaseService) { 
        this.enabledButton = false;
    }

    ngAfterContentInit() {
      this.renderViewTimeout = setTimeout(() => {
        this.renderView = true;             

      if (this._params.context.filtros.length === 0) {
          
          this.filtros = [];

          this.inicializarFiltros().then( (filtros: any) => {
           this.filtros = filtros;
         });               

      }else{
        
        //primero activos
        this._params.context.filtros.forEach((f,index)=>{
          f.valores.sort((x, y) => { return (x.activo === y.activo)? 0 : x.activo? -1 : 1; });
          this.filtros = this._params.context.filtros;
        })

        this.enabledButton = true;

      }

      
      this.ordenarPor = this._params.context.orden ? this._params.context.orden : '';
      this.activo = this._params.context.activo ? this._params.context.activo : false;


      }, 200)
    }

    setSlider(slider: Slider) {
      slider.scaleY = 2;
    }

    public submitModal() {
        let res = { activo: this.activo, filtros: this.filtros, orden: this.ordenarPor }
        this._params.closeCallback(res);
    }
    
    public closeModal() {
      this._params.closeCallback(null);
    }


    setButtonOrden(value) {
      this.ordenarPor = value;
      this.enabledButton = true;
    }

    setFiltro(array, item) {
      
      array.forEach((v)=>{
        v.activo = false;
      })

      item.activo = true;
      this.enabledButton = true;
    
    };


    limpiarFiltros(){
        //reset-values-activos
        this.ordenarPor = '';

        this.filtros.forEach((f,index)=>{
          f.valores.forEach((v)=>{
             v.activo = false;
          })
        })

        this.enabledButton = false;
        this.activo = false;

    }
 
    /*definir filtros*/
    private getCabeceras(query_filtro, params){
        return new Promise((resolve, reject) => { 
            this.serviceDB.all(query_filtro, params).then((rows: any)=>{
                resolve(rows);
            })
      })   
    }

    
    private getValoresQuerys(id){
        return new Promise((resolve, reject) => {
            let query_valor = 'SELECT fv.id, fv.nombre, fv.query, fv.orden FROM filtros_valores AS fv WHERE fv.filtro_id= ? GROUP BY fv.id ORDER BY fv.nombre';
            let params= [id];

            this.serviceDB.all(query_valor, params).then((rows: any)=>{
                resolve(rows)
            })

      })   
    }

    private getValores(id){
        return new Promise((resolve, reject) => {
            let query_valor = 'SELECT fv.id, fv.nombre, fv.query, fv.orden FROM filtros_valores AS fv INNER JOIN productos ON fv.id = productos.filtro_'+id+' WHERE fv.filtro_id= ? GROUP BY fv.id ORDER BY fv.nombre';
            let params= [id];

            this.serviceDB.all(query_valor, params).then((rows: any)=>{
                resolve(rows)
            })

      })   
    }

    private inicializarFiltros(){
      return new Promise((resolve, reject) => {
     
        let query_filtro = 'SELECT id, nombre, query, orden FROM filtros';
        let params = [];
     
        this.getCabeceras(query_filtro, params).then((rows:any)=>{
            
            rows.forEach((row, index) => {
                   this.filtros.push({ 'id' : row.id, 'nombre' : row.nombre, 'query' : row.query, 'orden' : row.orden, 'valores': [] });
                   let res = this.inicializarFiltrosValores(row,index);
            })

            resolve(this.filtros)
        })
        
      })
    }

    private inicializarFiltrosValores(f, index){
        return new Promise<any>(resolve => {
           if(f.query && f.query === 'S'){

             this.getValoresQuerys(f.id).then((rows:any)=>{
               rows.forEach(row => {
                 this.filtros[index].valores.push({ 'id' : row.id, 'nombre' : row.nombre, 'query' : row.query, 'orden' : row.orden, 'activo': false });                                    
               })
               resolve(true);
             })
           
           }else{
             
             this.getValores(f.id).then((rows:any)=>{   
                              if (rows.length > 0) {
                                this.filtros[index].valores.push({ 'id' : 'todos', 'nombre' : 'Todos', 'activo': false });                                       
                                rows.forEach(row => {
                                   this.filtros[index].valores.push({ 'id' : row.id, 'nombre' : row.nombre, 'query' : row.query, 'activo': false });                                    
                                })
                              }
                              resolve(true);
             })

          }
       })
    }

}
