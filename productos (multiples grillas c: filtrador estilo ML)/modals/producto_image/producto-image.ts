import { Component, OnInit, ViewChild, AfterContentInit, ElementRef } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { getString } from "tns-core-modules/application-settings";
import {GestureEventData, PanGestureEventData, PinchGestureEventData} from "tns-core-modules/ui/gestures";
import { Page } from "tns-core-modules/ui/page";
import { View } from "tns-core-modules/ui/core/view";
import { TextField } from "tns-core-modules/ui/text-field";
import { alert } from "tns-core-modules/ui/dialogs";
import { WebImage } from "nativescript-web-image-cache";
var utils = require("tns-core-modules/utils/utils"); 
declare var CGAffineTransformMakeScale;
declare var UIImageView;
let states = ["unknown", "start", "change", "end"];


let density: number;
let prevDeltaX: number;
let prevDeltaY: number;
let startScale = 1;


@Component({
    moduleId: module.id,
    templateUrl: "./producto-image.html",
})

export class modalImage implements OnInit, AfterContentInit{

	@ViewChild('webimage', {static: false})  webImage: ElementRef;
    public url: string;

    renderView = false;
    renderViewTimeout: any;


    constructor(
      private _params: ModalDialogParams,
      private page: Page) {
    }
    
    ngOnInit() {
    	this.url = this._params.context;
    }

    ngAfterContentInit() {
    	this.renderViewTimeout = setTimeout(() => {
             this.renderView = true;             
			    	// console.dir(this.webImage.nativeElement);
				    try{

					    this.webImage.nativeElement.translateX = 0;
					    this.webImage.nativeElement.translateY = 0;
					    this.webImage.nativeElement.scaleX = 1;
					    this.webImage.nativeElement.scaleY = 1;
					}catch{
						// console.log('err-set-1')
					};

					// try{
					//     this.webImage.nativeElement.UIImageView.translateX = 0;
					//     this.webImage.nativeElement.UIImageView.translateY = 0;
					//     this.webImage.nativeElement.UIImageView.scaleX = 1;
					//     this.webImage.nativeElement.UIImageView.scaleY = 1;
					// }catch{
					// 	 console.log('err-set-2')
					// };
		}, 300);
    }

    public close() {
      setTimeout(() => {
                this._params.closeCallback();
      }, 100);
    }

    onDoubleTap(args: GestureEventData) {
    console.log("DOUBLETAP");

	    try{
	    		let image_1 =  this.webImage.nativeElement;
		        image_1.animate({
				        translate: { x: 0, y: 0 },
				        scale: { x: 1, y: 1 },
				        curve: "easeOut",
				        duration: 300
				    }).then(function () {
				       console.log('animate1')
				    });
		}catch{console.log('err-pa-1')}
	    	
	 //    try{
	 //    		let image_2 =  this.webImage.nativeElement.UIImageView;
		//         image_2.animate({
		// 		        translate: { x: 0, y: 0 },
		// 		        scale: { x: 1, y: 1 },
		// 		        curve: "easeOut",
		// 		        duration: 300
		// 		    }).then(function () {
		// 		       console.log('animate-2')
		// 		    });
		// }catch{console.log('err-pa-1')}

	}

    onPan(args: PanGestureEventData) {
    console.log("PAN[" + states[args.state] + "] deltaX: " + Math.round(args.deltaX) + " deltaY: " + Math.round(args.deltaY));

	    if (args.state === 1) {
	        prevDeltaX = 0;
	        prevDeltaY = 0;
	    }else if (args.state === 2) {
	    	
	    	try{
	    		let image_1 = this.webImage.nativeElement;
		        image_1.translateX += args.deltaX - prevDeltaX;
		        image_1.translateY += args.deltaY - prevDeltaY;
			}catch{console.log('err-pa-1')}
	    	
	  //   	try{
	  //   		let image_2 =  this.webImage.nativeElement.UIImageView;
		 //        image_2.translateX += args.deltaX - prevDeltaX;
		 //        image_2.translateY += args.deltaY - prevDeltaY;
			// }catch{console.log('err-pa-1')}
	        
	        prevDeltaX = args.deltaX;
	        prevDeltaY = args.deltaY;
	    }
	
	}


    onPinch(args: PinchGestureEventData) {
    	    console.log("PINCH[" + states[args.state] + "] scale: " + args.scale + " focusX: " + args.getFocusX() + " focusY: " + args.getFocusY());
    	    // let transform = CGAffineTransformMakeScale(newScale, newScale)
  	
    	    try{
				let image_1 = this.webImage.nativeElement;
				// image_1.transform = transform;
				 if (args.state === 1) {
			        const newOriginX = args.getFocusX() - image_1.translateX;
			        const newOriginY = args.getFocusY() - image_1.translateY;

			        const oldOriginX = image_1.originX * image_1.getMeasuredWidth();
			        const oldOriginY = image_1.originY * image_1.getMeasuredHeight();

			        image_1.translateX += (oldOriginX - newOriginX) * (1 - image_1.scaleX);
			        image_1.translateY += (oldOriginY - newOriginY) * (1 - image_1.scaleY);

			        image_1.originX = newOriginX / image_1.getMeasuredWidth();
			        image_1.originY = newOriginY / image_1.getMeasuredHeight();

			        startScale = image_1.scaleX;
			    
			    }
				else if (args.scale && args.scale !== 1) {
			        let newScale = startScale * args.scale;
			        newScale = Math.min(8, newScale);
			        newScale = Math.max(0.125, newScale);

			        image_1.scaleX = newScale;
			        image_1.scaleY = newScale;
			    }


			}catch{
				console.log('e1')
			}
			
			// try{
			// 	let image_2 = this.webImage.nativeElement.UIImageView;
			// 	// image_2.transform = transform;
			// 	 if (args.state === 1) {
			//         const newOriginX = args.getFocusX() - image_2.translateX;
			//         const newOriginY = args.getFocusY() - image_2.translateY;

			//         const oldOriginX = image_2.originX * image_2.getMeasuredWidth();
			//         const oldOriginY = image_2.originY * image_2.getMeasuredHeight();

			//         image_2.translateX += (oldOriginX - newOriginX) * (1 - image_2.scaleX);
			//         image_2.translateY += (oldOriginY - newOriginY) * (1 - image_2.scaleY);

			//         image_2.originX = newOriginX / image_2.getMeasuredWidth();
			//         image_2.originY = newOriginY / image_2.getMeasuredHeight();

			//         startScale = image_2.scaleX;
			    
			//     }
			// 	else if (args.scale && args.scale !== 1) {
			//         let newScale = startScale * args.scale;
			//         newScale = Math.min(8, newScale);
			//         newScale = Math.max(0.125, newScale);

			//         image_2.scaleX = newScale;
			//         image_2.scaleY = newScale;
			//     }


			// }catch{
			// 	console.log('e2')
			// }

	 
		}
		
  
}
