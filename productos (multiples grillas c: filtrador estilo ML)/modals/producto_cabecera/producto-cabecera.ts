import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";

import { isAndroid } from "tns-core-modules/platform";
import { action } from "tns-core-modules/ui/dialogs";
import { TextField } from "tns-core-modules/ui/text-field";
import { Page } from "tns-core-modules/ui/page";
import { RadCalendarComponent } from "nativescript-ui-calendar/angular";
import { RadCalendar, CalendarSelectionMode, CalendarEvent } from "nativescript-ui-calendar";
import { Color } from 'tns-core-modules/color';
 
const IOS_KEYBOARDTYPE_NUMBERPAD: number = 4;
declare var UIKeyboardTypeNumberPad;
declare var UIReturnKeyDone;


@Component({
    moduleId: module.id,
    templateUrl: "./producto-cabecera.html",
})


export class modalCabecera implements OnInit {
    
    public origen: any;

    public current: any = { 
        'fecha_entrega': '',
        'forma_pago': 'ELEGIR',
        'transporte_manual': 'ELEGIR',
        'forma_pago_erp': '999999',
        'transporte_manual_erp': '999999',
        'observaciones': '',
        'descuento': 0,
        'desde_hs': 9,
        'hasta_hs': 18
    }

    private _events: Array<CalendarEvent>;
    
    @ViewChild("myCalendar", {static: false})  _calendar: RadCalendarComponent;

    constructor(
      private _params: ModalDialogParams) {     
          
    }

    ngOnInit() {
        this.inicializarCabecera();
    }

     get eventSource() {
        return this._events;
    }

    public onFocus(args) {
      console.log('onFocus')

      let _Textfield = <TextField>args.object;
      
      if (isAndroid) {

          _Textfield.android.setSelectAllOnFocus(true);
          _Textfield.android.requestFocus();

          setTimeout(()=>{
            _Textfield.android.selectAll();  
          },100)
        
      }else{
        
          let _Textfield = <TextField>args.object;
              _Textfield.ios.keyboardType =  UIKeyboardTypeNumberPad;
              _Textfield.ios.returnKeyType = UIReturnKeyDone;
              _Textfield.ios.returnKeyType = UIReturnKeyDone;

          let startPosition: any = _Textfield.ios.beginningOfDocument
          let endPosition: any = _Textfield.ios.endOfDocument
          let selectedRange: any = _Textfield.ios.selectedTextRange
          
                
          setTimeout(()=>{
                  try{
                    _Textfield.ios.selectedTextRange = _Textfield.ios.textRangeFromPositionToPosition(startPosition, endPosition);  
                  }catch{console.log('error-selectAll()')}  
                  console.log('selectAll()')
          },100)

                // Shows the soft input method, ususally a soft keyboard.
      }

    }

    private inicializarCabecera(){
       

       console.log('origen:')
       this.origen = this._params.context;
       
       console.dir(this.origen);

       if (this.origen.fecha_entrega && this.origen.fecha_entrega.length > 2) {
           try{

           let d = this.origen.fecha_entrega;
           let fecha = d.split(',')

            if(fecha.length >= 2){
                console.log(fecha);
                let entrega = new Date(fecha[1]+'/'+fecha[0]+'/'+fecha[2]);

                this.current.fecha_entrega = entrega;
                console.log('seteada con , ')
                console.log(this.current.fecha_entrega)

                if (isNaN(this.current.fecha_entrega.getTime())) {
                  console.log('isInvalid ')
                  this.current.fecha_entrega = new Date();
                  this.current.fecha_entrega.setDate(this.current.fecha_entrega.getDate() + 1); 
                }

            }else{
                fecha = fecha[0].split('/');   
                console.log(fecha);

                let entrega = new Date(fecha[1]+'/'+fecha[0]+'/'+fecha[2]);

                this.current.fecha_entrega = entrega;
                console.log('seteada con / ')
                console.log(this.current.fecha_entrega)


                if (isNaN(this.current.fecha_entrega.getTime())) {
                  console.log('isInvalid ')
                  this.current.fecha_entrega = new Date();
                  this.current.fecha_entrega.setDate(this.current.fecha_entrega.getDate() + 1); 
                }
            }

           }catch(e){
               this.current.fecha_entrega = new Date();
               this.current.fecha_entrega.setDate(this.current.fecha_entrega.getDate() + 1); 

               console.log('seteada con newDate ')
               console.log(this.current.fecha_entrega)

           }

       }else{
           this.current.fecha_entrega = new Date();
           this.current.fecha_entrega.setDate(this.current.fecha_entrega.getDate() + 1);

           console.log('seteada con newDate ')
           console.log(this.current.fecha_entrega)
       }
       
       this.current.descuento =  this.origen.descuento ? this.origen.descuento  : this.origen.cliente.descuento ? this.origen.cliente.descuento : 0;       
       this.current.desde_hs =  this.origen.desde_hs ? this.origen.desde_hs : 9;
       this.current.hasta_hs =  this.origen.hasta_hs ? this.origen.hasta_hs : 18;
       this.current.observaciones =  this.origen.observaciones ? this.origen.observaciones : this.origen.cliente.observaciones ? this.origen.cliente.observaciones : '';
       
       //fecha-seleccion:
       if (this.current.fecha_entrega) {
         let startDate = new Date(this.current.fecha_entrega);
         let endDate = new Date(this.current.fecha_entrega);
         let event = new CalendarEvent("E", startDate, endDate, true, new Color(200, 188, 26, 214) )
         this._events = [];
         this._events.push(event);
       }

       //transporte-seleccion:
       // let forma_pago =  this.origen.forma_pago ? this.origen.forma_pago : this.origen.cliente.forma_de_pago ? this.origen.cliente.forma_de_pago : 'ELEGIR';
       // let transporte_manual = this.origen.transporte_manual ? this.origen.transporte_manual : this.origen.cliente.transporte ? this.origen.cliente.transporte : 'ELEGIR';
       
     

       this.current.forma_pago =  this.origen.forma_pago ? this.origen.forma_pago : this.origen.cliente.nombre_pago ? this.origen.cliente.nombre_pago : 'ELEGIR';
       this.current.transporte_manual = this.origen.transporte_manual ? this.origen.transporte_manual : this.origen.cliente.nombre_transporte ? this.origen.cliente.nombre_transporte : 'ELEGIR';
       
       this.current.forma_pago_erp =  this.origen.forma_pago_erp ? this.origen.forma_pago_erp : this.origen.cliente.forma_de_pago ? this.origen.cliente.forma_de_pago : '999999';
       this.current.transporte_manual_erp = this.origen.transporte_manual_erp ? this.origen.transporte_manual_erp : this.origen.cliente.transporte ? this.origen.cliente.transporte : '999999';
       
       if (!this.origen.forma_pago || this.origen.forma_pago.length == 0) {
           
         this.origen.pagos_array.forEach(p=>{
           if (p.id == this.current.forma_pago_erp) {
             this.current.forma_pago = p.nombre
           }
         })

         this.origen.transportes_array.forEach(t=>{
               if (t.id == this.current.transporte_manual_erp) {
                 this.current.transporte = t.nombre
               }
         })

         this.origen.pagos_array.forEach(p=>{
           if (p.codigo_erp == this.current.forma_pago_erp) {
               this.current.forma_pago = p.nombre;
           }
         })

         this.origen.transportes_array.forEach(t=>{
               if (t.codigo_erp == this.current.transporte_manual_erp) {
                 this.current.transporte = t.nombre;
               }
         })
        
        }
       console.dir(this.current);
    }

    public f_pago(){
        let opciones = [];
        
        this.origen.pagos_array.forEach(p=>{
            opciones.push(p.nombre)
        })

        action("Seleccionar metodo de pago", "CANCELAR", opciones).then(res => {  
                if (res && res != 'CANCELAR') {
                    this.current.forma_pago = res; 
                     this.origen.pagos_array.forEach(p=>{
                       if (p.nombre == res) {
                         this.current.forma_pago_erp = p.codigo_erp ? p.codigo_erp : p.id;  
                       }
                     })
                }

                console.log(this.current.forma_pago);
                console.log(this.current.forma_pago_erp);
        });


    }


     public f_transporte(){
        let opciones = [];
        
        this.origen.transportes_array.forEach(p=>{
            opciones.push(p.nombre)
        })

        action("Seleccionar metodo de pago", "CANCELAR", opciones).then(res => {  
                if (res && res != 'CANCELAR') {
                    this.current.transporte_manual = res; 

                    this.origen.transportes_array.forEach(t=>{
                           if (t.nombre == res) {
                             this.current.transporte_manual_erp = t.codigo_erp ? t.codigo_erp : t.id; 
                           }
                    })

                }    

                console.log(this.current.transporte_manual);
                console.log(this.current.transporte_manual_erp);
        });
    }

    public onDateSelected(args){
        // console.log(args.date)
        // console.log(this.current.fecha_entrega);
        // args.calendar.SelectionMode = 'Multiple';  
        // args.calendar.args.calendar.setDate(new Date(2018, 7, 12));
        // args.calendar.SelectedDates.Add(new Date(2018, 7, 15)); 
       this._events = [];
       this.current.fecha_entrega = args.date; 
       let startDate = new Date(this.current.fecha_entrega);
       let endDate = new Date(this.current.fecha_entrega);
       let event = new CalendarEvent("E", startDate, endDate, true, new Color(200, 188, 26, 214) )
       this._events = [];
       this._events.push(event);
       console.log('onDateSelected') 
    }
 
    public submit() {
        
         console.dir(this.current);
         
         //salvar descuento
         if (this.current.descuento > 100) {
           this.current.descuento = 100;
         }

         //salvar descuento nulo
         if (!this.current.descuento || this.current.descuento <= 0 ||  this.current.descuento == 'undefined') {
          this.current.descuento = 0; 
         }

         //salvar horas
         if (this.current.desde_hs < 0) {
           this.current.desde_hs = 9;
         }else if (this.current.desde_hs > 23) {
           this.current.desde_hs = 0;
         }


         //salvar horas
         if (this.current.hasta_hs < 0) {
           this.current.hasta_hs = 18;
         }else if (this.current.hasta_hs > 23) {
           this.current.hasta_hs = 0;
         }
         
         setTimeout(() => {
                this._params.closeCallback(this.current);
         }, 300);
    }

    public close(){
      // this._router.back();
      setTimeout(() => {
                this._params.closeCallback();
      }, 300);
    }
    
}

