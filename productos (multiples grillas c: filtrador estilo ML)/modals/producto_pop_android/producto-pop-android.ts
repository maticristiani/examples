import { Component, OnInit, ViewChild, AfterContentInit } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { getString } from "tns-core-modules/application-settings";
import { TextField } from "tns-core-modules/ui/text-field";
import { alert } from "tns-core-modules/ui/dialogs";

var utils = require("tns-core-modules/utils/utils");

@Component({
    moduleId: module.id,
    templateUrl: "./producto-pop-android.html",
})

export class modalPopAndroid implements OnInit, AfterContentInit{
    
    @ViewChild("TFcantidad", {static: false})  _TFcant: TextField;
    @ViewChild("TFdescuento", {static: false})  _TFdesc: TextField;

    public datos: any;
    public producto: any;

    public controlStock: boolean;

    constructor(
      private _params: ModalDialogParams) {

        let value = getString('control_stock');

        switch (value) {
          case "OFF":
            this.controlStock = false;
            break;
          
          case "ON":
            this.controlStock = true;
            break;
          
          default:
            this.controlStock = true;
            break;
        }
    
    }

    ngOnInit() {
       this.producto = this._params.context;

       if (this.producto.cantidad != 0) {

            this.datos = { 
              'cantidad': this.producto.cantidad , 
              'descuento': this.producto.descuento,
              'disponible': this.producto.disponible 
            };  
        
          if (Number(this.producto.cantidad) == Number(this.producto.disponible) ) {
            this.producto.disponible = 0;
          }
          this.producto.disponible = Number(this.producto.disponible) + Number(this.producto.cantidad);
          
        }else{
          this.datos = { 'cantidad': '', 'descuento': 0, };  
        }

    }

    ngAfterContentInit(){
       try{
          this.openKeyboard()
        }catch{
          console.log('')
        }
    }



    public onFocus(args) {
      console.log('onFocus')

      let _Textfield = <TextField>args.object;
      
          _Textfield.android.setSelectAllOnFocus(true);
          _Textfield.android.requestFocus();

          setTimeout(()=>{
            _Textfield.android.selectAll();  
          },100)
    }
    
    public focusFirst(args): void{
      let firstTextfield = <TextField>args.object;
          firstTextfield.android.selectAll();  
      
      setTimeout(() => {
            firstTextfield.focus(); 
            // Shows the soft input method, ususally a soft keyboard.
      }, 100);
    
    }

    public focusSecond(args): void{
      let secondTextfield = <TextField>args.object;
          secondTextfield.android.selectAll();  
    }

    public close() {
        

        let descuento = this.producto.precio * this.datos.descuento / 100;
        
        if (!descuento) {descuento = 0}
        
        console.log(descuento);
        console.log(this.producto.precio_con_descuento);
        console.log(this.producto.precio);
        
        this.producto.precio_con_descuento = this.producto.precio - descuento;
        this.producto.precio_con_descuento = this.producto.precio_con_descuento.toFixed(2);
        
        if (this.producto.disponible == 0) {
          this.datos.cantidad = 0
        }

        this.producto.subtotal_con_descuento = (this.producto.precio_con_descuento*this.datos.cantidad).toFixed(2);
        this.producto.disponible = this.datos.disponible ? Number(this.datos.disponible) : this.producto.disponible;
        setTimeout(() => {
                this._params.closeCallback();
        }, 150);
    }

    public submit(datos){


          if (!this.controlStock || datos.cantidad <= this.producto.disponible) {
                this.onTextChangeSubtotal(datos)
              }else{
                 let options = {
                    title: "ATENCIÓN",
                    message: "No posee suficiente stock disponible: "+this.producto.disponible,
                    okButtonText: "OK"
                };

                return  alert(options).then(() => {
                   try{
                     // this._TFcant.focus(); 
                     this.openKeyboard();
                   }catch{
                     console.log('not-focus')
                   }
                });
 
        };

         if ((datos.cantidad % this.producto.unidad_venta) != 0) {

               let options = {
                    title: "ATENCIÓN",
                    message: "Su unidad de venta es "+this.producto.unidad_venta,
                    okButtonText: "OK"
                };
                
                datos.cantidad = this.producto.unidad_venta;

               return alert(options).then(() => {
                   try{
                     // this._TFcant.focus(); 
                     this.openKeyboard();
                   }catch{
                     console.log('not-focus')
                   }
                });            
        }


        if ((datos.cantidad % this.producto.unidad_venta) == 0 ) {
          if (!this.controlStock || datos.cantidad <= this.producto.disponible) {
            
            try{
              this._TFcant.dismissSoftInput()
            }catch{
              console.log('imposible-dismissSoftInput')
            }

            try{
              this._TFdesc.dismissSoftInput()
            }catch{
              console.log('imposible-dismissSoftInput')
            }


            setTimeout(() => {
                this._params.closeCallback(datos);
                return;
            }, 300);
          }
        }
        
      }
    
    public pressKeySecond(datos): void{
           this.submit(datos);
    }


    public pressKeyOne(){
      try{
        this._TFdesc.focus();
      }catch{
        try{
          this._TFdesc.android.requestFocus();
        }catch{
          console.log('nofocus')
        }
      }
    }
    
    public onTextChangeCant(datos): void{
        setTimeout(()=>{
            
            //salvo u-venta & stock maximo
            if (!this.producto.unidad_venta) { this.producto.unidad_venta = 1 };
            
            if ((datos.cantidad % this.producto.unidad_venta) != 0) {
          
                datos.cantidad = this.producto.unidad_venta;
            
            }else{

              if (!this.controlStock || datos.cantidad <= this.producto.disponible) {
                this.onTextChangeSubtotal(datos)
              }
              // else{
                
                // if (this.producto.disponible>0) {
                //   datos.cantidad = this.producto.disponible;  
                // }else{
                //   datos.cantidad = 0;
                // }
                
                // this.onTextChangeSubtotal(datos)
              // }
            }            
       
       } , 800)
    }

    public onTextChangeDesc(datos): void{
        setTimeout(()=>{
          if (datos.descuento<0) {
              datos.descuento = 0;
          }else if(datos.descuento>100){
              datos.descuento = 100;
          }

          this.onTextChangeSubtotal(datos)
        } , 600)
    }

    public onTextChangeSubtotal(datos): void{
       let descuento = this.producto.precio * this.datos.descuento / 100;
        if (!descuento) {descuento = 0}
        this.producto.precio_con_descuento = (this.producto.precio - descuento).toFixed(2);
        this.producto.subtotal_con_descuento = (this.producto.precio_con_descuento*this.datos.cantidad).toFixed(2);
    }

    private openKeyboard(): void{
      
      setTimeout(function() {
                // places the cursor here but doesn't open the keyboard
                this._TFcant.android.setSelectAllOnFocus(true);
                this._TFcant.android.requestFocus();
                // this._TFcant.android.setInputType(InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS);//="text|textNoSuggestions"
                var imm = utils.ad.getInputMethodManager();
                imm.showSoftInput(this._TFcant.android, 0);
               
      }, 400);
    
      if (this.producto.cantidad > 0){
           setTimeout(function() {
               this._TFcant.setSelectAllOnFocus(true);
               this._TFcant.requestFocus();
          }, 600);

        }
    }

}
