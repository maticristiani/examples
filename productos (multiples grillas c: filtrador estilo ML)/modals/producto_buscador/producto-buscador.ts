import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { DatabaseService } from "../../../providers/database/database";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { getNumber } from "tns-core-modules/application-settings";
import { isAndroid } from "tns-core-modules/platform";
import { SearchBar } from "tns-core-modules/ui/search-bar";

declare var UISearchBarStyle;
declare var UIImage;

// >> passing-parameters
@Component({
    moduleId: module.id,
    templateUrl: "./producto-buscador.html",
    changeDetection: ChangeDetectionStrategy.OnPush
})

export class modalBuscador implements OnInit {
   
    public searchPhrase: string;

    private arrayItems: Array<{ nameCodigo: string}> = [];
    public valores: ObservableArray<{ nameCodigo: string}> = new ObservableArray<{ nameCodigo: string}>();


    constructor(
       private _params: ModalDialogParams,
       private serviceDB: DatabaseService,
       private changeDetectorRef: ChangeDetectorRef) {

        this.valores =  new ObservableArray();
        
    }

    ngOnInit() {
        this.searchPhrase = this._params.context;
        this.incializarValores();
    }

    public searchBarLoaded(args) {
        let searchBar = <SearchBar>args.object;
        
        //no abrir keyboard
        //searchBar.dismissSoftInput();

        if (isAndroid) {
            // searchBar.text = "";
            // searchBar.android.clearFocus();
            searchBar.android.setInputType(80001);
            searchBar.height = 50;
        }else{
            searchBar.ios.showsCancelButton = false;
            searchBar.ios.endEditing(true);
            searchBar.text = "";
            
            let nativeSearchBar = searchBar.nativeView;
            nativeSearchBar.searchBarStyle = UISearchBarStyle.Minimal;
            nativeSearchBar.backgroundImage = UIImage.new();
            setTimeout(()=>{
                searchBar.focus();
            }, 300)
        }

        searchBar.hint = "Buscar en productos";

    }

    public onSubmit(args) {
        let searchBar = <SearchBar>args.object;
        let searchValue = searchBar.text.toUpperCase();
        
        let res = {str:'',modal:false};
          res.str = searchValue;
          res.modal = false;
        this._params.closeCallback(res);
        
        // if(isAndroid){
            
        //     this.valores = new ObservableArray<{ nameCodigo: string}>();
            
        //     if (searchValue !== "") {
        //         for (let i = 0; i < this.arrayItems.length; i++) {
        //             if (this.arrayItems[i].nameCodigo.toUpperCase().indexOf(searchValue) !== -1) {
        //                 this.valores.push(this.arrayItems[i]);
        //             }
        //         }
        //     }
        // }

    }


    public onTextChanged(args) {
        let limitTime = getNumber('limitSearchTime') ? getNumber('limitSearchTime')  : 600;

        setTimeout(()=>{
            let searchBar = <SearchBar>args.object;
            let searchValue = searchBar.text.toUpperCase();
            
            let limitSearchPhrase = getNumber('limitSearch') ? getNumber('limitSearch')  : 4;

            this.valores = new ObservableArray<{ nameCodigo: string}>();

            if (searchValue !== "" && searchValue.length >= limitSearchPhrase ) {
                for (let i = 0; i < this.arrayItems.length; i++) {
                    if (this.arrayItems[i].nameCodigo.toUpperCase().indexOf(searchValue) !== -1) {
                        this.valores.push(this.arrayItems[i]);
                    }
                }
            }

        }, limitTime)
    }

    public onClear(args) {
        let searchBar = <SearchBar>args.object;
        searchBar.text = "";
        searchBar.hint = "Buscar en productos";

        this.valores = new ObservableArray<{ nameCodigo: string}>();

        //aplicotodo los valores
        // this.arrayItems.forEach(item => {
        //     this.valores.push(item);
        // });
    }

    public close(){
        this._params.closeCallback();
    }

    public tapSearchFast(nameCodigoFast) {
      let res = {str:'',modal:false};
          res.str = nameCodigoFast.split("|");
          res.modal = false;
      this._params.closeCallback(res);
    }

    public longTapSearchFast(nameCodigoFast) {
      let res = {str:'',modal:false};
          res.str = nameCodigoFast.split("|");
          res.modal = true;
      this._params.closeCallback(res);
    }

    private incializarValores(){
        this.arrayItems = [];

        //inicializar-valores
        let params = [];
        let query_prod =  " SELECT p.id, p.codigo_erp, p.nombre "; 
            query_prod += " FROM productos as p ";
            query_prod += " WHERE p.codigo_erp IS NOT NULL";

        this.serviceDB.all(query_prod, params).then((rows: any)=>{
          
          rows.forEach((row: any)=>{
            let codigo: string = row.codigo_erp+'|'+row.nombre;
            this.arrayItems.push({ nameCodigo: codigo });            
          })

        })   

        //aplico-todos-los-valores
        // this.valores = new ObservableArray<{ nameCodigo: string}>(this.arrayItems);
    }

}
