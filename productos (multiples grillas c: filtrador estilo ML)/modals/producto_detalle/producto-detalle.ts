import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { getBoolean, getString } from "tns-core-modules/application-settings";
import { isAndroid } from "tns-core-modules/platform";

let http = require("tns-core-modules/http");

@Component({
    moduleId: module.id,
    templateUrl: "./producto-detalle.html",
})

export class modalDetalle implements OnInit {
    
    @ViewChild("drawee", {static: false}) drawee: ElementRef;
    public prod: any;
    public mostrarFoto: boolean;
   
    constructor(
      private _params: ModalDialogParams ) {
      this.mostrarFoto = true;
    }

    ngOnInit() {
       this.prod = this._params.context;      
       this.mostrarFoto = (getString('mostrar_foto') && getString('mostrar_foto')=='ON') ? true : false;
       console.dir(this.prod);
    }

    public close() {
        this._params.closeCallback();
    }

    public reloadImage() {
      
      if (!this.mostrarFoto) {
        return;
      }

      let fotos_online = (typeof(getBoolean('fotos_online')) === 'boolean' ) ? getBoolean('fotos_online') : false;

      
      if (fotos_online && isAndroid) {
          
          let foto = this.prod.foto;
          this.prod.foto = ''
          
          setTimeout(()=>{
              this.prod.foto = foto;
          },1200)
        
      }
      

      if (!fotos_online) {
          
        let urlPath = getString('path_fotos');       
            if (urlPath) {
              this.downloadFile(this.prod.foto, ('file:/'+urlPath)).then((res)=>{
                if (res=='OK') {
                  console.log('OFFLINE');
                  this.prod.foto =  urlPath+'/'+this.prod.foto_nombre;
                }else{
                  console.log('ONLINE');
                  this.prod.foto = this.prod.foto;
                }
              })
            }

      }
      
    }

    private downloadFile(url_descarga, filePath){
      return new Promise((resolve, reject) => {  
        http.getFile(url_descarga, filePath).then(downloadedFile => {
           resolve('OK');
        }).catch(err=>{
          console.dir(err);
          reject('ERROR');      
        })
      })
    }

}
