import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { getString } from "tns-core-modules/application-settings";
import { TextField } from "tns-core-modules/ui/text-field";
import { alert } from "tns-core-modules/ui/dialogs";
import { EventData } from "tns-core-modules/data/observable";

var utils = require("tns-core-modules/utils/utils");

const IOS_KEYBOARDTYPE_NUMBERPAD: number = 4;
declare var UIKeyboardTypeNumberPad;
declare var UIReturnKeyDone;
declare var CGPoint;

@Component({
    moduleId: module.id,
    templateUrl: "./producto-pop-ios.html",
})

export class modalPopIos implements OnInit{
    
    @ViewChild("TFcantidad", {static: false})  _TFcant: TextField;
    @ViewChild("TFdescuento", {static: false})  _TFdesc: TextField;

    public datos: any;
    public producto: any;

    public controlStock: boolean;
    
    constructor(
        // private _router: RouterExtensions,
        private _params: ModalDialogParams) {
      
        let value = getString('control_stock');

        switch (value) {
          case "OFF":
            this.controlStock = false;
            break;
          
          case "ON":
            this.controlStock = true;
            break;
          
          default:
            this.controlStock = true;
            break;
        }

    }

    ngOnInit(){ 
        this.producto = this._params.context;
        
        if (this.producto.cantidad != 0) {
          
            this.datos = { 
              'cantidad': this.producto.cantidad , 
              'descuento': this.producto.descuento,
              'disponible': this.producto.disponible 
            };  

          if (Number(this.producto.cantidad) == Number(this.producto.disponible) ) {
            this.producto.disponible = 0;
          }
          this.producto.disponible = Number(this.producto.disponible) + Number(this.producto.cantidad);
          
        }else{
          this.datos = { 'cantidad': '', 'descuento': 0 };  
        }
    }

    public onFocus(args) {
      console.log('onFocus')
        
          let _Textfield = <TextField>args.object;
              _Textfield.keyboardType =  UIKeyboardTypeNumberPad;
              _Textfield.returnKeyType = UIReturnKeyDone;
              _Textfield.ios.returnKeyType = UIReturnKeyDone;

          let startPosition: any = _Textfield.ios.beginningOfDocument
          let endPosition: any = _Textfield.ios.endOfDocument
          let selectedRange: any = _Textfield.ios.selectedTextRange
          
                
          setTimeout(()=>{
                  try{
                    _Textfield.ios.selectedTextRange = _Textfield.ios.textRangeFromPositionToPosition(startPosition, endPosition);  
                  }catch{console.log('error-selectAll()')}  
                  console.log('selectAll()')
          },100)

                // Shows the soft input method, ususally a soft keyboard.

    }
    

    focusFirst(args){
      console.log('focusFirst');
      //UITextPosition // UITextRange

      let firstTextfield = <TextField>args.object;
          firstTextfield.keyboardType =  UIKeyboardTypeNumberPad;
          firstTextfield.returnKeyType = UIReturnKeyDone;
          firstTextfield.ios.returnKeyType = UIReturnKeyDone;

      let startPosition: any = firstTextfield.ios.beginningOfDocument
      let endPosition: any = firstTextfield.ios.endOfDocument
      let selectedRange: any = firstTextfield.ios.selectedTextRange
      
     
      setTimeout(() => {
            firstTextfield.focus(); 
            console.log('focus')

            setTimeout(()=>{
              try{
                firstTextfield.ios.selectedTextRange = firstTextfield.ios.textRangeFromPositionToPosition(startPosition, endPosition);  
              }catch{console.log('error-selectAll()')}  
              console.log('selectAll()')
            },100)

            // Shows the soft input method, ususally a soft keyboard.
      }, 100);
    }

    focusSecond(args){
      let secondTextfield = <TextField>args.object;
      
      secondTextfield.keyboardType =  UIKeyboardTypeNumberPad;
          secondTextfield.returnKeyType = UIReturnKeyDone;
          secondTextfield.ios.returnKeyType = UIReturnKeyDone;

      let startPosition: any = secondTextfield.ios.beginningOfDocument
      let endPosition: any = secondTextfield.ios.endOfDocument
      let selectedRange: any = secondTextfield.ios.selectedTextRange
      
     
      setTimeout(() => {
            // secondTextfield.focus(); 
            console.log('focus')

            setTimeout(()=>{
              try{
                secondTextfield.ios.selectedTextRange = secondTextfield.ios.textRangeFromPositionToPosition(startPosition, endPosition);  
              }catch{console.log('error-selectAll()')}  
              console.log('selectAll()')
            },100)

            // Shows the soft input method, ususally a soft keyboard.
      }, 100);
    }
 
    public close() {
        let descuento = this.producto.precio * this.datos.descuento / 100;
        if (!descuento) {descuento = 0}
        this.producto.precio_con_descuento = this.producto.precio - descuento;
        this.producto.precio_con_descuento = this.producto.precio_con_descuento.toFixed(2);
        
        if (this.producto.disponible == 0) {
          this.datos.cantidad = 0
        }

        this.producto.subtotal_con_descuento = (this.producto.precio_con_descuento*this.datos.cantidad).toFixed(2);
        this.producto.disponible = this.datos.disponible ? Number(this.datos.disponible) : this.producto.disponible;
        
        setTimeout(() => {
                this._params.closeCallback();
        }, 150);
    }

    public submit(datos) {


        if (!this.controlStock || datos.cantidad <= this.producto.disponible) {
                this.onTextChangeSubtotal(datos)
              }else{
                 let options = {
                    title: "ATENCIÓN",
                    message: "No posee suficiente stock disponible: "+this.producto.disponible,
                    okButtonText: "OK"
                };

                return  alert(options).then(() => {
                   try{
                     this._TFcant.focus(); 
                   }catch{
                     console.log('not-focus')
                   }
                });
 
        };

         if ((datos.cantidad % this.producto.unidad_venta) != 0) {

               let options = {
                    title: "ATENCIÓN",
                    message: "Su unidad de venta es "+this.producto.unidad_venta,
                    okButtonText: "OK"
                };
                
                datos.cantidad = this.producto.unidad_venta;

               return alert(options).then(() => {
                   try{
                     this._TFcant.focus(); 
                   }catch{
                     console.log('not-focus')
                   }
                });            
        }


         if ((datos.cantidad % this.producto.unidad_venta) == 0) {
          if (!this.controlStock || datos.cantidad <= this.producto.disponible) {   
            return this._params.closeCallback(datos);
          }
        }
      }
    
    public onTextChangeCant(datos){
        setTimeout(()=>{
            
            //salvo u-venta & stock maximo
            if (!this.producto.unidad_venta) { this.producto.unidad_venta = 1 };
            
            if ((datos.cantidad % this.producto.unidad_venta) != 0) {
          
                datos.cantidad = this.producto.unidad_venta;
            
            }else{

              if (!this.controlStock || datos.cantidad <= this.producto.disponible) {
                this.onTextChangeSubtotal(datos)
              }
              // else{
                
              //   if (this.producto.disponible>0) {
              //     datos.cantidad = this.producto.disponible;  
              //   }else{
              //     datos.cantidad = 0;
              //   }
                
              //   this.onTextChangeSubtotal(datos)
              // }
            }            
       
       } , 600)
    }

    public onTextChangeDesc(datos){
        setTimeout(()=>{
          if (datos.descuento<0) {
              datos.descuento = 0;
          }else if(datos.descuento>100){
              datos.descuento = 100;
          }

          this.onTextChangeSubtotal(datos)
        } , 600)
    }

    public onTextChangeSubtotal(datos){
       let descuento = this.producto.precio * this.datos.descuento / 100;
        if (!descuento) {descuento = 0}
        this.producto.precio_con_descuento = (this.producto.precio - descuento).toFixed(2);
        this.producto.subtotal_con_descuento = (this.producto.precio_con_descuento*this.datos.cantidad).toFixed(2);
    }


}
