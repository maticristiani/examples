import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptFormsModule } from "nativescript-angular/forms";
import { productos2RoutingModule } from "./productos2-routing.module";
import { productos2Component } from "./productos2.component";

import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";


 import { modalDetalle } from "./modals/producto_detalle/producto-detalle";
 import { modalFiltrador } from "./modals/producto_filtrador/producto-filtrador";
 import { modalBuscador } from "./modals/producto_buscador/producto-buscador";
 import { modalCabecera } from "./modals/producto_cabecera/producto-cabecera";
 import { modalPopAndroid } from "./modals/producto_pop_android/producto-pop-android";
 import { modalPopIos } from "./modals/producto_pop_ios/producto-pop-ios";
 import { modalImage } from "./modals/producto_image/producto-image";

import { HeadComponent } from "./components/head-component/head.component";
import { FiltroComponent } from "./components/filtros-component/filtros.component";
import { ProductoComponent } from "./components/producto-component/producto.component";



@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptFormsModule,
        productos2RoutingModule,
        NativeScriptUIListViewModule
    ],
    entryComponents: [ modalPopAndroid, modalPopIos, modalFiltrador, modalBuscador, modalDetalle, modalImage, modalCabecera ],
    declarations: [
        productos2Component,
        HeadComponent,
        ProductoComponent,
        FiltroComponent,
        modalPopAndroid,
        modalPopIos,
        modalFiltrador,
        modalBuscador,
        modalDetalle,
        modalImage,
        modalCabecera
    ],
    exports: [
        productos2Component
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class Productos2Module { }


