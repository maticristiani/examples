import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

/* ***********************************************************
* Keep data that is displayed as drawer items in the MyDrawer component class.
*************************************************************/
@Component({
    selector: "HeadComponent",
    moduleId: module.id,
    templateUrl: "./head.component.html"
})
export class HeadComponent implements OnInit {

    @Input() cliente: string;
    @Input() de_sistema: boolean;
    @Input() descuento: number;
    @Input() subtotal_con_descuento: number;
    @Input() iva: number;
    @Input() total: number;
    @Input() cantidad: number;
    @Input() cantidad_items: number;
    
    constructor(private routerExtensions: RouterExtensions) {

    }

    ngOnInit(): void {
        /* ***********************************************************
        * Use the MyDrawerItemComponent "onInit" event handler to initialize the properties data values.
        *************************************************************/
    }

}
