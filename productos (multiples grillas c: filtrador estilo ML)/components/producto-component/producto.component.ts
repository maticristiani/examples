import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { getBoolean } from "tns-core-modules/application-settings";

@Component({
    selector: "ProductoComponent",
    moduleId: module.id,
    templateUrl: "./producto.component.html"
})
export class ProductoComponent implements OnInit {

    
    @Input() mostrarFoto: boolean;
    @Input() viewActive: string;
    @Input() producto: any;
    
    @Output() openPop = new EventEmitter();
    @Output() openDetalle = new EventEmitter();
    @Output() openImage = new EventEmitter();
    @Output() delete = new EventEmitter();
    
    private openImageOneClick: boolean;

    constructor(private routerExtensions: RouterExtensions) {
        this.openImageOneClick = getBoolean('abrir_imagen_oneclick') ? getBoolean('abrir_imagen_oneclick') : false;
    }

    ngOnInit(): void {
    }

    abrirPopCantidad(prod): void{
        this.openPop.emit(prod);
    }
    
    borrarProducto(prod): void{
        this.delete.emit(prod);
    }

    abrirPopDetalle(codigo_erp, foto): void{
        //ONE
        return ((this.openImageOneClick && this.producto.sin_foto != 'S') ? this.openImage.emit(foto) : this.openDetalle.emit(codigo_erp));
    }

    abrirImagen(codigo_erp, foto): void{
        //LONG
        return ((this.openImageOneClick && this.producto.sin_foto != 'S') ? this.openDetalle.emit(codigo_erp) : this.openImage.emit(foto));

        // if(!this.openImageOneClick){
        //     this.openDetalle.emit(codigo_erp);
        // }else{
        //     this.openImage.emit(foto);
        // }
    }

}
