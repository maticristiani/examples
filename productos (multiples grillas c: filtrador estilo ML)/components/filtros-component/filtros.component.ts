import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

/* ***********************************************************
* Keep data that is displayed as drawer items in the MyDrawer component class.
*************************************************************/
@Component({
    selector: "FiltroComponent",
    moduleId: module.id,
    templateUrl: "./filtros.component.html"
})
export class FiltroComponent implements OnInit {

    @Input() filtros: Array<string>;
    @Output() filtrosChange = new EventEmitter();

    constructor(private routerExtensions: RouterExtensions) {

    }

    ngOnInit(): void {
        /* ***********************************************************
        * Use the MyDrawerItemComponent "onInit" event handler to initialize the properties data values.
        *************************************************************/
    }

    resetarFiltros(): void {
        this.filtrosChange.emit();
    }
}
