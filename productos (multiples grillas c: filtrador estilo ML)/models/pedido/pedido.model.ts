export interface Pedido {
		id: any,
		subtotal_con_descuento: number,
		total: number,
		iva: number,
		descuento: number,
		cantidad: number,
		cantidad_items: number, 
		no_compra: string
}