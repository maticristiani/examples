export interface Vista {
	url:string,
	name:string,
	android: boolean
}