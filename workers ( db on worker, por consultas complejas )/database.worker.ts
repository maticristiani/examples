// app/worker.ts
import "globals";
import { getString } from "tns-core-modules/application-settings";

const context: Worker = self as any;
const sqlite: any = require('nativescript-sqlite');

(<any>global).onmessage = workerMsg => {
	console.log("Inside DB worker...");

    setTimeout(()=>{
        
        var dbname = getString('bd_name') ? getString('bd_name') : 'sin_asignar.db';
        var request: any = workerMsg.data;
        var query: any = request.query ? request.query : '';
        var type: any = request.type ? request.type : 'error';
        var params: any = request.params ? request.params : [];
        var responseMsg: any = { success: true };

        switch (type) {
            case "error":
                    console.log("Error DB worker...");
                    
                    throw { error: 'Error Object Thrown.'};
                    return;
                break;
            
            case "close":
                    console.log("Close DB  worker...");
                    
                    close();
                    return;
                break;
            
            case "query":
                    console.log("Query DB worker...");

                    queryData(dbname, query, params).then((res)=>{
                        responseMsg.res = res;
                        console.log("Outside DB worker...");
                        (<any>global).postMessage(responseMsg);  
                    })
                break;

            case "get":
                    console.log("Get DB worker...");

                    getData(dbname, query, params).then((row)=>{
                        responseMsg.row = row;
                        console.log("Outside DB worker...");
                        (<any>global).postMessage(responseMsg);  
                    })
                break;

            case "all":
                    console.log("All DB worker...");

                    allData(dbname, query, params).then((rows)=>{
                        responseMsg.rows = rows;
                        console.log("Outside DB worker...");
                        (<any>global).postMessage(responseMsg);  
                    })
                break;

            case "parametros":
                    console.log("parametros General worker...");

                    allData(dbname, query, params).then((rows)=>{ 
                        responseMsg.rows = rows;
                        console.log("Outside General worker...");
                        (<any>global).postMessage(responseMsg);  
                    })
                break;


            default:
                // code...
                break;
        }

    }, 50)
};

(<any>global).onerror = err => {
    // return true to not propagate to main
    console.dir(err);
    return true;
}


function allData(dbname, query, params) {
    // do all the complicated work here.
    return new Promise((resolve, reject) => {
        new sqlite(dbname, function(err, db) {   
        db.resultType(sqlite.RESULTSASOBJECT);
            db.all(query, params).then((rows)=>{
                resolve(rows); 
            }).catch(err=>{ 
                console.dir(err);
                throw { error: 'Error Object Thrown.'};
                return;
            })
        })
    })
}

function getData(dbname, query, params) {
    // do all the complicated work here.
    return new Promise((resolve, reject) => {
        new sqlite(dbname, function(err, db) {   
        db.resultType(sqlite.RESULTSASOBJECT);
            db.get(query, params).then((row)=>{
                resolve(row); 
            }).catch(err=>{ 
                console.dir(err);
                throw { error: 'Error Object Thrown.'};
                return;
            })
        })
    })
}


function queryData(dbname, query, params) {
    // do all the complicated work here.
    return new Promise((resolve, reject) => {
        new sqlite(dbname, function(err, db) {   
        db.resultType(sqlite.RESULTSASOBJECT);
            db.query(query, params).then((res)=>{
                resolve(res); 
            }).catch(err=>{ 
                console.dir(err);
                throw { error: 'Error Object Thrown.'};
                return;
            })
        })
    })
}