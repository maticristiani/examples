import { InternetConnectionService } from "../shared/services/internet-connection.service";
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import { trigger, state, transition, style, animate } from '@angular/animations';
import { Component, NgZone, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import * as appSettings from "tns-core-modules/application-settings";
import { RouterExtensions } from "nativescript-angular/router";
import { Observable } from "tns-core-modules/data/observable";
import { ApiService } from "../shared/services/api.service";
import { localize } from "nativescript-localize";
import { ActivatedRoute } from "@angular/router";
import { Page } from "tns-core-modules/ui/page";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "SeleccionarTienda",
    moduleId: module.id,
    templateUrl: "./seleccionar-tienda.component.html",
    styleUrls: ["./seleccionar-tienda.component.css"],
    animations: [
    
        trigger('slide-in-out', [
          transition(':enter', [
            style({transform: 'translateY(-10%)'}),
            animate('1000ms ease-in', style({transform: 'translateY(0%)'}))
          ]),
          transition(':leave', [
            animate('1000ms ease-in', style({transform: 'translateY(-10%)'}))
          ])
        ])

    ]
})

export class SeleccionarTienda  implements OnInit, OnDestroy {

    store$;
    connection$;
    
    public mapbox;
    public connectionStatus: boolean;
    public arrayFiltros: any;
    public isLoading: boolean;
    public isregister: boolean;
    public isSolaped: boolean;
    public isStoreSelected: boolean;
    public isBigMap: boolean;

    private _dataStoreFilters: any;
    private _dataStore: ObservableArray<any>;
    
    private feedback: Feedback;
    
    public selectedStore: any;
    
    constructor(
        private _apiService: ApiService,
        private _internetConnection: InternetConnectionService,
        private _route: RouterExtensions,
        private _routeActive: ActivatedRoute,
        private _ngZone: NgZone,
        private page: Page) {

        this.feedback = new Feedback()
        
        this.page.actionBarHidden = false;
        this.page.backgroundSpanUnderStatusBar = false;
        this.page.className = "page-seleccionar-tienda-container";
        this.page.statusBarStyle = "dark";

        this.connectionStatus = true;
        this.isStoreSelected = false;
        this.isSolaped = false;
        this.isBigMap = false;

        this.arrayFiltros = [];
        this._dataStore  = new ObservableArray();
        this.isregister = true;
        
        this.selectedStore = {
          id: 0,
          alias: '',
          direccion: '',
          horarios: '',
          observacion: '',
          selected: false,
          delivery: false,
          ratio: 0,
          medios_de_pago: []
        };

    }


    get dataStore(): ObservableArray<any> {
        return this._dataStore;
    }
    
    ngOnInit(): void {
        this.connection$ = this._internetConnection.connectionStatus$.subscribe(data => {
            this.connectionStatus = data.valueOf();
        });

        let authentication = appSettings.getString('authentication');
        
        this._apiService.getStoresWithFilter(authentication, false).then((res: any)=>{
                if (res.status != 'success'){
                    this.showWarning(localize('select_store.error_get_stores'));
                    return;
                }
              
                if (!res.data || res.data.length == 0) {
                  return   
                }
                
                this._dataStoreFilters = res.data;
                this._dataStore  = new ObservableArray(res.data);

                this.store$ = this._apiService.Store$.subscribe(value => {
                    this.selectedStore = value;
                })

                if (this.selectedStore && this.selectedStore.id > 0) {
                    this._dataStore.forEach((ds)=>{ if (ds.id == this.selectedStore.id) { ds.selected = true } })
                }

        })

        this._routeActive.params.subscribe((params) => {
            this.isregister = (params["isregister"]=='true') ? true : false;
        });


     


    }

    ngOnDestroy(): void {
        if (this.connection$)
            this.connection$.unsubscribe();
    }


    public changeMap(){
       if(this.isBigMap){
            this.isBigMap = false;
            return 
        }

        this.isBigMap = true;
    }
    
    public toggleFilter(filter){
        if(filter.active){
            filter.active = false;
            this.filterList()
            return 
        }

        filter.active = true;

        this.filterList()
    }

    public filterList() {
        console.dir('@filterList');
        this.isLoading = true;
        let onFilter = 0;
        let result = []

        this._dataStore  = new ObservableArray();

        this._dataStoreFilters.forEach((s)=>{
            this.arrayFiltros.forEach((f)=>{
                if (f.active) {
                    onFilter =  onFilter + 1;
                    if (s.delivery == f.clave || s.medios_de_pago.indexOf(f.clave)>-1) {
                        result.push(s);
                    }
                }
            })
        })

        result = result.filter((e,i)=> result.indexOf(e) >= i)

        // sin filtros
        if (onFilter==0) {
            this._dataStore = new ObservableArray(this._dataStoreFilters);    
            return
        }
        
        this._dataStore = new ObservableArray(result);
        
        this.isLoading = false;
    }

    public selectStore(store){
        console.dir("@selectStore");
        this._dataStore.map(a=>a.selected=false);
        
        this.isSolaped = true; 
        
        if(store.selected){
            store.selected = false;
            return 
        }

        store.selected = true;

        this.selectedStore = store

        this.mapbox.setCenter({
                    lat: store.lat, // mandatory
                    lng: store.long, // mandatory
                    animated: true // default true
                  })

        this._apiService.setStore(this.selectedStore);
        this.isStoreSelected = true;
    }

    public onMapReady(args){
        console.dir("@onMapReady");
        let map = args.map;
        this.mapbox = map;
        
        let markers = [];
        
        if (!this._dataStore || this._dataStore.length == 0) {
            return;
        }
        
        this._dataStore.forEach((s)=>{
           if (s.lat && s.long) {
            markers.push({
                                    lat: s.lat,
                                    lng: s.long,
                                    title: s.alias,
                                    subtitle: s.direccion,
                                    selected: false,
                                    iconPath: 'assets/select_store/marker-store.png',
                                    onCalloutTap: () => {
                                        this._ngZone.run(()=>{
                                            this.selectStore(s);
                                        })
                                    }
                                    
            })
          }
        })            

        map.addMarkers(markers).then((res)=>{
            console.dir(res);
        })

        map.trackUser({
            mode: "NONE", // "NONE" | "FOLLOW" | "FOLLOW_WITH_HEADING" | "FOLLOW_WITH_COURSE"
            animated: true
        });


        if (this.selectedStore.id && this.selectedStore.id > 0) {
            
            this.mapbox.setCenter({
                        lat: this.selectedStore.lat, // mandatory
                        lng: this.selectedStore.long, // mandatory
                        animated: true // default true
            })
        }

    }
    
    /* feedbacks */
    showWarning(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Warning,
              backgroundColor: new Color("#df4a32"),
        });
    }

    showSuccess(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Success,
              backgroundColor: new Color("#95ba65"),
        });
    }

    /* navigations */
    public confirmSelectStore() {
        if (!this.selectedStore) {
            return;
        }
        
        this.navigateToHome();
    }

    public navigateToHome() {
        if(this.isLoading || !this.connectionStatus) return;
        this._ngZone.run(() => {
            this._route.navigate(["home"], {
                clearHistory: true,
                animated: true,
                transition: {
                    name: "slideTop",
                    duration: 600,
                    curve: "ease"
                }
            });
        });
    }


    public navigateToEditDirection() {
        if(this.isLoading || !this.connectionStatus) return;
        let page = this.isregister ? 'nueva-direccion' : 'home';
        this._ngZone.run(() => {
            this._route.navigate([page], {
                clearHistory: true,
                animated: true,
                transition: {
                    name: "slideTop",
                    duration: 600,
                    curve: "ease"
                }
            });
        });
    }

}
