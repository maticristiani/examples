import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { SeleccionarTienda } from "./seleccionar-tienda.component";

const routes: Routes = [
    { path: "", component: SeleccionarTienda }
];

@NgModule({
    imports: [NativeScriptRouterModule.forChild(routes)],
    exports: [NativeScriptRouterModule]
})
export class SeleccionarTiendaRoutingModule { }
