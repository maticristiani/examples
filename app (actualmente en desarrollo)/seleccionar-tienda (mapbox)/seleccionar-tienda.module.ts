import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptUIDataFormModule } from "nativescript-ui-dataform/angular";
import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";

import { SeleccionarTiendaRoutingModule } from "./seleccionar-tienda-routing.module";
import { SeleccionarTienda } from "./seleccionar-tienda.component";

import { SharedModule } from "../shared/shared.module"

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptUIDataFormModule,
        NativeScriptUIListViewModule,
        SeleccionarTiendaRoutingModule,
        SharedModule
    ],
    declarations: [
        SeleccionarTienda
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SeleccionarTiendaModule { }
