import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "FloatSearch",
    moduleId: module.id,
    template: `
        <GridLayout rows="20, auto" marginTop="0" marginBottom="5" height="100">
            <Label #label row="0" textAlignment="left" paddingLeft="10" marginLeft="0" [text]="placeholder" opacity="0.5" fontSize="14" color="#4b6ca5" class="input futura"></Label>
            <SearchBar row="1" #searchField [isEnabled]="status "hint="" (textChange)="onTextChange($event)" color="#393939" textFieldBackgroundColor="#ffffff" textFieldHintColor="#393939"></SearchBar>
        </GridLayout>
    `
})
export class FloatSearch {
    @Input() status: boolean;
    @Input() placeholder: string;
    @ViewChild("label",{static: false}) label: ElementRef;
    @ViewChild("searchField",{static: false}) searchField: ElementRef;
    @Output() textChange = new EventEmitter();

    constructor() {
    }

    ngOnInit(): void {
    }

    onTextChange(args) {
        this.textChange.emit(args);
    }

}
