import { Component, ElementRef, Input, Output, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "FloatLabel",
    moduleId: module.id,
    template: `
        <GridLayout rows="25, auto" marginTop="0" marginBottom="0" height="70">
            <Label #label marginTop="-40" row="1" textAlignment="left" paddingLeft="0" marginLeft="0" [text]="placeholder" opacity="0.5" fontSize="14" color="#4b6ca5" class="input futura"></Label>
            <TextField
                [editable]="status"
                 #textField 
                 class="futura text-lowercase"
                 textAlignment="left"
                 paddingLeft="0"
                 [secure]="secure" 
                 row="1"  
                 (focus)="onFocus()"
                 (blur)="onBlur()"
                 borderBottomWidth="1"
                 borderBottomColor="#4b6ca5"
                 padding="2"></TextField>
        </GridLayout>
    `
})
export class FloatLabel {
    @Input() status: boolean;
    @Input() placeholder: string;
    @Input() secure: boolean;

    @ViewChild("label",{static: false}) label: ElementRef;
    @ViewChild("textField",{static: false}) textField: ElementRef;


    constructor() {
    }

    onFocus() {
        const label = this.label.nativeElement;
        const textField = this.textField.nativeElement;
        
        // animate the label sliding up and less transparent.
        label.animate({
            // translate: { x: 0, y: - 25 },
            opacity: 1,
        }).then(() => { }, () => { });

        // set the border bottom color to green to indicate focus
        textField.borderBottomColor = new Color('#4b6ca5');
    }

    onBlur() {
        const label = this.label.nativeElement;
        const textField = this.textField.nativeElement;

        // if there is text in our input then don't move the label back to its initial position.
        if (!textField.text) {
            label.animate({
                // translate: { x: 0, y: 0 },
                opacity: 0.4
            }).then(() => { }, () => { });
        }
        // reset border bottom color.
        textField.borderBottomColor = new Color('#4b6ca5');
    }
}
