import { Component, ElementRef, Input, ViewChild } from "@angular/core";


// template: `
//         <ActivityIndicator [row]="row" [col]="col" padding="10" [colSpan]="colSpan" [rowSpan]="rowSpan" color="#4b6ca5"  verticalAlignment="center" horizontalAlignment="center" width="100" height="100" [busy]="isLoading"></ActivityIndicator>
//         <ActivityIndicator [row]="row" [col]="col" [colSpan]="colSpan" [rowSpan]="rowSpan" color="#95ba65"  verticalAlignment="center" horizontalAlignment="center" width="80" height="80" [busy]="isLoading"></ActivityIndicator>
//         <ActivityIndicator [row]="row" [col]="col" [colSpan]="colSpan" [rowSpan]="rowSpan" color="#f8f6b9"  verticalAlignment="center" horizontalAlignment="center" width="60" height="60" [busy]="isLoading"></ActivityIndicator>
//     `

@Component({
    selector: "ThreeLoading",
    moduleId: module.id,
    template: `
     <AbsoluteLayout [visibility]="isLoading ? 'visible':'collapsed' " [row]="row" [col]="col" [colSpan]="colSpan" [rowSpan]="rowSpan" verticalAlignment="center" horizontalAlignment="center" borderWidth="2" borderRadius="100%" borderColor="#4b6ca5">
        <Image
                top="0"
                left="0"
                width="90"
                height="90"
                opacity="0.8"
                verticalAlignment="center"
                horizontalAlignment="center"
                class="rotate-infinite"
                src="~/assets/loader/loader-icon-2.png"
                stretch="aspectFill"></Image>
            <Image
                top="0"
                left="0"
                width="90"
                height="90"
                opacity="1"
                verticalAlignment="center"
                horizontalAlignment="center"
                src="~/assets/loader/loader-border-2.png"
                stretch="aspectFill"></Image>
    </AbsoluteLayout>
    `
})
export class ThreeLoading {
    @Input() row: number;
    @Input() col: number;
    @Input() rowSpan: number;
    @Input() colSpan: number;
    @Input() isLoading: boolean;
    
    constructor() {
    }
}
