import { Component, ElementRef, Input, ViewChild } from "@angular/core";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "ConnectionStatus",
    moduleId: module.id,
    template: `
     <Label
         [visibility]=" status ? 'collapsed' : 'visible' "
         horizontalAlignment="center"
         textAlignment="center"
         verticalAlignment="top"
         width="100%" height="30" 
         padding="8" 
         class="futura-bold"
         backgroundColor="#cc0808"
         color="#ffffff" 
         [text]="'app.connection_status' | L"> </Label>
    `
})
export class ConnectionStatus {
    @Input() status: boolean;
    
    constructor() {
        console.dir('status:', this.status)
    }

  }
