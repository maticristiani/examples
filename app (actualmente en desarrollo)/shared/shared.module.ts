import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";
import { NativeScriptLocalizeModule } from "nativescript-localize/angular";

/* componentes */
import { FloatSearch } from "./components/float-search/float-search.component";
import { FloatLabel } from "./components/float-label/float-label.component";
import { ThreeLoading } from "./components/three-loading/three-loading.component";
import { ConnectionStatus } from "./components/connection-status/connection-status.component";

/* Services */
import { InternetConnectionService } from "./services/internet-connection.service"


@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptLocalizeModule,
        ],
    declarations: [
        FloatLabel,
        FloatSearch,
        ThreeLoading,
        ConnectionStatus
        ],
    exports: [
        FloatLabel,
        FloatSearch,
        ThreeLoading,
        ConnectionStatus,
        NativeScriptLocalizeModule
    ],
    providers:[
        InternetConnectionService
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class SharedModule { }
