import { Component, ElementRef, Input, Output, EventEmitter, ViewChild } from "@angular/core";
import * as frame from "tns-core-modules/ui/frame";
import { isIOS } from "tns-core-modules/platform";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "ModalRestoreAccount",
    moduleId: module.id,
    styles: [`
   
      
        `

    ],
    template: `
       <AbsoluteLayout class="dialog-wrapper" height="auto">
            <StackLayout     
                        class="dialog"
                        borderBottomLeftRadius="10"
                        borderRadius="1"
                        borderWidth="1"                        
                        marginTop="30"
                        marginBottom="0"  
                        borderColor="#393939"
                        padding="0">

                <Label
                    maring="0"
                    width="100%"
                    paddingTop="6"
                    paddingBottom="6"
                    fontSize="16"
                    class="text-center futura-bold"
                    textWrap="true"
                    backgroundColor="#4b6ca5" color="#FFFFFF" [text]=" 'login.restore_account' | L"></Label>
               
                <StackLayout class="form" padding="10">
                    
                    <Label class="text-left futura" textWrap="true" [text]=" 'login.restore_instructions' | L"></Label>
            
                     <TextField #textField
                                 [(ngModel)]="email"
                                 backgroundColor="#ededed"
                                 class="input futura text-lowercase"
                                 [hint]=" 'login.restore_hint' | L"
                                 textAlignment="left"
                                 marginTop="30"
                                 returnKeyType="done" 
                                 (focus)="onFocus()" (blur)="onBlur()"
                                 (returnPress)="submitRestoreAccount()"
                                 borderBottomWidth="1"></TextField>
                    
                    <StackLayout class="hr-dark"></StackLayout>
                </StackLayout>
                
                <StackLayout orientation="horizontal" borderTopWidth="1" borderColor="#dadada">
                    
                    <Button  class="btn" width="40%" borderWidth="1" borderColor="#393939"  padding="0" margin="0" borderWidth="0" backgroundColor="#FFFFFF" color="#393939"  
                    [text]="'login.restore_cancel' | L" (tap)="cancelRestoreAccount()"></Button>

                    <Button  class="btn" width="60%" padding="0" margin="0" borderWidth="0" [backgroundColor]="(email && email.length > 0) ? '#95ba65' : '#dadada' "
                    [text]="'login.restore_confirm' | L" (tap)="submitRestoreAccount()"></Button>
        
                    
                </StackLayout>
            </StackLayout>
        </AbsoluteLayout>
    `
})
export class ModalRestoreAccount {
    public email: string;
    
    @ViewChild("textField",{static: false}) textField: ElementRef;
    @Output() submitModal = new EventEmitter();
    @Output() closeModal = new EventEmitter();

    constructor() {
    }

    ngAfterViewInit(): void {
        console.dir('ngOnInit: Modal')
         // isIOS ? frame.topmost().nativeView.endEditing(true) : this.textField.nativeElement.dismissSoftInput();
    }

    submitRestoreAccount() {
        if(!this.email || this.email.length ==0 ){
            return;
        }

        isIOS ? frame.topmost().nativeView.endEditing(true) : this.textField.nativeElement.dismissSoftInput();
        this.submitModal.emit(this.email);        
    }

    cancelRestoreAccount() {
        isIOS ? frame.topmost().nativeView.endEditing(true) : this.textField.nativeElement.dismissSoftInput();
        this.closeModal.emit();        
    }
    
    openFocusOnTextField() {
        this.textField.nativeElement.focus();
    }

    onFocus() {
        const textField = this.textField.nativeElement;

        // animate the label sliding up and less transparent.
        textField.animate({
            translate: { x: 0, y: - 25 },
            opacity: 1,
        }).then(() => { }, () => { });

        // set the border bottom color to green to indicate focus
        textField.borderBottomColor = new Color('#4b6ca5');
    }

    onBlur() {
        const textField = this.textField.nativeElement;

        // if there is text in our input then don't move the label back to its initial position.
        if (!textField.text) {
            textField.animate({
                translate: { x: 0, y: 0 },
                opacity: 0.4
            }).then(() => { }, () => { });
        }
        // reset border bottom color.
        textField.borderBottomColor = new Color('#4b6ca5');
    }
}
