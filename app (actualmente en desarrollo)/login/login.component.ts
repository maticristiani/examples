import { Component, ElementRef, NgZone, OnInit, OnDestroy, ViewChild } from "@angular/core";
import { InternetConnectionService } from "../shared/services/internet-connection.service";
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import * as appSettings from "tns-core-modules/application-settings";
import { RouterExtensions } from "nativescript-angular/router";
import { ApiService } from "../shared/services/api.service";
import * as firebase from "nativescript-plugin-firebase";
import * as appversion from "nativescript-appversion";
import { localize } from "nativescript-localize";
import { Page } from "tns-core-modules/ui/page";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "Login",
    moduleId: module.id,
    templateUrl: "./login.component.html",
    styleUrls: ["./login.component.css"],
    animations: [ ]
})

export class LoginComponent implements OnInit, OnDestroy {
    connection$;
    user$;
    
    @ViewChild("email",{ static: false }) email: any;
    @ViewChild("password",{ static: false }) password: any;
    @ViewChild("modalRestore",{ static: false }) modalRestore: any;
    
    public dataUser: any;
    public connectionStatus: boolean;
    public dialogOpen: boolean;
    public isLoading: boolean;
    public version: string;
    private feedback: Feedback;

    constructor(
        private _internetConnection: InternetConnectionService,
        private _routerExtensions: RouterExtensions,
        private _apiService: ApiService,
        private _ngZone: NgZone,
        private page: Page) {

        this.feedback = new Feedback()
        this.page.actionBarHidden = true;
        this.page.backgroundSpanUnderStatusBar = true;
        this.page.className = "page-login-container";
        this.page.statusBarStyle = "dark";
        this.version = "";
        
        this.connectionStatus = true;
        this.isLoading = false;
        this.dialogOpen = false;
    }


    ngOnInit(): void {
        this.connection$ = this._internetConnection.connectionStatus$.subscribe(data => {
            console.dir("this.connectionStatus",this.connectionStatus)
            console.dir("dialogOpen:",this.dialogOpen)
            this.connectionStatus = data.valueOf();
        });
        
        this.user$ = this._apiService.User$.subscribe(value => {
            this.dataUser = value;
        })

        appversion.getVersionName().then((version) => {
            this.version = version;
            
            appversion.getVersionCode().then((build: string) => {
                      this.version += " - "+ build;
            });
        });
    }

    ngAfterViewInit(): void {
        
        this.email.textField.nativeElement.text = this.dataUser.email;
        this.password.textField.nativeElement.text = this.dataUser.password;

        if ( appSettings.getString('email') ){
            this.email.textField.nativeElement.text  =  appSettings.getString('email');

            if ( appSettings.getString('password') ) {
                // Encontre email & password grabado.
                this.password.textField.nativeElement.text =  appSettings.getString('password');
                return this.loginWithEmail()
            }else if ( appSettings.getString('token') ){
                return this.loginWithEmail()
            }

        }
    }

    ngOnDestroy(): void {
        if (this.connection$){
            this.connection$.unsubscribe();
        }
        if (this.user$){
            this.user$.unsubscribe();
        }
    }

    loginWithEmail() {
        console.dir(" loginWithEmail() ");
        console.dir('isLoading', this.isLoading);
        console.dir('connectionStatus', this.connectionStatus);

        if(this.isLoading || !this.connectionStatus) return;
        let email, password, type, uid, token = null;
        
        email = this.email.textField.nativeElement.text ? this.email.textField.nativeElement.text : '';
        password = this.password.textField.nativeElement.text ? this.password.textField.nativeElement.text : '';
        token = appSettings.getString('token') ? appSettings.getString('token') : null;

        if (email == '' || !email) {
           this.showWarning(localize('login.warning_1'));
           return;
        }

        if (!token) {
            if (password == '' || !password) {
               this.showWarning(localize('login.warning_2'));
               return;
            }   
        }
          
        this.isLoading = true;

        this._apiService.postCredentials(email, password, type, uid, token).then((res: any)=>{
            
            if (res.status != 'success'){
                this.isLoading = false;
                this.showWarning(res.message);
                return;
            }

            this.isLoading = false;

            this._apiService.setUser({ 
                id: res.data.id,
                authorization: res.data.authorization,
                email: res.data.email, 
                password: res.data.password, 
                token: false,
                alias: (res.data.alias ? res.data.alias : res.data.name ? res.data.name :  ''), 
                nombre: (res.data.name ? res.data.name : ''), 
                apellido: (res.data.surname ? res.data.surname : ''), 
                tel: (res.data.phone_number ? res.data.phone_number : ''),
                tel_aux: ""
            });
            
            
            appSettings.setString('email', email)
            appSettings.setString('password', password)
            appSettings.setString('authentication', res.data.authorization);

            setTimeout(()=>{
                this.navigateHome();
            },100)

        })
    }

    loginWithGoogle() {
        console.dir(" loginWithGoogle() ");
        if(this.isLoading || !this.connectionStatus) return;
        
        let email, password, type, uid, token = null;
        
        this.isLoading = true;

        firebase.login({
            type: firebase.LoginType.GOOGLE,
            // Optional 
            googleOptions: {
              hostedDomain: "mygsuitedomain.com",
              // NOTE: no need to add 'profile' nor 'email', because they are always provided
              // NOTE 2: requesting scopes means you may access those properties, but they are not automatically fetched by the plugin
              scopes: [
                "https://www.googleapis.com/auth/contacts.readonly",
                "https://www.googleapis.com/auth/user.addresses.read",
                "https://www.googleapis.com/auth/user.phonenumbers.read",
                "https://www.googleapis.com/auth/userinfo.profile"
              ]
            }

        }).then( (result) => {
                console.dir("result socialGoogle");
                console.dir(result);
         
                //get uid of google
                uid = result.uid;

                //get email of google
                email = result.email;
                
                //get token of google
                result.providers.forEach((p)=>{
                    console.dir(p.id);
                    if(p.id == "google.com"){
                        type = p.id;
                        token = p.token
                    }
                })

                this._apiService.postCredentials(email, password, type, uid, token).then((res: any)=>{
                    
                    if (res.status != 'success'){
                        this.isLoading = false;
                        this.showWarning(res.message);
                        return;
                    }

                    this.isLoading = false;

                    this._apiService.setUser({ 
                        id: res.data.id,
                        authorization: res.data.authorization,
                        email: res.data.email, 
                        password: res.data.password, 
                        token: false,
                        alias: (res.data.alias ? res.data.alias : ''), 
                        nombre: (res.data.name ? res.data.name : ''), 
                        apellido: (res.data.surname ? res.data.surname : ''), 
                        tel: (res.data.phone_number ? res.data.phone_number : ''),
                        tel_aux: ""
                    });
                    
                    appSettings.setString('email', email)                    
                    appSettings.setString('token', token)
                    appSettings.setString('authentication', res.data.authorization);
                    
                    setTimeout(()=>{
                        this.navigateHome();
                    },100)

                })

        }, (errorMessage) => {
                setTimeout(()=>{ this.isLoading = false;}, 400) 

        });
    }

    loginWithFacebook() {
        console.dir(" LoginWithFacebook() ");

        if(this.isLoading || !this.connectionStatus) return;
        
        let email, password, type, uid, token = null;
        
        this.isLoading = true;

        firebase.login({
            type: firebase.LoginType.FACEBOOK,
            // Optional
            facebookOptions: {
              // defaults to ['public_profile', 'email']
              scopes: ['public_profile', 'email'] // note: this property was renamed from "scope" in 8.4.0
            }
        }).then((result) => {
                console.dir("result socialFacebook");
                console.dir(result);
                
                //get uid of facebook
                uid = result.uid;

                //get email of facebook
                email = result.email;
                
                //get token of facebook
                result.providers.forEach((p)=>{
                    console.dir(p.id);
                    if(p.id == "facebook.com"){
                        type = p.id;
                        token = p.token
                    }
                })

                this._apiService.postCredentials(email, password, type, uid, token).then((res: any)=>{
                    
                    if (res.status != 'success'){
                        this.isLoading = false;
                        this.showWarning(res.message);
                        return;
                    }

                    this.isLoading = false;

                    this._apiService.setUser({ 
                        id: res.data.id,
                        authorization: res.data.authorization,
                        email: res.data.email, 
                        password: res.data.password, 
                        token: false,
                        alias: (res.data.alias ? res.data.alias : ''), 
                        nombre: (res.data.name ? res.data.name : ''), 
                        apellido: (res.data.surname ? res.data.surname : ''), 
                        tel: (res.data.phone_number ? res.data.phone_number : ''),
                        tel_aux: ""
                    });
                    
                    appSettings.setString('email', email)                    
                    appSettings.setString('token', token)
                    appSettings.setString('authentication', res.data.authorization);
                    
                    setTimeout(()=>{
                        this.navigateHome();
                    },100)

                })
                

        }, (errorMessage) => {
                setTimeout(()=>{ this.isLoading = false;}, 400) 

        });
    }

    public restoreAccount() { 
        if(this.isLoading || !this.connectionStatus) return;
        this.dialogOpen = true;
            
        setTimeout(()=>{ this.modalRestore.openFocusOnTextField() },300);
    }


    /* ActionsOfModals */

    public cancelRestoreAccount(args) { 
        console.dir('@cancelRestoreAccount');
        this.dialogOpen = false;
        
    }

    public submitRestoreAccount(email){
        console.dir('@submitRestoreAccount');
        this.dialogOpen = false;
        this.isLoading = true;

        this._apiService.postRestoreAccount(email).then((res: any)=>{

            if (res.status != 'success'){
                this.isLoading = false;
                this.showWarning(res.message);
                return;
            }
            
            this.isLoading = false;
            this.showSuccess(localize('login.confirm_restore'))       
        })
    }    

    showWarning(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Warning,
              backgroundColor: new Color("#df4a32"),
        });
    }


    showSuccess(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Success,
              backgroundColor: new Color("#95ba65"),
        });
    }

    /* Navigation's */
    
    public navigateToProfile() {
        console.dir('@navigateToProfile');
        if(this.isLoading || !this.connectionStatus) return;
        this._ngZone.run(() => {
            this._routerExtensions.navigate(
                    ["profile", false ], {
                    clearHistory: true,
                    animated: true,
                    transition: {
                        name: "slideLeft",
                        duration: 350,
                        curve: "ease"
                    }
            })
        })
    }

    public navigateToRegister() {
        console.dir('@navigateToRegister');
        if(this.isLoading || !this.connectionStatus) return;
        this._ngZone.run(() => {
            this._routerExtensions.navigate(["register"], {
                    clearHistory: true,
                    animated: true,
                    transition: {
                        name: "slideLeft",
                        duration: 350,
                        curve: "ease"
                    }
            })
        })
    }

    public navigateHome() {
        console.dir('@navigateHome');
        if(this.isLoading || !this.connectionStatus) return;
        this._ngZone.run(() => {
            this._routerExtensions.navigate(["home"], {
                        clearHistory: true,
                        animated: true,
                        transition: {
                            name: "slideLeft",
                            duration: 350,
                            curve: "ease"
                        }
            })
        })
    }

   

}
