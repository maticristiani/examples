import { AndroidApplication, AndroidActivityBackPressedEventData } from "tns-core-modules/application";
import { registerElement } from "nativescript-angular/element-registry";
import { Component, OnInit, OnDestroy, NgZone } from "@angular/core";
import { Carousel, CarouselItem } from 'nativescript-carousel';
import * as application from "tns-core-modules/application";
import { ApiService } from "./shared/services/api.service";
import * as firebase from "nativescript-plugin-firebase";
import { messaging, Message } from "nativescript-plugin-firebase/messaging";
import { RouterExtensions } from "nativescript-angular";
import { Place } from 'nativescript-google-places-sdk';
import { isAndroid } from "tns-core-modules/platform";
import { MapboxView } from 'nativescript-mapbox';
import { Video } from 'nativescript-videoplayer';
import { Router } from "@angular/router";

registerElement("Mapbox", () => MapboxView);
registerElement('Carousel', () => Carousel);
registerElement('CarouselItem', () => CarouselItem);
registerElement("VideoPlayer", () => Video);


@Component({
    moduleId: module.id,
    selector: "ns-app",
    templateUrl: "app.component.html"
})

export class AppComponent implements OnInit, OnDestroy{
 	config$;

 	constructor(private _apiService: ApiService, 
				private _ngZone: NgZone,
 				private _router: Router, 
 				private _routerExtensions: RouterExtensions){

 	}

	ngOnInit(){
	 	console.dir('ngOnInit')
	 	/* https://medium.com/@renzocastro/nativescript-push-notifications-with-firebase-cloud-messaging-2a4a8643a8cb */
		

		messaging.registerForPushNotifications({
		  onPushTokenReceivedCallback: (token: string): void => {
		    console.log("Firebase plugin received a push token: " + token);
		  },

		  onMessageReceivedCallback: (message: Message) => {
		    console.log("Push message received: " + message.title);
		  },

		  // Whether you want this plugin to automatically display the notifications or just notify the callback. Currently used on iOS only. Default true.
		  showNotifications: true,

		  // Whether you want this plugin to always handle the notifications when the app is in foreground. Currently used on iOS only. Default false.
		  showNotificationsWhenInForeground: true
		}).then(() => console.log("Registered for push"));


		firebase.subscribeToTopic("general").then(() => console.log("Subscribed to general"));
		firebase.subscribeToTopic("ordenes").then(() => console.log("Subscribed to ordenes"));
		firebase.subscribeToTopic("conversaciones").then(() => console.log("Subscribed to conversaciones"));


	 	this.config$ = this._apiService.Config$.subscribe(config => {
	 		console.dir(config.android_api_key);
	 		Place.initialize(isAndroid ? config.android_api_key : config.ios_api_key);  
	 	})

	 	if (!isAndroid) {
      		return;
      	}

      	application.android.on(AndroidApplication.activityBackPressedEvent, (data: AndroidActivityBackPressedEventData) => {
	      
	      if (this._router.isActive("/login", false)) {
	      	console.dir('navigateBreak')
	        data.cancel = true; // prevents
	      }


	      if (this._router.isActive("/register", false)) {
	      	console.dir('navigateBreak')
	        data.cancel = true; // prevents
	        this.navigateTo(['login']);
	      }

		  if (this._router.isActive("/profile", false)) {
		  	console.dir('navigateBreak')
	        data.cancel = true; // prevents
	      }

	      if (this._router.isActive("/cambiar-direccion", false)) {
	      	console.dir('navigateBreak')
	        data.cancel = true; // prevents
	      }

	      if (this._router.isActive("/nueva-direccion", false)) {
	      	console.dir('navigateBreak')
	        data.cancel = true; // prevents
	      }

	      if (this._router.isActive("/confirmar-direccion", false)) {
	      	console.dir('navigateBreak')
	        data.cancel = true; // prevents
	      }


	      if (this._router.isActive("/seleccionar-tienda", false)) {
			console.dir('navigateBreak')
	        data.cancel = true; // prevents
	      }

	      if (this._router.isActive("/nueva-conversacion", false) || this._router.isActive("/conversacion", false) ) {
	      	console.dir('navigateBreak')
	        data.cancel = true; // prevents
	        this.navigateTo(['conversaciones']);
	      }

	      if (
	      	this._router.isActive("/conversaciones", false) ||
	        this._router.isActive("/notificaciones", false) ||
	        this._router.isActive("/preguntas-frecuentes", false) ||
	        this._router.isActive("/catalogo", false) ||
	        this._router.isActive("/ordenes", false) 
	      	) 
	      {
			console.dir('navigateBreak')
			this.navigateTo(['home']);
	        data.cancel = true; // prevents
	      }


	      if (this._routerExtensions.canGoBack()) {
          	    console.dir('navigateBreak - History')
                data.cancel = true;
                this._routerExtensions.back();
                return;
          }


	    });
	}

	ngOnDestroy(): void {
		console.dir('ngOnDestroy')
		if (this.config$)
            this.config$.unsubscribe();
	}

	/* NAVIGATION */
	navigateTo(opt) {
	 	this._ngZone.run(()=>{
        	this._routerExtensions.navigate(opt,{
                        clearHistory: true,
                        animated: true,
                        transition: {
                            name: 'slideLeft',
                            duration: 350,
                            curve: "ease"
                        }
            });
        })
    }
}

