import { Injectable } from "@angular/core";
import { CanLoad } from "@angular/router";
import * as appSettings from "tns-core-modules/application-settings";

// TODO: should be imported from kinvey-nativescript-sdk/angular but declaration file is currently missing
import { RouterExtensions } from "nativescript-angular/router";

@Injectable()
export class LoggedInLazyLoadGuard implements CanLoad {
	private authentication: string;
    
    constructor(private _routerExtensions: RouterExtensions) { 

    }

    canLoad(): boolean {
        
        if (appSettings.getString('authentication')) {
         this.authentication = appSettings.getString('authentication');
        }

        if (!this.authentication) {
             this._routerExtensions.navigate(["login"], { clearHistory: true });
        }

        return true;
    }
}
