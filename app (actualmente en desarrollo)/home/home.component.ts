import { Component, ElementRef, OnInit, OnDestroy, NgZone } from "@angular/core";
import { InternetConnectionService } from "../shared/services/internet-connection.service";
import { Feedback, FeedbackType, FeedbackPosition } from "nativescript-feedback";
import { trigger, state, transition, style, animate } from '@angular/animations';
import { ObservableArray } from "tns-core-modules/data/observable-array";
import { StackLayout } from "tns-core-modules/ui/layouts/stack-layout";
import { RouterExtensions } from "nativescript-angular/router";
import { EventData } from "tns-core-modules/data/observable";
import { ApiService } from "../shared/services/api.service";
import * as appversion from "nativescript-appversion";
import { localize } from "nativescript-localize";
import { Page } from "tns-core-modules/ui/page";
import { Color } from "tns-core-modules/color";

@Component({
    selector: "Home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
    styleUrls: ["./home.component.css"],
    animations: [
    
        trigger('slide-left', [
          transition(':enter', [
            style({transform: 'translateX(-200%)'}),
            animate('1500ms ease-in', style({transform: 'translateX(0%)'}))
          ]),
          transition(':leave', [
            animate('1500ms ease-in', style({transform: 'translateX(-200%)'}))
          ])
        ]),


        trigger('slide-right', [
          transition(':enter', [
            style({transform: 'translateX(200%)'}),
            animate('1500ms ease-in', style({transform: 'translateX(0%)'}))
          ]),
          transition(':leave', [
            animate('1500ms ease-in', style({transform: 'translateX(200%)'}))
          ])
        ])

    ]
})

export class HomeComponent implements OnInit, OnDestroy{
    user$;
    store$;
    direction$;
    connection$;

    public connectionStatus: boolean;
    public dataStore: any;
    public dataDirection: any;

    public dataUserWelcome: string;
    public dataSliders: any;
    public version: string; 

    public enBandeja: number;

    private feedback: Feedback;

    constructor(
        private _apiService: ApiService,
        private _internetConnection: InternetConnectionService,
        private _routerExtensions: RouterExtensions,
        private _ngZone: NgZone,
        private page: Page) {
          
        this.page.actionBarHidden = false;
        this.page.backgroundSpanUnderStatusBar = false;
        this.page.className = "page-home-container";
        this.page.statusBarStyle = "dark";
        this.version = "";
        this.feedback = new Feedback()
        this.enBandeja = 0;
        this.dataUserWelcome = ''
        this.dataSliders = [{ titulo: '', src:'~/assets/home/sin-slider.png'  }]
    }

    ngOnInit(): void {
        this.connection$ = this._internetConnection.connectionStatus$.subscribe(data => {
            this.connectionStatus = data.valueOf();
        });

        this.store$ = this._apiService.Store$.subscribe(value => {
            this.dataStore = value;
        })
        
        this.direction$ = this._apiService.Direction$.subscribe(value => {
            console.dir(value);
            this.dataDirection = value;
        })
        
        appversion.getVersionName().then((version) => {
            this.version = version;
            
            appversion.getVersionCode().then((build: string) => {
                      this.version += " - "+ build;
            });
        });

        this.user$ = this._apiService.User$.subscribe(value => {
            this.dataUserWelcome = (value.alias) ? (localize('home.hi') +' '+value.alias) : localize('home.hi');
        
            this._apiService.getSliders(value.authorization, false).then((res: any)=>{
                console.dir(res);

                if (res.status != 'success'){
                    console.dir('ups getSldiers');
                    return;
                }
              
                if (!res.data || res.data.length == 0) {
                  return   
                }
                
                this.dataSliders = res.data;   
            })
            
            this._apiService.getNotifications(value.authorization, true).then((res: any)=>{
                console.dir(res);

                if (res.status != 'success'){
                    console.dir('ups getNotificaciones')
                }
                
                if (res.data) {
                    this.enBandeja = Number(res.data);   
                }

                console.dir("this.enBandeja", this.enBandeja);
            })
        })
    }


    ngOnDestroy(): void {
        if (this.connection$){
            this.connection$.unsubscribe();
        }
    
        if (this.user$) {
            this.user$.unsubscribe();
        }

        if (this.direction$){
            this.direction$.unsubscribe();
        }
    
        if (this.store$) {
            this.store$.unsubscribe();
        }
    }

    onMenuButtonTap(args: EventData) {
        const menuButtonParent = (<StackLayout>args.object).parent;
        let page = menuButtonParent.get("data-name");
        
        console.log("Navigate to "+page);

        if(!this.connectionStatus) return;
        
        switch (page) {
            case "profile":

                this._routerExtensions.navigate(
                    ['profile', false],
                    {
                        clearHistory: false,
                        animated: true,
                        transition: {
                            name: 'slideLeft',
                            duration: 350,
                            curve: "ease"
                        }
                    });
                break;
            case "notificaciones":
                this.navigateTo("slideRight", "notificaciones");
                break;

            case "nueva-orden":

                if (!this.dataStore.alias) {
                    this.showWarning(localize('home.error_order_direction'));
                    // return;

                }

                if (!this.dataDirection.alias) {
                    this.showWarning(localize('home.error_order_store'));
                    // return;
                }
                
                this.navigateTo("slideLeft",'catalogo');
                break;
            case "ordenes":
                this.navigateTo("slideRight",'ordenes');
                break;

            case "conversaciones":
                this.navigateTo("slideLeft",'conversaciones');
                break;
            case "preguntas-frecuentes":
                this.navigateTo("slideRight",'preguntas-frecuentes');
                break;
        }

    }

    /* feedbacks */
    
    showWarning(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Warning,
              backgroundColor: new Color("#df4a32"),
        });
    }

    showSuccess(message){
        this.feedback.warning({
              message: message,
              position: FeedbackPosition.Top,
              type: FeedbackType.Success,
              backgroundColor: new Color("#95ba65"),
        });
    }

    /* navigations */

    navigateTo(transition, page) {
        this._routerExtensions.navigate([page],{
                        clearHistory: false,
                        animated: true,
                        transition: {
                            name: transition,
                            duration: 350,
                            curve: "ease"
                        }
                    });
    }

    changeDirection(args) {
        if(!this.connectionStatus) return;

        if (!this.dataStore || this.dataStore.alias.length <= 1) {
               this.showWarning(localize('home.error_select_direction'));
            // return;
        }
        this._ngZone.run(() => {
            this._routerExtensions.navigate(["cambiar-direccion"],
                    {
                        clearHistory: false,
                        animated: true,
                        transition: {
                            name: "slideTop",
                            duration: 350,
                            curve: "ease"
                        }
            })
         })
    }

    changeStore(args) {
        if(!this.connectionStatus) return;
        this._ngZone.run(() => {
         this._routerExtensions.navigate(
             ["seleccionar-tienda", false],
                    {
                        clearHistory: false,
                        animated: true,
                        transition: {
                            name: "slideBottom",
                            duration: 350,
                            curve: "ease"
                        }
         })
        })
    }

}
