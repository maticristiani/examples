import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptModule } from "nativescript-angular/nativescript.module";
import { NativeScriptAnimationsModule } from "nativescript-angular/animations";

/* Http */
import { NativeScriptHttpClientModule } from "nativescript-angular/http-client";

/* MultiLang */
import { NativeScriptLocalizeModule } from "nativescript-localize/angular";

/* Guard */
import { LoggedInLazyLoadGuard } from "./logged-in-lazy-load.guard";

import { ApiService } from "./shared/services/api.service"
import { AppRoutingModule } from "./app-routing.module";
import { AppComponent } from "./app.component";


@NgModule({
    bootstrap: [
        AppComponent
    ],
    imports: [
        NativeScriptAnimationsModule,
        NativeScriptHttpClientModule,
        NativeScriptLocalizeModule,
        NativeScriptModule,
        AppRoutingModule
    ],
    declarations: [
        AppComponent
    ],
    providers: [
        LoggedInLazyLoadGuard,
        ApiService
    ],
    exports: [
    ],
    schemas: [
        NO_ERRORS_SCHEMA
    ]
})
export class AppModule { }
