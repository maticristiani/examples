import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";
import { LoggedInLazyLoadGuard } from "./logged-in-lazy-load.guard";

const routes: Routes = [
    { path: "", redirectTo: "/login", pathMatch: "full" },
  
    { path: "login", loadChildren: "~/app/login/login.module#LoginModule" },  
    
    { path: "register", loadChildren: "~/app/register/register.module#RegisterModule" },
    // { path: "nueva-direccion-register", loadChildren: "~/app/nueva-direccion-register/nueva-direccion.module#NuevaDireccionModule" },
    // { path: "confirmar-direccion-register", loadChildren: "~/app/confirmar-direccion-register/confirmar-direccion.module#ConfirmarDireccionModule" },


    { path: "profile/:isregister", loadChildren: "~/app/profile/profile.module#ProfileModule" },
    
    { path: "cambiar-direccion", loadChildren: "~/app/cambiar-direccion/cambiar-direccion.module#CambiarDireccionModule" },
    { path: "nueva-direccion/:isregister", loadChildren: "~/app/nueva-direccion/nueva-direccion.module#NuevaDireccionModule" },
    { path: "confirmar-direccion/:isregister", loadChildren: "~/app/confirmar-direccion/confirmar-direccion.module#ConfirmarDireccionModule" },

    { path: "seleccionar-tienda/:register", loadChildren: "~/app/seleccionar-tienda/seleccionar-tienda.module#SeleccionarTiendaModule" },

    { path: "home", loadChildren: "~/app/home/home.module#HomeModule" , canLoad: [LoggedInLazyLoadGuard] },
    
    { path: "catalogo", loadChildren: "~/app/catalogo/catalogo.module#CatalogoModule" },
    { path: "confirmar-orden", loadChildren: "~/app/confirmar-orden/confirmar-orden.module#ConfirmarOrdenModule" },
    { path: "abonar-orden", loadChildren: "~/app/abonar-orden/abonar-orden.module#AbonarOrdenModule" },
    { path: "orden-confirmada/:ordenes/:id", loadChildren: "~/app/orden-confirmada/orden-confirmada.module#OrdenConfirmadaModule" },
    
    { path: "ordenes", loadChildren: "~/app/ordenes/ordenes.module#OrdenesModule" },
    { path: "orden/:id", loadChildren: "~/app/orden/orden.module#OrdenModule" },

    { path: "orden-pactar-entrega/:id", loadChildren: "~/app/orden-pactar-entrega/orden.module#OrdenPactarEntregaModule" },
    { path: "orden-calificar/:id", loadChildren: "~/app/orden-calificar/orden.module#OrdenCalificarModule" },



    { path: "conversaciones", loadChildren: "~/app/conversaciones/conversaciones.module#ConversacionesModule" },
    { path: "conversacion/:id", loadChildren: "~/app/conversacion/conversacion.module#ConversacionModule" },
    { path: "nueva-conversacion", loadChildren: "~/app/nueva-conversacion/nueva-conversacion.module#NuevaConversacionModule" },
    
    { path: "notificaciones", loadChildren: "~/app/notificaciones/notificaciones.module#NotificacionesModule" },
    { path: "preguntas-frecuentes", loadChildren: "~/app/preguntas/preguntas.module#PreguntasModule" }
   
];

@NgModule({
    imports: [NativeScriptRouterModule.forRoot(routes)],
    exports: [NativeScriptRouterModule]
})

export class AppRoutingModule { }
