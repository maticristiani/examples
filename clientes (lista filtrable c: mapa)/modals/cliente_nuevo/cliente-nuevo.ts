import { Component, OnInit, ViewChild } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
import { RadDataFormComponent } from "nativescript-ui-dataform/angular";

import * as utilsModule from "tns-core-modules/utils/utils";
import { isAndroid } from "tns-core-modules/platform";
import { Color } from "tns-core-modules/color";
import { RadDataForm, DataFormEventData } from "nativescript-ui-dataform";

import { ListPicker } from "tns-core-modules/ui/list-picker";

declare var android: any;
declare var UIEdgeInsets;

declare var UIPickerView;
declare var UIToolbar;
declare var UIColor;
declare var UIBarStyle;
declare var UIBarButtonItem;
declare var UIBarButtonItemStyle;
declare var UIBarButtonSystemItem;

class ClienteNuevo {
    constructor(
        public nombre: string,
        public cond_iva: any,
        public zona: any,
        public cuit: string,
        public calle: string,
        public numero: number,
        public localidad: string,
        public provincia: string,
        public pais: string,
        public telefono: string,
        public email: string,
        public f_pago: any,
        public f_transporte: number,
        public p_lista: number,
        public descuento: number
    ) {}
}

// >> passing-parameters
@Component({
    moduleId: module.id,
    templateUrl: "./cliente-nuevo.html"
})
export class modalNuevoDos implements OnInit {
    public _text: string;
    public _isAndroid: boolean;
    private _editorPaddingHorizontal: number;
    private _editorPaddingVertical: number;
    private _coreEditorPaddingHorizontal: number;
    private _coreEditorPaddingVertical: number;
    private _strokeWidth: number;
    private _cornerRadius: number;
    private _strokeColor: Color;
    private _fillColor: Color;

    @ViewChild("customDataForm", {static: false}) customDataForm: RadDataFormComponent;

    public _cliente: ClienteNuevo;

    public arrayPagos: Map<number, string>;
    public arrayTransporte: Map<number, string>;
    public arrayLista: Map<number, string>;
    public arrayIva: Map<number, string>;
    public arrayZona: Map<number, string>;
    public arrayProvincia: Map<string, string>;
    public arrayPais: Map<string, string>;

    constructor(private _params: ModalDialogParams) {

        this._isAndroid = isAndroid;
        this._editorPaddingHorizontal = 10;
        this._editorPaddingVertical = 6;
        this._coreEditorPaddingHorizontal = 10;
        this._coreEditorPaddingVertical = 6;
        this._strokeWidth = 1;
        this._cornerRadius = 4;
        this._strokeColor = new Color("DarkGray");
        this._fillColor = new Color("#00000000");
        
        this.arrayPagos = new Map([[1, "CARGANDO"]]);

        this.arrayTransporte = new Map([[1, "CARGANDO"]]);

        this.arrayLista = new Map([
            [1, "LISTA 1"],
            [2, "LISTA 2"],
            [3, "LISTA 3"],
            [4, "LISTA 4"],
            [5, "LISTA 5"],
            [6, "LISTA 6"],
            [7, "LISTA 7"],
            [8, "LISTA 8"],
            [9, "LISTA 9"],
            [10, "LISTA 10"],
            [11, "LISTA 11"],
            [12, "LISTA 12"],
            [13, "LISTA 13"],
            [14, "LISTA 14"],
            [15, "LISTA 15"],
            [16, "LISTA 16"],
            [17, "LISTA 17"],
            [18, "LISTA 18"],
            [19, "LISTA 19"],
            [20, "LISTA 20"],
            [21, "LISTA 21"],
            [22, "LISTA 22"],
            [23, "LISTA 23"],
            [24, "LISTA 24"],
            [25, "LISTA 25"],
            [26, "LISTA 26"],
            [27, "LISTA 27"],
            [28, "LISTA 28"],
            [29, "LISTA 29"],
            [30, "LISTA 30"]
        ]);

        this.arrayIva = new Map([
            [1, "IVA Responsable Inscripto"],
            [2, "IVA Exento"],
            [3, "Consumidor Final"],
            [4, "Monotributo"],
            [5, "Zona Franca"]
        ]);

        this.arrayZona = new Map([
            [1, "Domingo"],
            [2, "Lunes"],
            [3, "Martes"],
            [4, "Miercoles"],
            [5, "Jueves"],
            [6, "Viernes"],
            [7, "Sabado"]
        ]);

        this.arrayProvincia = new Map([["Sin definir", "Seleccionar país"]]);

        this.arrayPais = new Map([
            ["Argentina", "Argentina"],
            ["Brasil", "Brasil"],
            ["Bolivia", "Bolivia"],
            ["Chile", "Chile"],
            ["Colombia", "Colombia"],
            ["Ecuador", "Ecuador"],
            ["España", "España"],
            ["Paraguay", "Paraguay"],
            ["Uruguay", "Uruguay"],
            ["Venezuela", "Venezuela"]
        ]);
        
        this._cliente = {
            nombre: "",
            cond_iva: "",
            zona: "",
            cuit: "",
            numero: 0,
            calle: "",
            localidad: "",
            provincia: "",
            pais: "",
            telefono: "",
            email: "",
            f_pago: 1,
            f_transporte: 1,
            p_lista: 1,
            descuento: 0
        };
    }

    ngOnInit() {
        console.log("@ngOnInit");
    }

    get cliente(): ClienteNuevo {
        return this._cliente;
    }



    onPropertyCommitted(args){
        let _result = null;
        _result = this.customDataForm.dataForm.editedObject;
        var object = JSON.parse(_result);
        
        if(object.pais){
           let  res = this.getProvincias(object.pais)
           this.arrayProvincia = new Map(res)
        }
    }



    public initValores() {
        let obj = this._params.context;
        console.log("@initValores");

        if (obj.zonas) {
            this.arrayZona = new Map();
            if (obj.zonas.length > 0) {
                obj.zonas.forEach(z => {
                    this.arrayZona.set(z.id, z.nombre);
                });
            }
        }

        if (obj.pagos) {
            this.arrayPagos = new Map();
            if (obj.pagos.length > 0) {
                obj.pagos.forEach(p => {
                    this.arrayPagos.set(p.id, p.nombre);
                });
            }
        }

        if (obj.transportes) {
            this.arrayTransporte = new Map();
            if (obj.transportes.length > 0) {
                obj.transportes.forEach(t => {
                    this.arrayTransporte.set(t.id, t.nombre);
                });
            }
        }


        if (obj.condiciones_iva) {
            this.arrayIva = new Map();
            if (obj.condiciones_iva.length > 0) {
                obj.condiciones_iva.forEach(ci => {
                    this.arrayIva.set(ci.id, ci.nombre);
                });
            }
        }
        
        try{
            this.customDataForm.dataForm.commitAll();
        }catch(e){
            console.dir('AAAA')
        }
    }

    public onTap() {
        let isValid = true;

        //revisar-nombre
        let nombre: any = this.customDataForm.dataForm.getPropertyByName(
            "nombre"
        );
        //revisar-cuit
        let cuit = this.customDataForm.dataForm.getPropertyByName("cuit");
        //revisar-cuit
        let cond_iva = this.customDataForm.dataForm.getPropertyByName(
            "cond_iva"
        );
        //revisar-tel
        let tel = this.customDataForm.dataForm.getPropertyByName("telefono");
        //revisar-mail
        let email = this.customDataForm.dataForm.getPropertyByName("email");
        //revisar-desc
        let desc = this.customDataForm.dataForm.getPropertyByName("descuento");

        // validator-nombre
        this.esNombre(nombre.valueCandidate).then((res_1: any) => {
            if (!res_1.estado) {
                isValid = false;
                nombre.errorMessage = res_1.message;
                this.customDataForm.dataForm.notifyValidated("nombre", false);
            } else {
                this.customDataForm.dataForm.notifyValidated("nombre", true);
            }
            console.dir("validate-name");
        });

        // validator-iva
        this.esIva(cond_iva.valueCandidate).then((res_2: any) => {
            if (!res_2.estado) {
                isValid = false;
                cond_iva.errorMessage = res_2.message;
                this.customDataForm.dataForm.notifyValidated("cond_iva", false);
            } else {
                this.customDataForm.dataForm.notifyValidated("cond_iva", true);
            }
            console.dir("validate-iva");
        });

        // validator-cuit
        this.esCuit(cuit.valueCandidate).then((res_3: any) => {
            if (!res_3.estado) {
                isValid = false;
                cuit.errorMessage = res_3.message;
                this.customDataForm.dataForm.notifyValidated("cuit", false);
            } else {
                this.customDataForm.dataForm.notifyValidated("cuit", true);
            }
            console.dir("validate-cuit");
        });

        // validator-tel
        this.esTel(tel.valueCandidate).then((res_5: any) => {
            if (!res_5.estado) {
                isValid = false;
                tel.errorMessage = res_5.message;
                this.customDataForm.dataForm.notifyValidated("telefono", false);
            } else {
                this.customDataForm.dataForm.notifyValidated("telefono", true);
            }
            console.dir("validate-tel");
        });

        // validator-mail
        this.esMail(email.valueCandidate).then((res_6: any) => {
            if (!res_6.estado) {
                isValid = false;
                email.errorMessage = res_6.message;
                this.customDataForm.dataForm.notifyValidated("email", false);
            } else {
                this.customDataForm.dataForm.notifyValidated("email", true);
            }
            console.dir("validate-email");
        });

        // validator-desc
        this.esDesc(desc.valueCandidate).then((res_7: any) => {
            console.dir(res_7);
            if (!res_7.estado) {
                isValid = false;
                desc.errorMessage = res_7.message;
                this.customDataForm.dataForm.notifyValidated(
                    "descuento",
                    false
                );
            } else {
                this.customDataForm.dataForm.notifyValidated("descuento", true);
            }
            console.dir("validate-desc");
            this.corroborarGeneral(isValid);
        });
    }

    corroborarGeneral(isValid) {
        this._text = null;
        console.dir("validate-final");

        if (!isValid) {
            this._text = "Hay datos incorrectos en el formulario.";
        } else {
            this._text = "";
            this.customDataForm.dataForm.commitAll();

            //set-provincia
            // let prov_nro : number = Number(this.customDataForm.dataForm.getPropertyByName("provincia"));

            let _result = null;
            _result = this.customDataForm.dataForm.editedObject;
            var object = JSON.parse(_result);
            this._params.closeCallback(object);
        }
    }

    private esDesc(value) {
        return new Promise((resolve, reject) => {
            let valido = { estado: true, message: "ERROR" };

            //nulo-cero
            if (Number(value) >= 0 && Number(value) <= 100) {
                resolve(valido);
            } else {
                valido.estado = false;
                valido.message = "El campo descuento debe tener un valor.";
                resolve(valido);
            }
        });
    }

    private esMail(value) {
        return new Promise((resolve, reject) => {
            let valido = { estado: true, message: "ERROR" };
            let pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z.]{2,8}$/;

            //nulo-cero
            if (value.length == 0 || !value) {
                valido.estado = false;
                valido.message = "El campo email debe tener un valor.";
            }

            //caracteres-valido
            if (!value.match(pattern)) {
                valido.estado = false;
                valido.message = "El formato de email es incorrecto.";
            }

            resolve(valido);
        });
    }

    private esTel(value) {
        return new Promise((resolve, reject) => {
            let valido = { estado: true, message: "ERROR" };
            let letters = /^[0-9]+$/;

            //nulo-cero
            if (value.length == 0 || !value) {
                valido.estado = false;
                valido.message = "El campo telefono debe tener un valor.";
            }

            //nulo-cero
            if (value && value.length > 50) {
                valido.estado = false;
                valido.message =
                    "El campo telefono debe ser menor a 20 caracteres.";
            }

            //caracteres-valido
            // if (value.search(/[^a-zA-Z]+/) === -1) {
            //     valido.estado = false;
            //     valido.message = 'Solo se aceptan caracteres alfabeticos.';
            // }

            if (!value.match(letters)) {
                valido.estado = false;
                valido.message = "Solo se aceptan caracteres numericos (0-9).";
            }

            resolve(valido);
        });
    }

    private esIva(value) {
        return new Promise((resolve, reject) => {
            let valido = { estado: true, message: "ERROR" };
            let letters = /^[A-Za-z0-9]+$/;

            //nulo-cero
            if (value.length == 0 || !value) {
                valido.estado = false;
                valido.message = "El campo iva debe tener un valor.";
            }

            resolve(valido);
        });
    }

    private esNombre(value) {
        return new Promise((resolve, reject) => {
            let valido = { estado: true, message: "ERROR" };
            let letters = /^[A-Za-z0-9 {0,}]+$/;

            //nulo-cero
            if (value.length == 0 || !value) {
                valido.estado = false;
                valido.message = "El campo nombre debe tener un valor.";
            }

            //nulo-cero
            if (value && value.length > 50) {
                valido.estado = false;
                valido.message =
                    "El campo nombre debe ser menor a 50 caracteres.";
            }

            //caracteres-valido
            // if (value.search(/[^a-zA-Z]+/) === -1) {
            //     valido.estado = false;
            //     valido.message = 'Solo se aceptan caracteres alfabeticos.';
            // }

            if (!value.match(letters)) {
                valido.estado = false;
                valido.message =
                    "Solo se aceptan caracteres alfabeticos (a-z) y números (0-9).";
            }

            resolve(valido);
        });
    }
    private esCuit(value) {
        return new Promise((resolve, reject) => {
            let valido = {
                estado: false,
                message: "El cuit ingresado es incorrecto."
            };
            let sCUIT: any = value.toString();
            let aMult: any = "5432765432";
            aMult = aMult.split("");
            let aCUIT: any;
            let i = 0;

            if (sCUIT && sCUIT.length == 11) {
                aCUIT = sCUIT.split("");
                var iResult = 0;

                for (i = 0; i <= 9; i++) {
                    iResult += aCUIT[i] * aMult[i];
                }
                iResult = iResult % 11;
                iResult = 11 - iResult;

                if (iResult == 11) iResult = 0;
                if (iResult == 10) iResult = 9;

                if (iResult == aCUIT[10]) {
                    valido.estado = true;
                }
            }

            //Si es otr pais omito la verificacion:
            if ( (this.cliente.pais.toLowerCase()).indexOf('arg') <=0 ) {
                valido.estado = true;
                valido.message = '';
            }

            resolve(valido);
        });
    }

    public submit() {
        this.onTap();
    }

    public close() {
        this._params.closeCallback();
    }

    private getProvincias(pais){
        let provincias = [];
        switch (pais.toLowerCase()) {
            case "argentina":
                provincias = [
                                ["Buenos Aires", "Buenos Aires"],
                                ["Buenos Aires Capital", "Buenos Aires Capital"],
                                ["Catamarca", "Catamarca"],
                                ["Chaco", "Chaco"],
                                ["Chubut", "Chubut"],
                                ["Cordoba", "Cordoba"],
                                ["Corrientes", "Corrientes"],
                                ["Entre Rios", "Entre Rios"],
                                ["Formosa", "Formosa"],
                                ["Jujuy", "Jujuy"],
                                ["La Pampa", "La Pampa"],
                                ["La Rioja", "La Rioja"],
                                ["Mendoza", "Mendoza"],
                                ["Misiones", "Misiones"],
                                ["Neuquen", "Neuquen"],
                                ["Rio Negro", "Rio Negro"],
                                ["Salta", "Salta"],
                                ["San Juan", "San Juan"],
                                ["San Luis", "San Luis"],
                                ["Santa Cruz", "Santa Cruz"],
                                ["Santa Fe", "Santa Fe"],
                                ["Santiago del Estero", "Santiago del Estero"],
                                ["Tierra del Fuego", "Tierra del Fuego"],
                                ["Tucuman", "Tucuman"]
                            ];
                break;

            case "brasil":
                // code...
                provincias = [["Sin definir", "Sin definir"]];
                break;

            case "bolivia":
                // code...
                provincias = [["Sin definir", "Sin definir"]];
                break;

            case "chile":
                // code...
                provincias = [["Sin definir", "Sin definir"]];
                break;

            case "colombia":
                // code...
                provincias = [["Sin definir", "Sin definir"]];
                break;

            case "ecuador":
                provincias = [
                                ["Azuay","Azuay"],
                                ["Bolívar","Bolívar"],
                                ["Cañar","Cañar"],
                                ["Carchi","Carchi"],
                                ["Chimborazo","Chimborazo"],
                                ["Cotopaxi","Cotopaxi"],
                                ["El Oro","El Oro"],
                                ["Esmeraldas","Esmeraldas"],
                                ["Galápagos","Galápagos"],
                                ["Guayas","Guayas"],
                                ["Imbabura","Imbabura"],
                                ["Loja","Loja"],
                                ["Los Ríos","Los Ríos"],
                                ["Manabí","Manabí"],
                                ["Morona Santiago","Morona Santiago"],
                                ["Napo","Napo"],
                                ["Orellana","Orellana"],
                                ["Pastaza","Pastaza"],
                                ["Pichincha","Pichincha"],
                                ["Santa Elena","Santa Elena"],
                                ["Santo Domingo de los Tsáchilas","Santo Domingo de los Tsáchilas"],
                                ["Sucumbíos","Sucumbíos"],
                                ["Tungurahua","Tungurahua"],
                                ["Zamora Chinchipe","Zamora Chinchipe"]
                        ];
                break;
            
            case "españa":
                // code...
                provincias = [["Sin definir", "Sin definir"]];
                break;

            case "paraguay":
                // code...
                provincias = [["Sin definir", "Sin definir"]];
                break;

            case "uruguay":
                // code...
                provincias = [["Sin definir", "Sin definir"]];
                break;

            case "venezuela":
                // code...
                provincias = [["Sin definir", "Sin definir"]];
                break;
        }

        return provincias;

    }

    public dfEditorUpdate(args: DataFormEventData) {
            
        let editor = args.editor;
        let editorType = (isAndroid) ? args.propertyName  :  (<RadDataForm>args.object).getPropertyByName(args.propertyName).editor.type;
 
        if (!this._editorNeedsUpdate(editorType)) {
            return;
        }

        console.log('OK');

        if (isAndroid) {
            // Update editor padding
            let editorOffsetH = utilsModule.layout.toDevicePixels(
                this._coreEditorPaddingHorizontal
            );
            let editorOffsetV = utilsModule.layout.toDevicePixels(
                this._coreEditorPaddingVertical
            );
            editor
                .rootLayout()
                .setPadding(
                    editorOffsetH,
                    editorOffsetV,
                    editorOffsetH,
                    editorOffsetV
                );

            // Update core editor padding
            let coreEditorView = editor.getEditorView();
            let textOffsetH = utilsModule.layout.toDevicePixels(
                this._coreEditorPaddingHorizontal
            );
            let textOffsetV = utilsModule.layout.toDevicePixels(
                this._coreEditorPaddingVertical
            );
            coreEditorView.setPadding(
                textOffsetH,
                textOffsetV,
                textOffsetH,
                textOffsetV
            );

            // Update core editor background
            let drawable = new android.graphics.drawable.GradientDrawable();
            drawable.setCornerRadius(
                utilsModule.layout.toDevicePixels(this._cornerRadius)
            );
            drawable.setStroke(
                utilsModule.layout.toDevicePixels(this._strokeWidth),
                this._strokeColor.android
            );
            drawable.setColor(this._fillColor.android);
            coreEditorView.setBackgroundDrawable(drawable);
        } else {
            this._coreEditorPaddingVertical = 4;

            // Update editor padding
            const editorOffsets = new UIEdgeInsets({
                top: this._coreEditorPaddingVertical,
                left: this._coreEditorPaddingHorizontal,
                bottom: this._coreEditorPaddingVertical,
                right: this._coreEditorPaddingHorizontal
            });
            editor.style.insets = editorOffsets;

            // Update core editor padding
            const textOffsets = new UIEdgeInsets({
                top: this._coreEditorPaddingVertical,
                left: this._coreEditorPaddingHorizontal,
                bottom: this._coreEditorPaddingVertical,
                right: this._coreEditorPaddingHorizontal
            });
            if (this._editorHasValueLabel(editorType)) {
                editor.showAccessoryImage = false;
                editor.editorValueLabel.textInsets = textOffsets;
            } else if (this._editorIsTextField(editorType)) {
                editor.editor.textInsets = textOffsets;
            }

            // Update core editor background
            const layer = this._editorHasValueLabel(editorType)
                ? editor.editorValueLabel.layer
                : editor.editor.layer;
            layer.borderColor = this._strokeColor.ios.CGColor;
            layer.borderWidth = this._strokeWidth;
            layer.cornerRadius = this._cornerRadius;


            switch ((<RadDataForm>args.object).getPropertyByName(args.propertyName).editor.type) {
                case "Picker":
                    this._editorPicker(args.editor);
                    break;
            }
        }
        console.log('OK 2');
    }

    private _editorPicker(editor) {
        if (!isAndroid) {
            let corepicker: ListPicker = editor.pickerView;
            corepicker.effectivePaddingTop = 50;
            corepicker.backgroundColor = new Color("#dadada").ios;
            corepicker.isUserInteractionEnabled = true;

        }
    }

    private _editorNeedsUpdate(type) {
        return this._editorHasValueLabel(type) || this._editorIsTextField(type);
    }

    private _editorHasValueLabel(type) {
        return type == "DatePicker" || type == "TimePicker" || type == "Picker";
    }

    private _editorIsTextField(type) {
        return (
            type == "Text" ||
            type == "Email" ||
            type == "Password" ||
            type == "Phone" ||
            type == "Number" ||
            type == "Decimal"
        );
    }
}