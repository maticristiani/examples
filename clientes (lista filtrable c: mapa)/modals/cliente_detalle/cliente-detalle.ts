import { Component, OnInit } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";
// import { RouterExtensions } from 'nativescript-angular/router'

@Component({
    moduleId: module.id,
    templateUrl: "./cliente-detalle.html"
})
export class modalDetalle implements OnInit {
    public cliente: any;

    constructor(
        // private _router: RouterExtensions,
        private _params: ModalDialogParams
    ) {}

    ngOnInit() {
        this.cliente = this._params.context;
    }

    public close() {
        // this._router.back();
        setTimeout(() => {
            this._params.closeCallback();
        }, 300);
    }
}