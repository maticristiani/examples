import { Component, OnInit } from "@angular/core";
import { ModalDialogParams } from "nativescript-angular/modal-dialog";

@Component({
    moduleId: module.id,
    templateUrl: "./cliente-ctacte.html",
})

export class modalCtaCte implements OnInit {
    
    public deudas: Array<any>;
    public cliente: string;
    public saldo: number;
    
    public templateSelector = (item: any, index: number, items: any) => {
            return "impaga";
    }

    constructor(
        private _params: ModalDialogParams) {
        this.deudas = new Array<any>();
        this.cliente = "CLIENTE";
        this.saldo = 0;
    }

    ngOnInit() {
      
       this.deudas = this._params.context;
       
       this.deudas.forEach((d,i)=>{
           if (i===0 && d.cliente) { this.cliente = d.cliente };
           this.saldo = this.saldo + d.saldo;
       })

       console.dir(this.deudas);
    }

    public close() {
        this._params.closeCallback();
    }

}
