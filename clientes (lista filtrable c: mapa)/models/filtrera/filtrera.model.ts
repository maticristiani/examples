export interface Filtrera {
	activo: boolean, 
	filtros: Array<any>, 
	orden: string
}