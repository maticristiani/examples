export interface Cliente {
    id: number,
    codigo_erp: any,
    nombre: string,
    direccion: string,
    localidad: string,
    provincia: string,
    pais: string,
    email: string,
    telefono: string,
    zona_id: number,
    precio_lista: number,
    vendedor_erp: any,
    recibos_abiertos: number,
    recibos_cerrados: number,
    pedidos_abiertos: number,
    pedidos_cerrados: number,
    tipo_id: any
}