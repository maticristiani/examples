import { Component, Input, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

/* ***********************************************************
* Keep data that is displayed as drawer items in the MyDrawer component class.
*************************************************************/
@Component({
    selector: "TablaItemComponent",
    moduleId: module.id,
    templateUrl: "./tabla.component.html"
})
export class TablaItemComponent implements OnInit {
    
    @Input() origen: string;
    @Input() emision: string;
    @Input() descripcion: string;
    @Input() nro_doc: string;
    
    @Input() debe: number;
    @Input() haber: number;
    @Input() saldo: number;
    
    constructor(private routerExtensions: RouterExtensions) {

    }

    ngOnInit(): void {
        /* ***********************************************************
        * Use the MyDrawerItemComponent "onInit" event handler to initialize the properties data values.
        *************************************************************/
    }

    /* ***********************************************************
    * Use the "tap" event handler of the GridLayout component for handling navigation item taps.
    * The "tap" event handler of the app drawer item <GridLayout> is used to navigate the app
    * based on the tapped navigationItem's route.
    *************************************************************/
    onNavItemTap(navItemRoute: string): void {
       
    }
}
