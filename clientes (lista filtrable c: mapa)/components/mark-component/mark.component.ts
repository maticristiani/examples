import { Component, Input, OnInit, Output, EventEmitter } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";

@Component({
    selector: "MarkComponent",
    moduleId: module.id,
    templateUrl: "./mark.component.html"
})
export class MarkComponent implements OnInit {
    @Input() cliente: any;
    @Input() color: string;

    @Output() openCliente = new EventEmitter();
    @Output() openDetalle = new EventEmitter();
    @Output() serchOnMap = new EventEmitter();

    direccion: string
    
    constructor(private routerExtensions: RouterExtensions) {
        
    }

    ngOnInit(): void {
       
        this.direccion = 'Este cliente no posee una dirección.';
            
        if (this.cliente.direccion && this.cliente.direccion.length > 0) {

            this.direccion = this.cliente.direccion;
            this.direccion += (this.cliente.localidad && this.cliente.localidad.length > 0) ? (', '+ this.cliente.localidad) : '';
            this.direccion += (this.cliente.provincia && this.cliente.provincia.length > 0) ? (', '+ this.cliente.provincia) : '';
            this.direccion += (this.cliente.pais && this.cliente.pais.length > 0) ? (', '+ this.cliente.pais+'.') : '';
        }

        console.log(this.cliente.direccion);
    }

    tapping(client): void {
        this.serchOnMap.emit(client);
    }

    pressing(client): void {
        this.openCliente.emit(client);
    }

    detalle(client): void {
        this.openDetalle.emit(client);
    }
}