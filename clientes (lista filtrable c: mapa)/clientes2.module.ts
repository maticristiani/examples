import { NgModule, NO_ERRORS_SCHEMA } from "@angular/core";
import { NativeScriptCommonModule } from "nativescript-angular/common";

import { NativeScriptUIListViewModule } from "nativescript-ui-listview/angular";
import { NativeScriptUIDataFormModule } from "nativescript-ui-dataform/angular";
import { Clientes2RoutingModule } from "./clientes2-routing.module";
import { Clientes2Component } from "./clientes2.component";


import { modalDetalle } from "./modals/cliente_detalle/cliente-detalle";
import { modalFiltrador } from "./modals/cliente_filtrador/cliente-filtrador";
import { modalNuevoDos } from "./modals/cliente_nuevo/cliente-nuevo";
import { modalCtaCte } from "./modals/cliente_ctacte/cliente-ctacte";

import { NoMarkComponent } from "./components/nomark-component/nomark.component";
import { MarkComponent } from "./components/mark-component/mark.component";
import { TablaItemComponent } from "./components/tabla-component/tabla.component";

@NgModule({
    imports: [
        NativeScriptCommonModule,
        NativeScriptUIDataFormModule,
        NativeScriptUIListViewModule,
        Clientes2RoutingModule
    ],
    entryComponents: [modalDetalle, modalNuevoDos, modalFiltrador, modalCtaCte],
    declarations: [
        Clientes2Component,
        TablaItemComponent,
        NoMarkComponent,
        MarkComponent,
        modalDetalle, 
        modalNuevoDos, 
        modalFiltrador, 
        modalCtaCte
    ],
    schemas: [NO_ERRORS_SCHEMA]
})
export class Clientes2Module {}