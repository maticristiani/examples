/* angular */
import { ListViewLinearLayout, ListViewGridLayout, ListViewStaggeredLayout, LoadOnDemandListViewEventData, ListViewEventData, RadListView, ListViewLoadOnDemandMode  } from "nativescript-ui-listview";
import { Component, OnInit, DoCheck, OnDestroy, ViewChild, ViewContainerRef, ElementRef, AfterContentInit, NgZone, ChangeDetectionStrategy, ChangeDetectorRef } from "@angular/core";
import { ModalDialogService, ModalDialogOptions } from "nativescript-angular/modal-dialog";
import { concat } from 'rxjs';

/*radlistview*/
import { RadListViewComponent } from "nativescript-ui-listview/angular";

/*service*/
import { DatabaseService } from ".././providers/database/database";
import { GPService } from ".././providers/geolocation/geolocation";
import { WorkerService } from "../worker.service";

/* models */
import { Zona } from "./models/zona/zona.model";
import { Titulo } from "./models/titulo/titulo.model";
import { Cliente } from "./models/cliente/cliente.model";
import { Filtrera } from "./models/filtrera/filtrera.model";

/*components*/
import { getNumber, getString, setNumber, getBoolean } from "tns-core-modules/application-settings";
import { ObservableArray } from "tns-core-modules/data/observable-array";
import * as utilityModule from "tns-core-modules/utils/utils";
import { confirm, action, alert } from "tns-core-modules/ui/dialogs";
import { SearchBar } from "tns-core-modules/ui/search-bar";
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform"

import { ActivityIndicator } from "tns-core-modules/ui/activity-indicator";

/*routes*/
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";

/*radsidedrawer*/
import { RadSideDrawerComponent } from "nativescript-ui-sidedrawer/angular";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";

// mapsbox
import { MapboxView, MapboxApi, Mapbox, MapStyle, OfflineRegion, LatLng, Viewport, DownloadProgress, DownloadOfflineRegionOptions,  } from "nativescript-mapbox";
import { MapboxViewApi, Viewport as MapboxViewport, MapboxMarker } from "nativescript-mapbox";
import { AddressOptions, Directions, NavigateToOptionsType } from "nativescript-directions";
import { PropertyChangeData } from "tns-core-modules/data/observable";

/* declare */
const sqlite = require("nativescript-sqlite");
const application = require("tns-core-modules/application");

/* modals */
import { modalDetalle } from "./modals/cliente_detalle/cliente-detalle";
import { modalNuevoDos } from "./modals/cliente_nuevo/cliente-nuevo";
import { modalCtaCte } from "./modals/cliente_ctacte/cliente-ctacte";
import { modalFiltrador } from "./modals/cliente_filtrador/cliente-filtrador";

declare var Color;
declare var InputType;
declare var UITableViewCellSeparatorStyle;
declare var UIColor;
declare var UIApplication;
declare var UISearchBarStyle;
declare var UIImage;
declare var android;
declare var NSURL;
declare var UIBarButtonItem;
declare var UIActivityIndicatorViewStyleWhiteLarge;

@Component({
    selector: "Clientes2",
    moduleId: module.id,
    providers: [ModalDialogService],
    templateUrl: "./clientes2.component.html"
})
export class Clientes2Component implements OnDestroy, OnInit, AfterContentInit {
    
    @ViewChild("radListClients", {static: false}) listViewComponent: RadListViewComponent;

    private _templateSelector: (item: Cliente, index: number, items: any) => string;
    
    private _dataItems: ObservableArray<Cliente>;
    private _dataLoad: Array<Cliente>;
    private _dataSearch: Array<Cliente>;

    public titulo: Titulo;
    public filtrera: Filtrera;
    
    public position: number;
    public activity: boolean;
    public openModal: boolean;
    public agregarRendicion: boolean;
    public searchPhrase: string;
    public agregarCliente: boolean;
    private dbWorker;

    public mapConfig: any;
    public nativeMapView: MapboxViewApi;
    
    public hiddenMap: boolean;
    public following: boolean;
    public multipleFlags: boolean;

    public routes: Array<AddressOptions>;
    private directions: Directions;
    private markers: Array<any>;
    
    renderView = false;
    renderViewTimeout: any;

    constructor(
        private ngZone: NgZone,
        private _gpsservice: GPService,
        private vcRef: ViewContainerRef,
        private serviceDB: DatabaseService,
        private workerService: WorkerService,
        private activatedRoute: ActivatedRoute,
        private modalService: ModalDialogService,
        private routerExtensions: RouterExtensions
    ) {

        this.searchPhrase = "";

        this.agregarRendicion = getBoolean('rendicion')  ? getBoolean('rendicion') : false;
        this.agregarCliente = getBoolean('agrega_cliente')  ? getBoolean('agrega_cliente') : false;
        
        this.hiddenMap = ( getString("ocultar_mapa") && getString("ocultar_mapa") == 'SI' ) ? true : false;
        this.following = false;
        this.openModal = false;
        this.activity = true;

        this.multipleFlags = false;

        this._dataLoad = new Array<Cliente>();
        this._dataSearch = new Array<Cliente>();
        this._dataItems = new ObservableArray<Cliente>();
        this._templateSelector = this.templateSelectorFunction;
        this.dbWorker = this.workerService.initDatabaseWorker();

        this.titulo = { linea_1: "MIS CLIENTES ", linea_2: "CANTIDAD: ", linea_3: "" };
        this.filtrera = { activo: false, filtros: [], orden: '', };
        
        this.position = getNumber("position") ? getNumber("position") : 0;
        
       
        this.directions = new Directions();
        this.routes = [];
        this.markers = [];
        
        this.mapConfig = {
            accessToken: 'sk.eyJ1Ijoid2VibWVlbWJhIiwiYSI6ImNqdzJhMnlwMjAxM3A0NGw5bndzdngwNnIifQ.e1_wg1tA9qfAtgJSwK6plA', // see 'Prerequisites' above
            style: 'TRAFFIC_DAY', // see the mapbox.MapStyle enum for other options, default mapbox.MapStyle.STREETS
            delay: 400,
            lat: -34.603628,
            long: 58.381528,
            zoomLevel: 12,
            userlocation: true,
            hideLogo: false,
            hideAttribution: false,
            hideCompass: false,
            disableZoom: false,
            disableRotation: false,
            disableScroll: false,
            disableTilt: false,
          };
    }
 
    get dataItems(): ObservableArray<Cliente> {
        return this._dataItems;
    }
    
    get templateSelector(): (
        item: Cliente,
        index: number,
        items: any
    ) => string {
        return this._templateSelector;
    }

    set templateSelector(
        value: (item: Cliente, index: number, items: any) => string
    ) {
        this._templateSelector = value;
    }

    public templateSelectorFunction = (
        item: Cliente,
        index: number,
        items: any
    ) => {
        return "mark"
    };

    ngOnInit() {
        console.dir("ngOnInit");
        this.directions = new Directions();
        this.routes = [];
        this.markers = [];

    }

    
    ngAfterContentInit() {
        this.renderViewTimeout = setTimeout(() => {
            this.renderView = true;
            console.dir("ngAfterContentInit");
            console.dir("OCULTAR MAPA", this.hiddenMap)
            
            if (this.hiddenMap) {
                let zona_preselect = getNumber("zona_asignada") ? getNumber("zona_asignada") : 0;
                let q_inicial = " GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC "
                    
                if (zona_preselect > 0) {
                    this.titulo.linea_2 = 'FILTRO: ZONA('+zona_preselect+')';
                    q_inicial = " AND 3=3 AND c.zona_id="+zona_preselect+" GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC ";
                }

                this.inicializarClientes(false, q_inicial)
            }

        }, 600);
    }

    ngOnDestroy() {
        // here is where i update my UI with the response from the worker, like set my Observable Array to the sectionedShifts returned from the worker.
        clearTimeout(this.renderViewTimeout);
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    private setPositionOnList(pos): void {
        if (pos && pos > 0 ) {
            if (isAndroid) {
                try {
                    this.listViewComponent.listView.androidListView.smoothScrollToPosition(
                        pos,
                        0
                    );
                } catch {
                    console.log("off-scroll");
                }
            }
        }
    }

    public limpiarBusqueda(){

       let zona_preselect = getNumber("zona_asignada") ? getNumber("zona_asignada") : 0;
       let q_inicial = " GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC "
            
       if (zona_preselect > 0) {
           q_inicial = " AND 3=3 AND c.zona_id="+zona_preselect+" GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC ";
       }
            
      if (this.searchPhrase.length > 0) {
          this.titulo.linea_2 =  'CANTIDAD: ';
          this.titulo.linea_3 = (zona_preselect>0) ? 'ZONA: '+zona_preselect : '-';
          this.searchPhrase = '';
          
          //restauro-clientes
          this.inicializarClientes(false, q_inicial);
      };

      if (this.filtrera.activo) { 

            this.filtrera.activo = false;
            this.filtrera.filtros = [];
            this.filtrera.orden = '';
            
            //restauro-title
            this.titulo.linea_2 =  'CANTIDAD: ';
            this.titulo.linea_3  = (zona_preselect>0) ? 'ZONA: '+zona_preselect : '-';
            

            //restauro-clientes
            this.inicializarClientes(false, q_inicial);
      }

    }


    private inicializarClientes(filtro, query_filter): void {
        this.activity = true;

        this.markers = new Array<any>();
        this._dataLoad = new Array<Cliente>();
        this._dataSearch = new Array<Cliente>();
        this._dataItems = new ObservableArray<Cliente>();    

        /* Asociado_erp representa el codigo_erp correpsondiente a las tablas mencionadas anteriormente vendedores/clientes. */
        let supervisor = getNumber("supervisor");
        let params = [];
        
        let q_c = "SELECT c.id, c.web_id, c.codigo_erp, c.nombre, c.orden, c.telefono, c.email, ";
            q_c += " cd.lat AS dm_lat, cd.lng AS dm_long , cd.nombre AS dm_nombre, cd.principal AS dm_principal, ";
            q_c += " IFNULL(cd.calle,'') AS dm_calle,  IFNULL(cd.altura,'') AS dm_altura,  IFNULL(cd.localidad,'') AS dm_localidad,  IFNULL(cd.provincia,'') AS dm_provincia,  IFNULL(cd.pais,'') AS dm_pais,";
            q_c += ' IFNULL(c.direccion,"") AS direccion, IFNULL(c.localidad,"") AS localidad, IFNULL(c.provincia,"") AS provincia,  IFNULL(c.pais,"") AS pais, ';
            q_c += " c.zona_id, c.precio_lista, c.vendedor_erp, IFNULL(COUNT(r.id),0) AS recibos_abiertos, IFNULL(COUNT(rr.id),0) AS recibos_cerrados, IFNULL(COUNT(p.id),0) AS pedidos_abiertos, IFNULL(COUNT(pr.id),0) AS pedidos_cerrados, ";
            q_c += " IFNULL(c.tipo_id,0) AS tipo_id FROM clientes AS c ";
            
            q_c += ' LEFT JOIN clientes_domicilios AS cd ON (c.id = cd.cliente_id ) ';
            q_c += ' LEFT JOIN pedidos AS p ON (c.codigo_erp = p.cliente_erp AND p.de_sistema = "N" AND p.enviado = 0 ) ';
            q_c += ' LEFT JOIN recibos AS r ON (r.cliente_id = c.id AND r.sincronizado = 0 ) ';
            q_c += ' LEFT JOIN recibos AS rr ON (rr.cliente_id = c.id AND rr.sincronizado = "1" ) ';
            
            q_c += ' LEFT JOIN pedidos AS pr ON (c.codigo_erp = pr.cliente_erp AND pr.de_sistema = "N"  AND pr.enviado != 0 ) ';
            q_c += ' LEFT JOIN pedidos AS prweb ON (c.web_id = prweb.cliente_web_id AND prweb.de_sistema = "N"  AND prweb.enviado != 0 ) ';
            
            q_c += ' WHERE 1=1 ';


        if (!supervisor) {
            let asociado_erp = getString("asociado_erp");
            let asociado_tabla = getString("asociado_tabla");
            if (asociado_tabla == "vendedores") {
                let vendedor_id = getNumber("asociado_id");
                q_c += " AND c.vendedor_id = ? ";
                params.push(vendedor_id);
            } else {
                q_c += " AND c.codigo_erp = ? ";
                params.push(asociado_erp);
            }
        };

        q_c += query_filter;
        
        this.dbWorker.postMessage({ type: 'all', query: q_c, params: params });
        const that = new WeakRef(this);

        this.dbWorker.onmessage = (res) => {
        that.get().ngZone.runOutsideAngular(()=>{
            
            let status = res.data.success;
            let rows = res.data.rows;
            
            console.dir("rows");
            console.dir(rows[0]);
            console.dir(rows[1]);

            if (!status) {
                that.get().activity = false;
                alert("Hubo un error al inicializar los clientes.");
            }
            
            if(filtro){
                that.get().titulo.linea_2 = (rows && rows.length) ? (rows.length+' CLIENTES. ') : ('SIN RESULTADOS');
                that.get().titulo.linea_3 =  (that.get().searchPhrase && that.get().searchPhrase.length==0) ? 'REESTABELCER BUSQUEDA' : 'REESTABELCER BUSQUEDA';
            }
            
            if (rows && rows.length > 0) { 
                    that.get()._dataSearch = rows;
                    that.get()._dataLoad = rows;

                    let numbers = getNumber("itemShows") ? getNumber("itemShows") : isAndroid ? 100 : 10;
                    if (numbers > rows.length) { numbers = rows.length };
                   
                    // markers on map
                    rows.forEach( (c, i) => {
                        
                        c.selected = false;

                        if (c.dm_long && c.dm_long > (-90) && c.dm_long < (90) ) {
                            if (c.dm_lat && c.dm_lat > (-90) && c.dm_lat < (90) ) {
                                
                                let str_dir = c.dm_calle +' '+c.dm_altura; 
                                    str_dir += (c.dm_localidad) ? (', '+ c.dm_localidad) : '';
                                    str_dir += (c.dm_provincia) ? (', '+ c.dm_provincia) : '';
                                    str_dir += (c.dm_pais) ? (', '+ c.dm_pais+'.') : '';

                                this.markers.push({
                                    id: c.id, // id de cliente
                                    lat: c.dm_lat, // lat
                                    lng: c.dm_long, // long
                                    title: (c.dm_nombre) ? (c.dm_nombre) : (c.dm_calle+' '+c.dm_altura),
                                    selected: false,
                                    subtitle: str_dir +' (Presiona aquí para viajar)',
                                    onCalloutTap: () => {
                                      
                                    if (this.multipleFlags) {
                                            this.searchClientMap(i, c)
                                     }else{
                                         this.showDirectionsTo([
                                            {
                                              lat: c.dm_lat, // lat
                                              lng: c.dm_long, // long
                                              address: str_dir
                                            }
                                          ]);
                                    }

                                    }
                                  })
                            }
                        }

                    })

                    that.get().ngZone.run(() => {
                        console.log('detect-changes')

                        /* Limpio marcadores en el mapa si hay para agregar nuevamente*/                            
                            if(that.get().markers && that.get().markers.length > 0){
                                
                                try{
                                    that.get().nativeMapView.removeMarkers();
                                }catch(e) {
                                    console.log('mapa-no-cargado;')
                                }

                                try{
                                    that.get().nativeMapView.addMarkers(that.get().markers).then((res)=>{
                                            setTimeout(()=>{
                                                that.get().centerOnMap();        
                                            }, 3000)
                                    })
                                }catch(e) {
                                    console.log('mapa-no-cargado;')
                                }
                            }
                        

                        that.get()._dataItems = new ObservableArray<Cliente>();
                        that.get().addMoreClientsFromSource(numbers);


                    })

            }else{
              that.get().activity = false;
              that.get().titulo.linea_2 = 'No se encontraron clientes con los parametros seleccionados.';
              that.get().titulo.linea_3 = 'DESHACER PARAMETROS ';
            }

           })
        };

        this.dbWorker.onerror = function(err) {  
          that.get().ngZone.run(()=>{
            console.log(`An unhandled error occurred in worker: ${err.filename}, line: ${err.lineno} :`);
            console.log(err.message);
            that.get().activity = false;
          })
        }

    }

    /* funciones del buscador */
    public searchBar_Loaded(args: any) {
        let searchBar: SearchBar = <SearchBar>args.object;
        searchBar.dismissSoftInput();
        searchBar.hint = "Buscar por nombre";

        if (isAndroid) {
            searchBar.android.clearFocus();
            // searchBar.android.setInputType(InputType.TYPE_CLASS_NUMBER)
        } else {
            searchBar.ios.endEditing(true);
            console.dir('searchBar');
            
            let nativeSearchBar = searchBar.nativeView;
            
                nativeSearchBar.tintColor = new UIColor({red: 76/255, green: 217/255, blue: 100/255, alpha: 1});
                nativeSearchBar.searchBarStyle = UISearchBarStyle.Prominent;
                nativeSearchBar.showsCancelButton = true;
                nativeSearchBar.showsSearchResultsButton = false;
                nativeSearchBar.showsScopeBar = false;
                nativeSearchBar.backgroundImage = UIImage.new();

        }

        searchBar.text = "";
        searchBar.hint = "Buscar por nombre";
    }

    public searchBar_onSubmit(args) {
        let searchBar = <SearchBar>args.object;
        let searchValue = searchBar.text.toUpperCase();

        if (this.activity) {
                return;
        }
        searchBar.dismissSoftInput();
        
        let query = " AND  1 = 1 AND c.codigo_erp LIKE '%"+searchValue+"%' OR  c.nombre LIKE '%"+searchValue+"%' ";  
            query += " GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC ";

        this.inicializarClientes(true, query)
    }

    public searchBar_onTextChange(args) {
        let limitTime = getNumber('limitSearchTime') ? getNumber('limitSearchTime')  : 600;
        
        setTimeout(() => {
            let limitSearchPhrase = getNumber('limitSearch') ? getNumber('limitSearch')  : 4;

            if (this.activity) {
                return;
            }

            let searchBar = <SearchBar>args.object;
            let searchValue = searchBar.text.toUpperCase();
            if (
                (searchValue !== null && searchValue.length > limitSearchPhrase)
            ) {
               let query = " AND  1 = 1 AND c.codigo_erp LIKE '%"+searchValue+"%' OR  c.nombre LIKE '%"+searchValue+"%' ";  
                   query += " GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC ";
               
               this.inicializarClientes(true, query)
            }
        }, limitTime);
    }

    public searchBar_onClear(args) {
            
            if (this.activity) {
                return;
            }

            let searchBar = <SearchBar>args.object;
            let searchValue = searchBar.text.toUpperCase();
            
            this.searchPhrase = '';
            this.inicializarClientes(false, ' GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC ')
       
        try {
                searchBar.dismissSoftInput();
        } catch {
                console.log("error catch: dismissSoftInput");
        }   
    }

    public openDetalle(posicion, cliente){

       let options = {
            message: 'Cliente: '+cliente.nombre+' [ '+cliente.codigo_erp+' ] :',
            cancelButtonText: 'Volver',
            actions: [
                'Llamar al cliente',
                'Enviar un email',
                'Ver direcciones',
                'Ver estado de cuenta',
                'Más información',
                'Ir a pedidos',
                'Ir a cobranzas',
                'Cargar motivo de no compra',
                'Marcar cliente como visitado', 
            ]};

        action(options).then((res)=>{
                switch (res) {
                    case 'Llamar al cliente':
                        this.emitCall(cliente);
                        break;
                    
                    case 'Enviar un email':
                        this.emitEmail(cliente);
                        break;

                    case 'Ver direcciones':
                        this.emitPopupDirecciones(cliente);
                        break;

                    case 'Ver estado de cuenta':
                        this.emitEstadoCuenta(cliente);
                        break;
                    
                    case 'Más información':
                        this.emitPopOverInfo(cliente);
                        break;

                    case 'Ir a pedidos':
                        this.emitPedido(posicion, cliente);
                        break;

                    case 'Ir a cobranzas':
                        this.emitCobranza(cliente);
                        break;
                        
                    case 'Cargar motivo de no compra':
                        this.emitNoCompra(cliente);
                        break;
                        
                    case 'Marcar cliente como visitado':
                        this.emitVisita(cliente);
                        break;
                }
        })         

    }
    
    private insertPCabecera(query, params){
     return new Promise((resolve, reject) => {                           
         this.serviceDB.query(query,params).then((res)=>{
              resolve(res)
          }).catch((err)=>{
              resolve('ERROR')
          })
     })
    }  

    private insertPDetalle(id,p,datos){
        return new Promise((resolve, reject) => {                              
            let query = "INSERT INTO pedidos_detalle (pedido_id, codigo_erp, nombre, precio_unitario, unidad_venta, cantidad, descuento, iva, subtotal, de_sistema) VALUES (?,?,?, ?,?,?, ?,?,? ,? )";
            
            let subtotal = (p.precio * datos.cantidad)-(p.precio * datos.cantidad*datos.descuento/100)
            
            let params = [ 
                id,
                p.codigo_erp,
                p.nombre,
                p.precio,
                p.unidad_venta,
                datos.cantidad,
                datos.descuento,
                p.iva,
                subtotal,
                p.de_sistema
                ];

             
             
             this.serviceDB.query(query, params).then((res)=>{
                  resolve('OK')
              }).catch((err)=>{
                  resolve('ERROR')
              })
        })
    }  

    private emitVisita(cliente) { 
        
        let query: string =  " SELECT p.* FROM Productos AS p  WHERE p.codigo_erp IS NOT NULL AND p.de_sistema = 'S' ;";    
        this.dbWorker.postMessage({ type: 'all', query: query, params: [] });
        
        const that = new WeakRef(this);
        
        this.dbWorker.onmessage = (res) => {
           that.get().ngZone.runOutsideAngular(()=>{
             let status = res.data.success;
             let p_visita = res.data.rows;
             let opciones = [];

             p_visita.forEach(p => { opciones.push(p.nombre) })


            action("Seleccionar motivo", "CANCELAR", opciones).then(res => {  
                if (res) {

                    if (res.toUpperCase() == 'CANCELAR') {
                       return;
                    }

                    //asignar-no-compra
                    p_visita.forEach(p =>{
                      
                      if (p.nombre.toUpperCase().trim() == res.toUpperCase().trim()) {
                        let result = { cantidad: 1 , descuento: 0 };
                        p.precio = 0;
                           
                        let query = "INSERT INTO pedidos (dispositivo, cliente_id, cliente_erp, cliente_web_id, usuario_email, vendedor_erp, vendedor_email, descuento, observaciones, web_id, estado, total, enviado, fecha, transporte_manual_erp, forma_pago_erp, transporte_manual, forma_pago,  de_sistema, de_sistema_tipo, fecha_entrega) VALUES (?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?,? ,?,? ,? )";            
                        let fecha = new Date();
                        
                        let params = [ 
                            device.uuid,
                            cliente.id ? cliente.id : cliente.id,
                            cliente.erp ? cliente.erp : cliente.codigo_erp, 
                            cliente.web_id,
                            getString('email') ? getString('email') : '',
                            getString('vendedor_erp') ? getString('vendedor_erp') : '',
                            '-',
                            0,
                            '[ MOTIVO VISITA ]',
                            0,
                            'PENDIENTE',
                            0,
                            0,
                            fecha,
                            '',
                            '',
                            '',
                            '',
                            'N',
                            '-',
                            ''
                            ];

                        this.insertPCabecera(query,params).then((res : any)=>{
                            this.insertPDetalle(res, p,result).then((res)=>{

                            })
                        })
                       
                      }
                    })
                }
            });



           })
        }
    }

    private emitNoCompra(cliente) {
        
        let query: string =  " SELECT p.* FROM Productos AS p  WHERE p.codigo_erp IS NOT NULL AND p.de_sistema = 'S' ;";    
        this.dbWorker.postMessage({ type: 'all', query: query, params: [] });
        
        const that = new WeakRef(this);
        
        this.dbWorker.onmessage = (res) => {
           that.get().ngZone.runOutsideAngular(()=>{
             let status = res.data.success;
             let p_nocompra = res.data.rows;
             let opciones = [];

             p_nocompra.forEach(p => { opciones.push(p.nombre) })


            action("Seleccionar motivo", "CANCELAR", opciones).then(res => {  
                if (res) {

                    if (res.toUpperCase() == 'CANCELAR') {
                       return;
                    }

                    //asignar-no-compra
                    p_nocompra.forEach(p =>{
                      
                      if (p.nombre.toUpperCase().trim() == res.toUpperCase().trim()) {
                        let result = { cantidad: 1 , descuento: 0 };
                        p.precio = 0;
                           
                        let query = "INSERT INTO pedidos (dispositivo, cliente_id, cliente_erp, cliente_web_id, usuario_email, vendedor_erp, vendedor_email, descuento, observaciones, web_id, estado, total, enviado, fecha, transporte_manual_erp, forma_pago_erp, transporte_manual, forma_pago,  de_sistema, de_sistema_tipo, fecha_entrega) VALUES (?,?,?,?,?,?, ?,?,?,?,?,?, ?,?,?,?,?,? ,?,? ,? )";            
                        let fecha = new Date();
                        
                        let params = [ 
                            device.uuid,
                            cliente.id ? cliente.id : cliente.id,
                            cliente.erp ? cliente.erp : cliente.codigo_erp, 
                            cliente.web_id,
                            getString('email') ? getString('email') : '',
                            getString('vendedor_erp') ? getString('vendedor_erp') : '',
                            '-',
                            0,
                            '[ MOTIVO NO COMPRA ]',
                            0,
                            'PENDIENTE',
                            0,
                            0,
                            fecha,
                            '',
                            '',
                            '',
                            '',
                            'N',
                            '-',
                            ''
                            ];

                        this.insertPCabecera(query,params).then((res : any)=>{
                            this.insertPDetalle(res, p,result).then((res)=>{

                            })
                        })
                       
                      }
                    })
                }
            });



           })
        }
    }

    private emitCobranza(cliente) {
        
        if (!cliente.codigo_erp) {
            return alert("No puede generar una cobranza en un cliente sin un codigo erp asignado.");
        }

        this.routerExtensions.navigate(["/recibos",cliente.id,cliente.codigo_erp,cliente.nombre],{ clearHistory: false });

    }

    private emitPedido(index, cliente) {
        this.position = index;

        if (this.searchPhrase.length == 0) {
            console.log("sin-busqueda seteo sig.");
            setNumber("position", index);
        }

        //save-erp
        if (!cliente.codigo_erp) {
            cliente.codigo_erp = "sin-erp-"+cliente.id;
        }

        this.routerExtensions.navigate(
            [
                "/pedidos",
                cliente.id,
                cliente.codigo_erp,
                cliente.nombre,
                cliente.precio_lista,
                cliente.tipo_id
            ],
            { clearHistory: false }
        );
    }

    private emitPopOverInfo(cliente) {
        let qc =  ' SELECT c.*,  ';
            qc +=  ' v.nombre AS vendedor_nombre, z.nombre AS zona_nombre, ci.nombre AS iva_nombre, ';
            qc +=  ' t.nombre AS transporte_nombre , p.nombre AS pago_nombre ';
            qc +=  ' FROM clientes  AS c '
            qc +=  ' LEFT JOIN condiciones_iva AS ci ON(ci.id = c.iva) '
            qc +=  ' LEFT JOIN vendedores AS v ON(v.id = c.vendedor_id) '
            qc +=  ' LEFT JOIN zonas AS z ON(z.id = c.zona_id) '
            qc +=  ' LEFT JOIN transportes AS t ON(t.id = c.transporte) '
            qc += ' LEFT JOIN pagos AS p ON(p.id = c.forma_de_pago) ';

        if (cliente.codigo_erp && cliente.codigo_erp.length > 1) {
            qc +=  ' WHERE ';
            qc += ' c.codigo_erp = "' + cliente.codigo_erp + '" ';
        } else {
            qc += ' WHERE ';
            qc += ' c.id = "' + cliente.id + '" ';
        }

        this.serviceDB.get(qc).then(cliente => { 
            this.createModel(cliente, true, "detalle").then(result => {  this.openModal = false })
        })
    }

    private emitEstadoCuenta(cliente) {
        let params = [ cliente.id ];
        let items = [];
        
        let query_deuda = "SELECT ec.*, strftime('%d/%m/%Y', ec.fecha_emision) as 'emision_string' FROM estado_cuenta AS ec WHERE cliente_id = ?";
         
        this.serviceDB.all(query_deuda, params).then((rows: any) => {

            rows.forEach(r => {
                r.fecha_emision = r.emision_string;
                r.cliente = cliente.nombre;
                items.push(r);
            })

            let query_recibos = " SELECT r.*, strftime('%d/%m/%Y', 'now') AS 'emision_string', c.nombre AS cliente_nombre, c.codigo_erp AS cliente_erp_, IFNULL(SUM(rec_i.importe),0) AS total_recibo, COUNT(rec_i.id) AS cantidad_item";
              query_recibos += " FROM recibos AS r ";
              query_recibos += " LEFT JOIN clientes AS c ON (r.cliente_id = c.id ) "
              query_recibos += " LEFT JOIN recibos_items AS rec_i ON (r.id = rec_i.recibo_id) ";
              query_recibos += " WHERE cliente_id = ?";
              query_recibos += " GROUP BY r.id ";

            this.serviceDB.all(query_recibos, params).then((rows: any) => {

                rows.forEach(r => {
                    //datos
                    r.origen = 'BEEORDER';
                    r.tipo_doc = r.tipo;
                    r.clase_doc = 'Doc. Beeorder';
                    r.nro_doc =  r.nro;
                    r.cliente = cliente.nombre;
                    r.debe = 0;
                    r.saldo = r.haber = Number(r.total_recibo).toFixed(2);
                    r.fecha_emision = r.emision_string;

                    items.push(r);
                });

                if (items && items.length > 0) {
                    console.dir(items);
                    this.createModel(items, true, 'estadocuenta').then(res => {
                        this.openModal = false;
                    });

                } else {
                    alert("El cliente no posee un estado de cuenta valido y/o no pudimos obtener su información correctamente.");
                }
            });

        });
    }

    private emitPopupDirecciones(cliente) {
        let direcciones = [];

        this.markers.forEach((m)=>{
            if (m.id == cliente.id) {
                m.subtitle = m.subtitle.replace('(Presiona aquí para viajar)','');
                direcciones.push(m.subtitle);
            }
        })

        action({
            title: 'DIRECCIONES',
            message: 'Al parecer el cliente posee mas de una sucursal asiganda: ',
            cancelButtonText: "Cancelar",
            actions: direcciones
            }).then(result => {
                if (result && result != 'Cancelar') {
                    this.showDirectionsTo([ { address: result } ]);
                }
            })
    }

    private emitEmail(cliente) {
        try{
            let q = "mailto:" + cliente.email;
            utilityModule.openUrl(q);
        }catch(e){
            console.log('imposible enviar email:',cliente.email);
        }
    }

    private emitCall(cliente) {
        try{
            let q = "tel://+" + cliente.telefono;
            utilityModule.openUrl(q);
        }catch(e){
            console.log('imposible llamar al cliente.telefono:',cliente.telefono);
        }
    }

    public onItemSelected(index, c){
        c.selected = true;

        let str_dir = c.dm_calle +' '+c.dm_altura; 
            str_dir += (c.dm_localidad) ? (', '+ c.dm_localidad) : '';
            str_dir += (c.dm_provincia) ? (', '+ c.dm_provincia) : '';
            str_dir += (c.dm_pais) ? (', '+ c.dm_pais+'.') : '';

        let direccion: AddressOptions = { 
            lat: c.dm_lat,
            lng: c.dm_long,
            address:  str_dir 
        }
        
        this.routes.push(direccion)
        this.titulo.linea_3 = 'Direcciones: ' +this.routes.length;
    }

    public onItemDeselected(index, c){
       c.selected = false;
       this.routes.splice(index, 1);
       this.titulo.linea_3 = 'Direcciones: ' +this.routes.length;
    }

    /* funciones en map */
    public onMapReady(args){
        this.nativeMapView = args.map;

        this.nativeMapView.trackUser({
            mode: "FOLLOW_WITH_HEADING", // "NONE" | "FOLLOW" | "FOLLOW_WITH_HEADING" | "FOLLOW_WITH_COURSE"
            animated: false
        });

        let zona_preselect = getNumber("zona_asignada") ? getNumber("zona_asignada") : 0;
        let q_inicial = " GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC "
            
        if (zona_preselect > 0) {
            this.titulo.linea_2 = 'FILTRO: ZONA('+zona_preselect+')';
            q_inicial = " AND 3=3 AND c.zona_id="+zona_preselect+" GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC ";
        }
        console.dir("onMapReady");

        this.inicializarClientes(false, q_inicial)

    }
    
    public colorMap() {        
        
        if(this.mapConfig.style == 'TRAFFIC_DAY'){
           this.nativeMapView.setMapStyle(MapStyle.TRAFFIC_NIGHT);
           this.mapConfig.style = 'TRAFFIC_NIGHT';
           return;
        }

        this.nativeMapView.setMapStyle(MapStyle.TRAFFIC_DAY);
        this.mapConfig.style = 'TRAFFIC_DAY';
    }

    public alterMap() {        
        console.dir("ALTER-MAP")
        if (this.hiddenMap) {
            this.hiddenMap = false;
            return;
        }

        this.hiddenMap = true;
    }

    public centerOnMap() {
        try{
            this.nativeMapView.getUserLocation().then((userLocation) => { 
                               this.nativeMapView.setCenter({
                                lat: (userLocation.location.lat) ? userLocation.location.lat : Number(this._gpsservice.latitude), // mandatory
                                lng: (userLocation.location.lng) ? userLocation.location.lng : Number(this._gpsservice.longitude), // mandatory
                                animated: false // default true
                              })     
            }).catch((e)=>{
                console.dir(e)
                console.dir('UPS: @centerOnMap')
                this.nativeMapView.setCenter({
                                lat: Number(this._gpsservice.latitude), // mandatory
                                lng: Number(this._gpsservice.longitude), // mandatory
                                animated: false // default true
                })   
            })
        }catch(e){
            console.dir('@centerOnMap: Imposible centrar ubicación')
        }
    }

    public flagMarkers() {
        
        if (!this.multipleFlags) {
            this.multipleFlags = true;
            console.dir(this.multipleFlags);
            return
        }

        this.multipleFlags = false;
        this.routes = [];

        let zona_preselect = getNumber("zona_asignada") ? getNumber("zona_asignada") : 0;
        let q_inicial = " GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC "
        
        if (zona_preselect > 0) {
            this.titulo.linea_2 = 'FILTRO: ZONA('+zona_preselect+')';
            q_inicial = " AND 3=3 AND c.zona_id="+zona_preselect+" GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC ";
        }

        this.titulo.linea_3 = (zona_preselect>0) ? 'ZONA: '+zona_preselect : '-';
        this.titulo.linea_2 =  'CANTIDAD: ';
        
        this.filtrera.activo = false;
        this.filtrera.filtros = [];
        this.filtrera.orden = '';

        this.inicializarClientes(false, q_inicial)

    }

    public zoomOnMap(sum) {
      this.nativeMapView.getZoomLevel().then( (result) => {
        console.dir(result);
        let zoom = (sum) ? (result = result + 1) : (result = result - 1);
        
        this.nativeMapView.setZoomLevel({
            level: (zoom > 0) ? zoom : 0, // mandatory, 0-20
            animated: false // default true
          })

      }, (error) => {
        console.log("mapbox getZoomLevel error: " + error);
      })
    }

    public centerMeMap() {
     
     try{
         this.nativeMapView.getUserLocation().then(
              (userLocation) => {
                console.log("Current user location: " +  userLocation.location.lat + ", " + userLocation.location.lng);
                console.log("Current user speed: " +  userLocation.speed);
                  
               this.nativeMapView.setCenter({
                    lat: userLocation.location.lat,
                    lng: userLocation.location.lng,
                    animated: false // default true
                  })


              }
          )
     }catch(e){
         console.log("@centerMeMap");
     }

    };
    
    public doDownloadCurrentViewportAsOfflineRegion(mapbox: any): void {
        
        let options: any = {
            title: "DESCARGAR REGIÓN",
            message: "Desea descargar la region para utilizar cuando no posea una conexion a internet estable",
            okButtonText: "Si",
            cancelButtonText: "No",
            neutralButtonText: "Cancelar"
        };

        confirm(options).then((res: any) => {
            if (res) {
                /*HAY MAPA PREVIO*/
                
                mapbox.listOfflineRegions({accessToken: this.mapConfig.token}).then((regions)=>{
                    /*ELIMINAR MAPA*/
                    if (regions && regions.length > 0) {
                        mapbox.deleteOfflineRegion({name:"beeorder"}).then((res)=>{

                        })
                    }
                    /*DESCARGAR MAPA*/
                    mapbox.getViewport().then(
                        (viewport: Viewport) => {
                          mapbox.downloadOfflineRegion(
                              {
                                name: "beeorder",
                                style: MapStyle.OUTDOORS,
                                minZoom: viewport.zoomLevel,
                                maxZoom: viewport.zoomLevel + 2,
                                bounds: viewport.bounds,
                                onProgress: (progress: DownloadProgress) => {
                                    console.log(`Download progress: ${JSON.stringify(progress)}`);
                                }
                              }
                          ).then(() => {
                                let alertOptions = {
                                  title: "Viewport downloaded",
                                  message: `Downloaded viewport with bounds ${JSON.stringify(viewport.bounds)} at zoom levels ${viewport.zoomLevel} - ${(viewport.zoomLevel + 2)}`,
                                  okButtonText: "OK :)"
                                };
                                alert(alertOptions);
                              },
                              (error: string) => console.log("mapbox doDownloadCurrentViewportAsOfflineRegion error: " + error)
                          );
                        },(error: string) => {
                          let alertOptions = {
                            title: "Download error",
                            message: error,
                            okButtonText: "Got it"
                          };
                          alert(alertOptions);
                        });
                    /*DESCARGAR MAPA*/
                },(error: string) => {
                        console.log('ERROR listOfflineRegions');
                })
                /*HAY MAPA PREVIO*/
            }
        })
    }

    public toggleFollowing(args: PropertyChangeData): void {
        if (args.value !== null && args.value !== this.following) {
          this.following = args.value;
          // adding a timeout so the switch has time to animate properly
          setTimeout(() => {
            this.nativeMapView.trackUser({
              mode: this.following ? "FOLLOW_WITH_COURSE" : "NONE",
              animated: false
            });
          }, 200);
        }
      }

    public searchClientMap(i, c): void {
        console.log('@searchClientMap');

        this.position = i;
        this.setPositionOnList(this.position); 

        if (getBoolean('abrir_pedido_oneclick') &&  !this.multipleFlags && !this.hiddenMap) {
            this.emitPedido(i, c);
        }

        //mapa-oculto
        if (this.hiddenMap) {
            this.emitPedido(i, c);
        }

        //multiple-flags
        if (this.multipleFlags) {
            console.log('flag');
            if (c.dm_lat && c.dm_long ) {
            
            if ( c.selected ) {
                return this.onItemDeselected(i, c);    
            }

            return this.onItemSelected(i, c);    
            }
        };

        if (!c.dm_lat || !c.dm_long ) {
            return;
        }

        //mapa-set-person
        this.nativeMapView.setCenter({
             lat: Number(c.dm_lat),
             lng: Number(c.dm_long),
             animated: false 
        }).then(()=>{
            setTimeout(()=>{
                this.nativeMapView.setZoomLevel({ level: 12, animated: false });
            },1000)
        })
    }

    public showDirectionsTo(addresses: Array<AddressOptions>): void {
        
        setTimeout(()=>{

        action({
            title: 'NAVEGAR HACIA ESTE PUNTO',
            message: 'Como desea dirigirse hacia su destino: ',
            cancelButtonText: "Cancelar",
            actions: [ 'AUTO', 'CAMINANDO', 'BICICLETA', 'TRANSPORTE' ]
        }).then(r => {
            //driving, transit, bicycling or walking
            let route: any = "";
            switch (r) {
                case "AUTO":
                    route = 'driving'
                break;
                case "CAMINANDO":
                    route = 'walking'
                break;
                case "BICICLETA":
                    route = 'bicycling'
                break;
                case "TRANSPORTE":
                    route = 'transit'
                break;
            }

            if (route && route.length > 0) {

                this.directions.navigate({
                      to: addresses,
                      ios: {
                        // Apple Maps can't show waypoints, so open Google maps if available in that case
                        preferGoogleMaps: true, 
                        allowGoogleMapsWeb: true
                      },
                      type: route,
                    }).then(() => {

                        let zona_preselect = getNumber("zona_asignada") ? getNumber("zona_asignada") : 0;
                        let q_inicial = " GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC "
                        
                        if (zona_preselect > 0) {
                            this.titulo.linea_2 = 'FILTRO: ZONA('+zona_preselect+')';
                            q_inicial = " AND 3=3 AND c.zona_id="+zona_preselect+" GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC ";
                        }
                        
                        this.inicializarClientes(false, q_inicial)

                    }).catch((error) =>{

                       console.dir(error)
                    });
            }

        })

        },1000)
    
    }

   

    public openViewFilter() {
        
        if(this.activity){
          return;
        }

        this.createModel(this.filtrera, true, 'filtro').then(res => {
        this.openModal = false;
       
          //where
          if (res && res != 'undefined') {
            
            let query_filtro = ' AND 1 = 1 ';
            let array_orden = [];
            let query_orden = '';

            res.filtros.forEach((f,index)=>{
              f.valores.forEach(v=>{

                if (v.activo && v.query && v.query.length > 4) {
                  
                  query_filtro +=  v.query;

                  if (v.orden && v.orden.length > 0) {
                      array_orden.push(v.orden);
                  }

                }else if(v.activo){
                  query_filtro +=  (v.id =='todos') ? (' AND 2 = 2 ') : (' AND c.filtro_'+f.id+' = '+v.id);
                }

              })
            })

          //group
          query_filtro += ' GROUP BY cd.id ';
          
          //orden_personalizado
          if (array_orden && array_orden.length > 0) {
            array_orden.forEach((orden,i)=>{
               if (i==0) {
                 query_orden += ' ORDER BY '+orden;
               }else{
                 query_orden += ' , '+orden;
               }
            })
          }

          //orden  
           if (res.orden && res.orden.length > 0) {
            
             switch (res.orden) {
                case "con-pedidos":
                  query_orden += (array_orden && array_orden.length==0) ? ' ORDER BY pedidos_abiertos DESC, cd.principal DESC, c.id ASC ' : ' , pedidos_abiertos DESC, cd.principal DESC, c.id ASC ';
                  break;
               
                case "orden":
                  query_orden += (array_orden && array_orden.length==0) ? ' ORDER BY c.orden ASC, cd.principal DESC, c.id ASC ': ' , c.orden ASC, cd.principal DESC, c.id ASC ';
                  break;
               
                case "abc":
                  query_orden += (array_orden && array_orden.length==0) ? ' ORDER BY c.nombre ASC, cd.principal DESC ': ' , c.nombre ASC, cd.principal DESC, c.id ASC ';
                  break;

                case "cba":
                  query_orden += (array_orden && array_orden.length==0) ? ' ORDER BY c.nombre DESC, cd.principal DESC, c.id ASC ': ' , c.nombre DESC, cd.principal DESC, c.id ASC ';
                  break;

                default:
                  break;
              }

            }
            
            console.dir('query-orden:', query_orden);

            if (query_orden.length == 0) {
                console.dir('query-orden-default');
                query_filtro += " order BY c.id ASC, c.orden ASC, cd.principal DESC ";
            }

            query_filtro = query_filtro + query_orden;
            
            console.dir('query-filtro: ',query_filtro)
            
            //limpiA-busqueda si estaba activa
            if (this.searchPhrase.length >0) {
              this.searchPhrase = '';
            }

            this.activity = true;
            
            this.filtrera.activo = true;
            this.filtrera.filtros = res.filtros;
            this.filtrera.orden = res.orden;

            this.titulo.linea_2 =  'APLICANDO PARAMETROS...';
            this.inicializarClientes(true, query_filtro);

          }
        
        })
    }

    private getZonas() {
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM zonas";
            let params = [];
            let pagos = [];
            this.serviceDB
                .all(query, params)
                .then((rows: any) => {
                    rows.forEach(row => {
                        pagos.push(row);
                    });
                    resolve(pagos);
                })
                .catch(res => {
                    reject("ERROR");
                });
        });
    }

    private getPagos() {
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM pagos";
            let params = [];
            let pagos = [];
            this.serviceDB
                .all(query, params)
                .then((rows: any) => {
                    rows.forEach(row => {
                        pagos.push(row);
                    });
                    resolve(pagos);
                })
                .catch(res => {
                    reject("ERROR");
                });
        });
    }

    private getCondicionesIva() {
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM condiciones_iva";
            let params = [];
            let condiciones = [];
            this.serviceDB
                .all(query, params)
                .then((rows: any) => {
                    rows.forEach(row => {
                        condiciones.push(row);
                    });
                    resolve(condiciones);
                })
                .catch(res => {
                    reject("ERROR");
                });
        });
    }

    private getTransportes() {
        return new Promise((resolve, reject) => {
            let query = "SELECT * FROM transportes";
            let params = [];
            let pagos = [];
            this.serviceDB
                .all(query, params)
                .then((rows: any) => {
                    rows.forEach(row => {
                        pagos.push(row);
                    });
                    resolve(pagos);
                })
                .catch(res => {
                    reject("ERROR");
                });
        });
    }

    public addClient() {
        let obj = { zonas: [], pagos: [], transportes: [], condiciones_iva: [] };

        this.getTransportes().then((rows_t: any) => {
            obj.transportes = rows_t;
            console.dir(rows_t[0]);

            this.getZonas().then((rows_z: any) => {
                obj.zonas = rows_z;
                console.dir(rows_z[0]);

                this.getPagos().then((rows_p: any) => {
                    obj.pagos = rows_p;

                    this.getCondicionesIva().then((rows_ci: any) => {
                    obj.condiciones_iva = rows_ci;
                    console.log("TODO OK");
                    
                    this.createModel(obj, true, "nuevo").then(res => {
                        this.openModal = false;
                        
                        if (res) {
                            this.activity = true;
                            this.setCliente(res);
                        }

                    });

                   });
                });
            });
        });
    }

    public setCliente(cliente) {
        console.dir("crear-cliente");
        console.dir(cliente);

        let query =" INSERT INTO clientes (web_id, codigo_erp, vendedor_erp, vendedor_id, zona_id, nombre, estado, direccion, pais, localidad, provincia, email, telefono, cuit, iva, precio_lista, observaciones, descuento, transporte, forma_de_pago, tipo_id ) VALUES (?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?,?,?,?,?, ?) ";
        let erp_temporal = "ERP_" + new Date().getTime();
        let param = [];

        param.push(0);
        param.push(erp_temporal);
        param.push(String(getString("vendedor_erp")));
        param.push(Number(getNumber("vendedor_id")));
        param.push(Number(cliente.zona));
        param.push(cliente.nombre);
        param.push("S");
        param.push((cliente.calle+' '+cliente.numero));
        let pais = cliente.pais ? cliente.pais: 'Argentina';
        param.push(pais);
        param.push(cliente.localidad);
        param.push(cliente.provincia);
        param.push(cliente.email);
        param.push(cliente.telefono);
        param.push(cliente.cuit);
        param.push(Number(cliente.cond_iva));
        param.push(Number(cliente.p_lista));
        param.push("SIN OBSERVACIONES");
        param.push(Number(cliente.descuento));
        param.push(Number(cliente.f_transporte));
        param.push(Number(cliente.f_pago));
        param.push(1);

        console.dir(query);
        console.dir(param);

        this.serviceDB
            .query(query, param)
            .then(res => {
                this.eventGPS("CLIENTE-NUEVO-" + res);

                let zona_preselect = getNumber("zona_asignada") ? getNumber("zona_asignada") : 0;
                let q_inicial = " GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC "
                
                if (zona_preselect > 0) {
                    this.titulo.linea_2 = 'FILTRO: ZONA('+zona_preselect+')';
                    q_inicial = " AND 3=3 AND c.zona_id="+zona_preselect+" GROUP BY cd.id order BY c.id ASC, c.orden ASC, cd.principal DESC ";
                }
                
                this.inicializarClientes(false, q_inicial)

            })
            .catch(err => {
                this.activity = false;
                console.dir(err);
                alert("HUBO UN ERROR AL INTENTAR CREAR EL CLIENTE.");
            });
    }

    public volverHome(): void {
        this.routerExtensions.navigate(["/home"], { clearHistory: true });
    }

    eventGPS(tipo): void {
        let asociado_erp = getString("asociado_erp");
        console.log(asociado_erp);
        if (!asociado_erp) {
            asociado_erp = "sin-erp-"+getString("asociado_id");
        }
        let email = getString("email");
        let lat = this._gpsservice.latitude;
        let long = this._gpsservice.longitude;
        let ts = this._gpsservice.timestamp;

        let query =
            " INSERT INTO gps (usuario_erp, usuario_email, tipo, lat, long, timestamp) VALUES (?,?,?,?,?,?) ";
        let params = [asociado_erp, email, tipo, lat, long, ts];

        this.serviceDB.query(query, params);
    }
    
    public onLoadMoreClientsRequested(args: LoadOnDemandListViewEventData) {
        const that = new WeakRef(this);
        const listView: RadListView = args.object;

        console.dir('that.get()._dataLoad:', that.get()._dataLoad.length);

        if (that.get()._dataLoad && that.get()._dataLoad.length > 0) {
                let numbers = getNumber("itemShows") ? getNumber("itemShows") : isAndroid ? 40 : 10;
                
                setTimeout(function () {
                    that.get().addMoreClientsFromSource(numbers);
                    listView.notifyLoadOnDemandFinished();
                }, 600);

                args.returnValue = true; 
        
        } else {
            args.returnValue = false;
            listView.notifyLoadOnDemandFinished(true);
        }
    }

    public addMoreClientsFromSource(chunkSize: number) {
        try{
            let newItems = this._dataLoad.splice(0, chunkSize);
            
            if (newItems && newItems.length > 0) {
                
                this._dataItems.push(newItems);
                this.setPositionOnList(this.position); 
                
                this.activity = false;

            }
            
            this.listViewComponent.listView.loadOnDemandBufferSize = chunkSize;
            this.listViewComponent.listView.loadOnDemandMode = ListViewLoadOnDemandMode.None;
            this.listViewComponent.listView.loadOnDemandMode = ListViewLoadOnDemandMode.Auto;

        
        }catch(e){
            console.dir(e);
            console.dir('ERROR - addMoreClientsFromSource')
        }
    }

    private createModel(item, fullscreen, type): Promise<any> {
        if (!this.openModal) {
            this.openModal = true;

            const options: ModalDialogOptions = {
                viewContainerRef: this.vcRef,
                context: item,
                fullscreen: fullscreen
            };

            if (type == "filtro") {
                return this.modalService.showModal(modalFiltrador, options);
            } else if (type == "detalle") {
                return this.modalService.showModal(modalDetalle, options);
            } else if (type == "nuevo") {
                return this.modalService.showModal(modalNuevoDos, options);
            } else if (type == "estadocuenta") {
                return this.modalService.showModal(modalCtaCte, options);
            }
        }
    }

}