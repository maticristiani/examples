import { NgModule } from "@angular/core";
import { Routes } from "@angular/router";
import { NativeScriptRouterModule } from "nativescript-angular/router";

import { Clientes2Component } from "./clientes2.component";

const routes: Routes = [{ path: "", component: Clientes2Component }];

@NgModule({
	imports: [NativeScriptRouterModule.forChild(routes)],
	exports: [NativeScriptRouterModule]
})
export class Clientes2RoutingModule {}