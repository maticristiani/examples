import { Component, ViewChild, ElementRef, AfterViewInit, OnDestroy, ChangeDetectorRef } from "@angular/core";
import { WebView, LoadEventData } from 'tns-core-modules/ui/web-view';
import * as appversion from "nativescript-appversion";
import { RouterExtensions } from "nativescript-angular/router";
import { CouchbaseInstance } from "../providers/couchbase/couchbase";
import { setString, getString, clear, remove } from 'tns-core-modules/application-settings';
import { isAndroid, isIOS, device, screen } from "tns-core-modules/platform";
import { Page } from 'tns-core-modules/ui/page';
import * as application from 'tns-core-modules/application';
var OneSignal : any= require('nativescript-onesignal').TnsOneSignal;
const connectivityModule = require("tns-core-modules/connectivity");
let webViewInterfaceModule = require('nativescript-webview-interface');
const urlWeb : string = 'https://www.colegiorussell.edu.ar/app/token_alumnos/';
let utils = require("utils/utils");
  

/* VARIABLES AND */
declare var android;

@Component({
    selector: "home",
    moduleId: module.id,
    templateUrl: "./home.component.html",
})
export class HomeComponent implements AfterViewInit, OnDestroy {
  //, {static: false}
  @ViewChild('webView', {static: false}) webView: ElementRef;
  
  private WebViewInterface;
  public isLoading: boolean;
  private appId: string;
  renderView = false;
  renderViewTimeout: any;

  constructor(
      private changeDetectorRef: ChangeDetectorRef, 
      private page: Page, 
      private _routerExtensions: RouterExtensions) {
      appversion.getAppId().then((id) => { this.appId = id });
      this.isLoading = true;

  }

  ngAfterViewInit() {
    this.renderViewTimeout = setTimeout(() => {
      this.renderView = true;

      this.webView.nativeElement.src = urlWeb;
      this.webView.nativeElement.animate({ opacity: 0, duration: 100 })

      const connectionType = connectivityModule.getConnectionType();
      
      if (Number(connectionType) < 1 ) {          
          this._routerExtensions.navigate(["offline"], { clearHistory: false});
      }else{
          this.page.actionBarHidden = true;

          // let couchbase = new CouchbaseInstance();
          // let docs_p = couchbase.getDatabase().executeQuery("tipo", { descending: true, startKey: "notificacion", endKey: "notificacion"  });                            

          // if(docs_p && docs_p.length == 0){
          //   let context = utils.ad.getApplicationContext();
          //   let manager = context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
            
          //   let notificationsArray = manager.getActiveNotifications();
                                      
          //   for (let i = 0; i < notificationsArray.length; i++) { 
          //     setTimeout(()=>{              
          //       OneSignal.cancelNotification(notificationsArray[i].getId())
          //     },200)
            
          //     setTimeout(()=>{
          //       manager.cancel(notificationsArray[i].getId())
          //     },300)
                                    
          //   }
          // }

          this.setupWebViewInterface();       
      }

    }, 500)

  }

  ngOnDestroy() {
    // cleaning up references/listeners.
    this.WebViewInterface.destroy();
    this.WebViewInterface = null;
  }
 
  /**
   * Initializes webViewInterface for communication between webview and android/ios
   */
  private setupWebViewInterface() {
    let webView: WebView = this.webView.nativeElement;

    console.log('OK INTERFACE', webView.src);
    
    this.WebViewInterface = new webViewInterfaceModule.WebViewInterface(webView, webView.src);
    this.listenWebViewEvents()
    
    webView.on(WebView.loadFinishedEvent, (args: LoadEventData) => {
      
      if (!args.error) {
        console.log('LOAD WEBVIEW')
        
          if (isAndroid && webView.android) {
                webView.android.getSettings().setJavaScriptEnabled(true);
                webView.android.getSettings().setLoadWithOverviewMode(true);
                webView.android.getSettings().setUseWideViewPort(true);
                webView.android.getSettings().setSupportZoom(true);
                webView.android.getSettings().setBuiltInZoomControls(true);

          } else {
                // webView.ios.scrollView.minimumZoomScale = 1.0;
                // webView.ios.scrollView.maximumZoomScale = 1.0;
                // webView.ios.scalesPageToFit = true;
                // webView.ios.scrollView.bounces = false;
          }


        setTimeout(()=>{ 
          
          console.log('VERIFICANDO PARAMETROS');

           if ( getString('notificacion_codigo') && getString('u_dni')) {
              
              let codigo = getString('notificacion_codigo');
              let dni = getString('u_dni');
              remove('notificacion_codigo');
              
              this.callJSFunction_3(codigo, dni);
          }

          console.log(getString('u_dni'));
          console.log(getString('u_pass'));

          if (getString('u_dni') && getString('u_pass')  ) {
              let usuario = {  dni: getString('u_dni'), password:getString('u_pass') };
              console.dir('AUTO-LOGIN',usuario.dni);
              this.callJSFunction_1(usuario);
          }
          
          this.callJSFunction_2()   

          this.isLoading = false;
          this.changeDetectorRef.detectChanges();
        
          console.log('FIN PARAMETROS');
        }, 300)
         
        
           
         this.webView.nativeElement.animate({ opacity: 1, duration: 1200 })

        
      }  else {

          this.changeDetectorRef.detectChanges();
          console.dir("Error loading " + args.url + ": " + args.error);
      }

    });

  }

  callJSFunction_1(usuario){
    this.WebViewInterface.callJSFunction('iniciar_sesion', usuario, (res)=>{ 
        console.log(res);
        console.log('USUARIO LOGUEADO: ', usuario.dni)
        
        setTimeout(()=>{
          this.isLoading = false;
        },1000)

    });
  };

  callJSFunction_2(){
    this.WebViewInterface.callJSFunction('app_id', this.appId, (res)=>{ 
        console.log(res);
    });
  };

  callJSFunction_3(codigo, dni){
    console.log('callJSFunction_3');
    var notificacion = { codigo: codigo, dni:dni };
    this.WebViewInterface.callJSFunction('app_notificacion', notificacion, (res)=>{ 
        if (res) {
          console.log(res);
          let webView: WebView = this.webView.nativeElement;
          webView.src = res;
        }
    });
  };


  /**
   * Handles any event/command emitted by language webview.
   */
  private listenWebViewEvents() {
    // handles language selectionChange event.

    //listen change user
    this.WebViewInterface.on('listenUser', (usuario) => {
      this.changeDetectorRef.detectChanges();
      console.log('GUARDO USUARIO:', usuario);
      
      let dni = String(usuario.dni)
      let password = String(usuario.password)
      
      setString('u_dni', dni);
      setString('u_pass', password);


                  try{
                    let dni = String(usuario.dni)
                    let obj = { "app": this.appId, "dni": dni };  
                    let meTAGS = JSON.stringify(obj);  
                    
                    if (isIOS) {
                      OneSignal.sendTagsWithJsonString(meTAGS); 
                    }else{
                      OneSignal.sendTags(meTAGS);
                    }
                    console.log('REGISTRADO: '+usuario.dni);
                  
                  }catch(e){
                    console.dir(e);
                  }     

    });

    //listen clear app
    this.WebViewInterface.on('listenClear', () => {
          
            console.log('clear cache and push');

            this.changeDetectorRef.detectChanges();
            clear();

            try{
              
              let obj = { "app": "", "dni": ""};  
              let tags = JSON.stringify(obj);  
                          
              if (isIOS) {
                   try{
                      OneSignal.deleteTagsWithJsonString(tags); 
                   }catch{console.log('error delete tags')}
              }else{
                   try{
                      OneSignal.deleteTag("app");
                      OneSignal.deleteTag("dni");
                   }catch{console.log('error delete tags')}
              }

              console.log('UNREGISTER PUSH');
              
            }catch(e){
              console.dir(e);
            }  
    })
    
    //listen cache app
    this.WebViewInterface.on('listenCache', () => {
      console.log('clear-cache');
      
      this.changeDetectorRef.detectChanges();
      clear();

    })

    //remover notificacion todas
    this.WebViewInterface.on('removerNotificacionAll', () => {
      let utils = require("utils/utils");
      let context = utils.ad.getApplicationContext();
      let manager = context.getSystemService(android.content.Context.NOTIFICATION_SERVICE);
      
      //arrayDeNotificaciones
      let notificationsArray = manager.getActiveNotifications();
      
      for (let i = 0; i < notificationsArray.length; i++) { 
          console.dir(notificationsArray[i].getId());  

          setTimeout(()=>{              
            OneSignal.cancelNotification(notificationsArray[i].getId())
          },200)
          setTimeout(()=>{
            manager.cancel(notificationsArray[i].getId())
          },300)
                                    
      }
      
    })

    //ver version de app
    this.WebViewInterface.on('versionApp', () => {
      console.log("ACCION: VERSIONAPP");   
        appversion.getVersionName().then((version: string) => {
          appversion.getVersionCode().then((code: string) => {
            let version_str = 'La version de su aplicaciones: '+ version + ' - '+code;
            console.log(version_str);
            alert(version_str)
          });
        });
    })

  }


}